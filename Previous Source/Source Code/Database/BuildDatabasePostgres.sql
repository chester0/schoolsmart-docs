--
-- PostgreSQL database cluster dump
--

-- Started on 2015-07-07 12:49:28

SET default_transaction_read_only = off;

SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;

--
-- Roles
--

CREATE ROLE web;
ALTER ROLE web WITH NOSUPERUSER INHERIT NOCREATEROLE NOCREATEDB LOGIN NOREPLICATION PASSWORD 'md5ec2999c5602bcbd855c64cc38f0d04d2' VALID UNTIL 'infinity';

--
-- Database creation
--

CREATE DATABASE studentpickup WITH TEMPLATE = template0 OWNER = postgres;
REVOKE ALL ON DATABASE studentpickup FROM PUBLIC;
REVOKE ALL ON DATABASE studentpickup FROM postgres;
GRANT ALL ON DATABASE studentpickup TO postgres;
GRANT CONNECT,TEMPORARY ON DATABASE studentpickup TO PUBLIC;
GRANT ALL ON DATABASE studentpickup TO web;
REVOKE ALL ON DATABASE template1 FROM PUBLIC;
REVOKE ALL ON DATABASE template1 FROM postgres;
GRANT ALL ON DATABASE template1 TO postgres;
GRANT CONNECT ON DATABASE template1 TO PUBLIC;

SET default_transaction_read_only = off;

--
-- PostgreSQL database dump
--

-- Dumped from database version 9.4.4
-- Dumped by pg_dump version 9.4.0
-- Started on 2015-07-07 12:49:28

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 1990 (class 1262 OID 12135)
-- Dependencies: 1989
-- Name: postgres; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON DATABASE postgres IS 'default administrative connection database';


--
-- TOC entry 173 (class 3079 OID 11855)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 1993 (class 0 OID 0)
-- Dependencies: 173
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- TOC entry 172 (class 3079 OID 16384)
-- Name: adminpack; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS adminpack WITH SCHEMA pg_catalog;


--
-- TOC entry 1994 (class 0 OID 0)
-- Dependencies: 172
-- Name: EXTENSION adminpack; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION adminpack IS 'administrative functions for PostgreSQL';


--
-- TOC entry 1992 (class 0 OID 0)
-- Dependencies: 5
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2015-07-07 12:49:28

--
-- PostgreSQL database dump complete
--

\connect studentpickup

SET default_transaction_read_only = off;

--
-- PostgreSQL database dump
--

-- Dumped from database version 9.4.4
-- Dumped by pg_dump version 9.4.0
-- Started on 2015-07-07 12:49:28

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 7 (class 2615 OID 16398)
-- Name: dbo; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA dbo;


ALTER SCHEMA dbo OWNER TO postgres;

--
-- TOC entry 206 (class 3079 OID 11855)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2207 (class 0 OID 0)
-- Dependencies: 206
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = dbo, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 188 (class 1259 OID 16472)
-- Name: circletype; Type: TABLE; Schema: dbo; Owner: postgres; Tablespace: 
--

CREATE TABLE circletype (
    circletypeid integer NOT NULL,
    name text NOT NULL,
    description text
);


ALTER TABLE circletype OWNER TO postgres;

--
-- TOC entry 187 (class 1259 OID 16470)
-- Name: circletype_circletypeid_seq; Type: SEQUENCE; Schema: dbo; Owner: postgres
--

CREATE SEQUENCE circletype_circletypeid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE circletype_circletypeid_seq OWNER TO postgres;

--
-- TOC entry 2209 (class 0 OID 0)
-- Dependencies: 187
-- Name: circletype_circletypeid_seq; Type: SEQUENCE OWNED BY; Schema: dbo; Owner: postgres
--

ALTER SEQUENCE circletype_circletypeid_seq OWNED BY circletype.circletypeid;


--
-- TOC entry 186 (class 1259 OID 16461)
-- Name: country; Type: TABLE; Schema: dbo; Owner: postgres; Tablespace: 
--

CREATE TABLE country (
    countryid integer NOT NULL,
    name text NOT NULL
);


ALTER TABLE country OWNER TO postgres;

--
-- TOC entry 185 (class 1259 OID 16459)
-- Name: country_countryid_seq; Type: SEQUENCE; Schema: dbo; Owner: postgres
--

CREATE SEQUENCE country_countryid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE country_countryid_seq OWNER TO postgres;

--
-- TOC entry 2212 (class 0 OID 0)
-- Dependencies: 185
-- Name: country_countryid_seq; Type: SEQUENCE OWNED BY; Schema: dbo; Owner: postgres
--

ALTER SEQUENCE country_countryid_seq OWNED BY country.countryid;


--
-- TOC entry 205 (class 1259 OID 16559)
-- Name: deviceoperatingsystem; Type: TABLE; Schema: dbo; Owner: postgres; Tablespace: 
--

CREATE TABLE deviceoperatingsystem (
    deviceoperatingsystemid integer NOT NULL,
    name text NOT NULL
);


ALTER TABLE deviceoperatingsystem OWNER TO postgres;

--
-- TOC entry 204 (class 1259 OID 16557)
-- Name: deviceoperatingsystem_deviceoperatingsystemid_seq; Type: SEQUENCE; Schema: dbo; Owner: postgres
--

CREATE SEQUENCE deviceoperatingsystem_deviceoperatingsystemid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE deviceoperatingsystem_deviceoperatingsystemid_seq OWNER TO postgres;

--
-- TOC entry 2215 (class 0 OID 0)
-- Dependencies: 204
-- Name: deviceoperatingsystem_deviceoperatingsystemid_seq; Type: SEQUENCE OWNED BY; Schema: dbo; Owner: postgres
--

ALTER SEQUENCE deviceoperatingsystem_deviceoperatingsystemid_seq OWNED BY deviceoperatingsystem.deviceoperatingsystemid;


--
-- TOC entry 184 (class 1259 OID 16453)
-- Name: geozonepolygonpoint; Type: TABLE; Schema: dbo; Owner: postgres; Tablespace: 
--

CREATE TABLE geozonepolygonpoint (
    geozonepolygonpointid integer NOT NULL,
    geozoneid integer NOT NULL,
    longtitude numeric(9,6) NOT NULL,
    latitude numeric(9,6) NOT NULL
);


ALTER TABLE geozonepolygonpoint OWNER TO postgres;

--
-- TOC entry 183 (class 1259 OID 16451)
-- Name: geozonepolygonpoint_geozonepolygonpointid_seq; Type: SEQUENCE; Schema: dbo; Owner: postgres
--

CREATE SEQUENCE geozonepolygonpoint_geozonepolygonpointid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE geozonepolygonpoint_geozonepolygonpointid_seq OWNER TO postgres;

--
-- TOC entry 2218 (class 0 OID 0)
-- Dependencies: 183
-- Name: geozonepolygonpoint_geozonepolygonpointid_seq; Type: SEQUENCE OWNED BY; Schema: dbo; Owner: postgres
--

ALTER SEQUENCE geozonepolygonpoint_geozonepolygonpointid_seq OWNED BY geozonepolygonpoint.geozonepolygonpointid;


--
-- TOC entry 182 (class 1259 OID 16442)
-- Name: geozonetype; Type: TABLE; Schema: dbo; Owner: postgres; Tablespace: 
--

CREATE TABLE geozonetype (
    geozonetypeid integer NOT NULL,
    name text NOT NULL,
    description text
);


ALTER TABLE geozonetype OWNER TO postgres;

--
-- TOC entry 181 (class 1259 OID 16440)
-- Name: geozonetype_geozonetypeid_seq; Type: SEQUENCE; Schema: dbo; Owner: postgres
--

CREATE SEQUENCE geozonetype_geozonetypeid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE geozonetype_geozonetypeid_seq OWNER TO postgres;

--
-- TOC entry 2221 (class 0 OID 0)
-- Dependencies: 181
-- Name: geozonetype_geozonetypeid_seq; Type: SEQUENCE OWNED BY; Schema: dbo; Owner: postgres
--

ALTER SEQUENCE geozonetype_geozonetypeid_seq OWNED BY geozonetype.geozonetypeid;


--
-- TOC entry 196 (class 1259 OID 16513)
-- Name: guardian; Type: TABLE; Schema: dbo; Owner: postgres; Tablespace: 
--

CREATE TABLE guardian (
    guardianid integer NOT NULL,
    givenname text NOT NULL,
    familyname text NOT NULL,
    friendlyname text,
    mobilenumber text NOT NULL,
    phonenumber text,
    emailaddress text,
    lastknownlatitude numeric(9,6),
    lastknownlongtitude numeric(9,6),
    lastknownetaseconds integer,
    lastrecordedlocationtimestamp timestamp without time zone
);


ALTER TABLE guardian OWNER TO postgres;

--
-- TOC entry 195 (class 1259 OID 16511)
-- Name: guardian_guardianid_seq; Type: SEQUENCE; Schema: dbo; Owner: postgres
--

CREATE SEQUENCE guardian_guardianid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE guardian_guardianid_seq OWNER TO postgres;

--
-- TOC entry 2224 (class 0 OID 0)
-- Dependencies: 195
-- Name: guardian_guardianid_seq; Type: SEQUENCE OWNED BY; Schema: dbo; Owner: postgres
--

ALTER SEQUENCE guardian_guardianid_seq OWNED BY guardian.guardianid;


--
-- TOC entry 203 (class 1259 OID 16548)
-- Name: guardiandevice; Type: TABLE; Schema: dbo; Owner: postgres; Tablespace: 
--

CREATE TABLE guardiandevice (
    guardiandeviceid integer NOT NULL,
    guardianid integer NOT NULL,
    deviceappid text NOT NULL,
    deviceoperatingsystemid integer NOT NULL
);


ALTER TABLE guardiandevice OWNER TO postgres;

--
-- TOC entry 202 (class 1259 OID 16546)
-- Name: guardiandevice_guardiandeviceid_seq; Type: SEQUENCE; Schema: dbo; Owner: postgres
--

CREATE SEQUENCE guardiandevice_guardiandeviceid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE guardiandevice_guardiandeviceid_seq OWNER TO postgres;

--
-- TOC entry 2227 (class 0 OID 0)
-- Dependencies: 202
-- Name: guardiandevice_guardiandeviceid_seq; Type: SEQUENCE OWNED BY; Schema: dbo; Owner: postgres
--

ALTER SEQUENCE guardiandevice_guardiandeviceid_seq OWNED BY guardiandevice.guardiandeviceid;


--
-- TOC entry 201 (class 1259 OID 16541)
-- Name: guardianstudentcircle; Type: TABLE; Schema: dbo; Owner: postgres; Tablespace: 
--

CREATE TABLE guardianstudentcircle (
    guardianid integer NOT NULL,
    studentcircleid integer NOT NULL,
    isaccepted boolean NOT NULL
);


ALTER TABLE guardianstudentcircle OWNER TO postgres;

--
-- TOC entry 194 (class 1259 OID 16502)
-- Name: kiosk; Type: TABLE; Schema: dbo; Owner: postgres; Tablespace: 
--

CREATE TABLE kiosk (
    kioskid integer NOT NULL,
    parentkioskid integer,
    parentportnumber integer,
    schoolid integer NOT NULL,
    name text NOT NULL,
    latitude numeric(9,6) NOT NULL,
    longtitude numeric(9,6) NOT NULL
);


ALTER TABLE kiosk OWNER TO postgres;

--
-- TOC entry 193 (class 1259 OID 16500)
-- Name: kiosk_kioskid_seq; Type: SEQUENCE; Schema: dbo; Owner: postgres
--

CREATE SEQUENCE kiosk_kioskid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE kiosk_kioskid_seq OWNER TO postgres;

--
-- TOC entry 2231 (class 0 OID 0)
-- Dependencies: 193
-- Name: kiosk_kioskid_seq; Type: SEQUENCE OWNED BY; Schema: dbo; Owner: postgres
--

ALTER SEQUENCE kiosk_kioskid_seq OWNED BY kiosk.kioskid;


--
-- TOC entry 180 (class 1259 OID 16434)
-- Name: pickupschedule; Type: TABLE; Schema: dbo; Owner: postgres; Tablespace: 
--

CREATE TABLE pickupschedule (
    pickupscheduleid integer NOT NULL,
    studentid integer NOT NULL,
    startdate timestamp without time zone,
    enddate timestamp without time zone
);


ALTER TABLE pickupschedule OWNER TO postgres;

--
-- TOC entry 179 (class 1259 OID 16432)
-- Name: pickupschedule_pickupscheduleid_seq; Type: SEQUENCE; Schema: dbo; Owner: postgres
--

CREATE SEQUENCE pickupschedule_pickupscheduleid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE pickupschedule_pickupscheduleid_seq OWNER TO postgres;

--
-- TOC entry 2234 (class 0 OID 0)
-- Dependencies: 179
-- Name: pickupschedule_pickupscheduleid_seq; Type: SEQUENCE OWNED BY; Schema: dbo; Owner: postgres
--

ALTER SEQUENCE pickupschedule_pickupscheduleid_seq OWNED BY pickupschedule.pickupscheduleid;


--
-- TOC entry 192 (class 1259 OID 16494)
-- Name: pickupschedulesession; Type: TABLE; Schema: dbo; Owner: postgres; Tablespace: 
--

CREATE TABLE pickupschedulesession (
    pickupschedulesessionid integer NOT NULL,
    pickupscheduleid integer NOT NULL,
    guardianid integer NOT NULL,
    sessiondate date NOT NULL
);


ALTER TABLE pickupschedulesession OWNER TO postgres;

--
-- TOC entry 191 (class 1259 OID 16492)
-- Name: pickupschedulesession_pickupschedulesessionid_seq; Type: SEQUENCE; Schema: dbo; Owner: postgres
--

CREATE SEQUENCE pickupschedulesession_pickupschedulesessionid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE pickupschedulesession_pickupschedulesessionid_seq OWNER TO postgres;

--
-- TOC entry 2237 (class 0 OID 0)
-- Dependencies: 191
-- Name: pickupschedulesession_pickupschedulesessionid_seq; Type: SEQUENCE OWNED BY; Schema: dbo; Owner: postgres
--

ALTER SEQUENCE pickupschedulesession_pickupschedulesessionid_seq OWNED BY pickupschedulesession.pickupschedulesessionid;


--
-- TOC entry 200 (class 1259 OID 16532)
-- Name: school; Type: TABLE; Schema: dbo; Owner: postgres; Tablespace: 
--

CREATE TABLE school (
    schoolid integer NOT NULL,
    name text NOT NULL,
    address text NOT NULL,
    contactname text,
    contactemail text,
    contactnumber text,
    stateid integer NOT NULL,
    latitude numeric(9,6),
    longtitude numeric(9,6)
);


ALTER TABLE school OWNER TO postgres;

--
-- TOC entry 199 (class 1259 OID 16530)
-- Name: school_schoolid_seq; Type: SEQUENCE; Schema: dbo; Owner: postgres
--

CREATE SEQUENCE school_schoolid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE school_schoolid_seq OWNER TO postgres;

--
-- TOC entry 2240 (class 0 OID 0)
-- Dependencies: 199
-- Name: school_schoolid_seq; Type: SEQUENCE OWNED BY; Schema: dbo; Owner: postgres
--

ALTER SEQUENCE school_schoolid_seq OWNED BY school.schoolid;


--
-- TOC entry 178 (class 1259 OID 16423)
-- Name: schoolgeozone; Type: TABLE; Schema: dbo; Owner: postgres; Tablespace: 
--

CREATE TABLE schoolgeozone (
    schoolgeozoneid integer NOT NULL,
    geozonetypeid integer NOT NULL,
    schoolid integer NOT NULL,
    name text NOT NULL,
    guardianmessage text NOT NULL,
    description text
);


ALTER TABLE schoolgeozone OWNER TO postgres;

--
-- TOC entry 177 (class 1259 OID 16421)
-- Name: schoolgeozone_schoolgeozoneid_seq; Type: SEQUENCE; Schema: dbo; Owner: postgres
--

CREATE SEQUENCE schoolgeozone_schoolgeozoneid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE schoolgeozone_schoolgeozoneid_seq OWNER TO postgres;

--
-- TOC entry 2243 (class 0 OID 0)
-- Dependencies: 177
-- Name: schoolgeozone_schoolgeozoneid_seq; Type: SEQUENCE OWNED BY; Schema: dbo; Owner: postgres
--

ALTER SEQUENCE schoolgeozone_schoolgeozoneid_seq OWNED BY schoolgeozone.schoolgeozoneid;


--
-- TOC entry 176 (class 1259 OID 16412)
-- Name: state; Type: TABLE; Schema: dbo; Owner: postgres; Tablespace: 
--

CREATE TABLE state (
    stateid integer NOT NULL,
    name text NOT NULL,
    countryid integer NOT NULL
);


ALTER TABLE state OWNER TO postgres;

--
-- TOC entry 175 (class 1259 OID 16410)
-- Name: state_stateid_seq; Type: SEQUENCE; Schema: dbo; Owner: postgres
--

CREATE SEQUENCE state_stateid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE state_stateid_seq OWNER TO postgres;

--
-- TOC entry 2246 (class 0 OID 0)
-- Dependencies: 175
-- Name: state_stateid_seq; Type: SEQUENCE OWNED BY; Schema: dbo; Owner: postgres
--

ALTER SEQUENCE state_stateid_seq OWNED BY state.stateid;


--
-- TOC entry 190 (class 1259 OID 16483)
-- Name: student; Type: TABLE; Schema: dbo; Owner: postgres; Tablespace: 
--

CREATE TABLE student (
    studentid integer NOT NULL,
    studentschoolid integer,
    rfid text,
    schoolid integer NOT NULL,
    mobileappid text,
    mobilenumber text,
    givenname text NOT NULL,
    commonname text,
    familyname text NOT NULL,
    dateofbirth date NOT NULL
);


ALTER TABLE student OWNER TO postgres;

--
-- TOC entry 189 (class 1259 OID 16481)
-- Name: student_studentid_seq; Type: SEQUENCE; Schema: dbo; Owner: postgres
--

CREATE SEQUENCE student_studentid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE student_studentid_seq OWNER TO postgres;

--
-- TOC entry 2249 (class 0 OID 0)
-- Dependencies: 189
-- Name: student_studentid_seq; Type: SEQUENCE OWNED BY; Schema: dbo; Owner: postgres
--

ALTER SEQUENCE student_studentid_seq OWNED BY student.studentid;


--
-- TOC entry 174 (class 1259 OID 16401)
-- Name: studentcircle; Type: TABLE; Schema: dbo; Owner: postgres; Tablespace: 
--

CREATE TABLE studentcircle (
    studentcircleid integer NOT NULL,
    studentid integer NOT NULL,
    name text NOT NULL,
    circletypeid integer NOT NULL
);


ALTER TABLE studentcircle OWNER TO postgres;

--
-- TOC entry 173 (class 1259 OID 16399)
-- Name: studentcircle_studentcircleid_seq; Type: SEQUENCE; Schema: dbo; Owner: postgres
--

CREATE SEQUENCE studentcircle_studentcircleid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE studentcircle_studentcircleid_seq OWNER TO postgres;

--
-- TOC entry 2252 (class 0 OID 0)
-- Dependencies: 173
-- Name: studentcircle_studentcircleid_seq; Type: SEQUENCE OWNED BY; Schema: dbo; Owner: postgres
--

ALTER SEQUENCE studentcircle_studentcircleid_seq OWNED BY studentcircle.studentcircleid;


--
-- TOC entry 198 (class 1259 OID 16524)
-- Name: studentkiosknotification; Type: TABLE; Schema: dbo; Owner: postgres; Tablespace: 
--

CREATE TABLE studentkiosknotification (
    studentkiosknotificationid integer NOT NULL,
    studentid integer NOT NULL,
    kioskid integer NOT NULL,
    notificationtimestamp timestamp without time zone NOT NULL,
    response integer
);


ALTER TABLE studentkiosknotification OWNER TO postgres;

--
-- TOC entry 197 (class 1259 OID 16522)
-- Name: studentkiosknotification_studentkiosknotificationid_seq; Type: SEQUENCE; Schema: dbo; Owner: postgres
--

CREATE SEQUENCE studentkiosknotification_studentkiosknotificationid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE studentkiosknotification_studentkiosknotificationid_seq OWNER TO postgres;

--
-- TOC entry 2255 (class 0 OID 0)
-- Dependencies: 197
-- Name: studentkiosknotification_studentkiosknotificationid_seq; Type: SEQUENCE OWNED BY; Schema: dbo; Owner: postgres
--

ALTER SEQUENCE studentkiosknotification_studentkiosknotificationid_seq OWNED BY studentkiosknotification.studentkiosknotificationid;


--
-- TOC entry 1995 (class 2604 OID 16475)
-- Name: circletypeid; Type: DEFAULT; Schema: dbo; Owner: postgres
--

ALTER TABLE ONLY circletype ALTER COLUMN circletypeid SET DEFAULT nextval('circletype_circletypeid_seq'::regclass);


--
-- TOC entry 1994 (class 2604 OID 16464)
-- Name: countryid; Type: DEFAULT; Schema: dbo; Owner: postgres
--

ALTER TABLE ONLY country ALTER COLUMN countryid SET DEFAULT nextval('country_countryid_seq'::regclass);


--
-- TOC entry 2003 (class 2604 OID 16562)
-- Name: deviceoperatingsystemid; Type: DEFAULT; Schema: dbo; Owner: postgres
--

ALTER TABLE ONLY deviceoperatingsystem ALTER COLUMN deviceoperatingsystemid SET DEFAULT nextval('deviceoperatingsystem_deviceoperatingsystemid_seq'::regclass);


--
-- TOC entry 1993 (class 2604 OID 16456)
-- Name: geozonepolygonpointid; Type: DEFAULT; Schema: dbo; Owner: postgres
--

ALTER TABLE ONLY geozonepolygonpoint ALTER COLUMN geozonepolygonpointid SET DEFAULT nextval('geozonepolygonpoint_geozonepolygonpointid_seq'::regclass);


--
-- TOC entry 1992 (class 2604 OID 16445)
-- Name: geozonetypeid; Type: DEFAULT; Schema: dbo; Owner: postgres
--

ALTER TABLE ONLY geozonetype ALTER COLUMN geozonetypeid SET DEFAULT nextval('geozonetype_geozonetypeid_seq'::regclass);


--
-- TOC entry 1999 (class 2604 OID 16516)
-- Name: guardianid; Type: DEFAULT; Schema: dbo; Owner: postgres
--

ALTER TABLE ONLY guardian ALTER COLUMN guardianid SET DEFAULT nextval('guardian_guardianid_seq'::regclass);


--
-- TOC entry 2002 (class 2604 OID 16551)
-- Name: guardiandeviceid; Type: DEFAULT; Schema: dbo; Owner: postgres
--

ALTER TABLE ONLY guardiandevice ALTER COLUMN guardiandeviceid SET DEFAULT nextval('guardiandevice_guardiandeviceid_seq'::regclass);


--
-- TOC entry 1998 (class 2604 OID 16505)
-- Name: kioskid; Type: DEFAULT; Schema: dbo; Owner: postgres
--

ALTER TABLE ONLY kiosk ALTER COLUMN kioskid SET DEFAULT nextval('kiosk_kioskid_seq'::regclass);


--
-- TOC entry 1991 (class 2604 OID 16437)
-- Name: pickupscheduleid; Type: DEFAULT; Schema: dbo; Owner: postgres
--

ALTER TABLE ONLY pickupschedule ALTER COLUMN pickupscheduleid SET DEFAULT nextval('pickupschedule_pickupscheduleid_seq'::regclass);


--
-- TOC entry 1997 (class 2604 OID 16497)
-- Name: pickupschedulesessionid; Type: DEFAULT; Schema: dbo; Owner: postgres
--

ALTER TABLE ONLY pickupschedulesession ALTER COLUMN pickupschedulesessionid SET DEFAULT nextval('pickupschedulesession_pickupschedulesessionid_seq'::regclass);


--
-- TOC entry 2001 (class 2604 OID 16535)
-- Name: schoolid; Type: DEFAULT; Schema: dbo; Owner: postgres
--

ALTER TABLE ONLY school ALTER COLUMN schoolid SET DEFAULT nextval('school_schoolid_seq'::regclass);


--
-- TOC entry 1990 (class 2604 OID 16426)
-- Name: schoolgeozoneid; Type: DEFAULT; Schema: dbo; Owner: postgres
--

ALTER TABLE ONLY schoolgeozone ALTER COLUMN schoolgeozoneid SET DEFAULT nextval('schoolgeozone_schoolgeozoneid_seq'::regclass);


--
-- TOC entry 1989 (class 2604 OID 16415)
-- Name: stateid; Type: DEFAULT; Schema: dbo; Owner: postgres
--

ALTER TABLE ONLY state ALTER COLUMN stateid SET DEFAULT nextval('state_stateid_seq'::regclass);


--
-- TOC entry 1996 (class 2604 OID 16486)
-- Name: studentid; Type: DEFAULT; Schema: dbo; Owner: postgres
--

ALTER TABLE ONLY student ALTER COLUMN studentid SET DEFAULT nextval('student_studentid_seq'::regclass);


--
-- TOC entry 1988 (class 2604 OID 16404)
-- Name: studentcircleid; Type: DEFAULT; Schema: dbo; Owner: postgres
--

ALTER TABLE ONLY studentcircle ALTER COLUMN studentcircleid SET DEFAULT nextval('studentcircle_studentcircleid_seq'::regclass);


--
-- TOC entry 2000 (class 2604 OID 16527)
-- Name: studentkiosknotificationid; Type: DEFAULT; Schema: dbo; Owner: postgres
--

ALTER TABLE ONLY studentkiosknotification ALTER COLUMN studentkiosknotificationid SET DEFAULT nextval('studentkiosknotification_studentkiosknotificationid_seq'::regclass);


--
-- TOC entry 2181 (class 0 OID 16472)
-- Dependencies: 188
-- Data for Name: circletype; Type: TABLE DATA; Schema: dbo; Owner: postgres
--

COPY circletype (circletypeid, name, description) FROM stdin;
\.


--
-- TOC entry 2257 (class 0 OID 0)
-- Dependencies: 187
-- Name: circletype_circletypeid_seq; Type: SEQUENCE SET; Schema: dbo; Owner: postgres
--

SELECT pg_catalog.setval('circletype_circletypeid_seq', 1, false);


--
-- TOC entry 2179 (class 0 OID 16461)
-- Dependencies: 186
-- Data for Name: country; Type: TABLE DATA; Schema: dbo; Owner: postgres
--

COPY country (countryid, name) FROM stdin;
\.


--
-- TOC entry 2258 (class 0 OID 0)
-- Dependencies: 185
-- Name: country_countryid_seq; Type: SEQUENCE SET; Schema: dbo; Owner: postgres
--

SELECT pg_catalog.setval('country_countryid_seq', 1, false);


--
-- TOC entry 2198 (class 0 OID 16559)
-- Dependencies: 205
-- Data for Name: deviceoperatingsystem; Type: TABLE DATA; Schema: dbo; Owner: postgres
--

COPY deviceoperatingsystem (deviceoperatingsystemid, name) FROM stdin;
\.


--
-- TOC entry 2259 (class 0 OID 0)
-- Dependencies: 204
-- Name: deviceoperatingsystem_deviceoperatingsystemid_seq; Type: SEQUENCE SET; Schema: dbo; Owner: postgres
--

SELECT pg_catalog.setval('deviceoperatingsystem_deviceoperatingsystemid_seq', 1, false);


--
-- TOC entry 2177 (class 0 OID 16453)
-- Dependencies: 184
-- Data for Name: geozonepolygonpoint; Type: TABLE DATA; Schema: dbo; Owner: postgres
--

COPY geozonepolygonpoint (geozonepolygonpointid, geozoneid, longtitude, latitude) FROM stdin;
\.


--
-- TOC entry 2260 (class 0 OID 0)
-- Dependencies: 183
-- Name: geozonepolygonpoint_geozonepolygonpointid_seq; Type: SEQUENCE SET; Schema: dbo; Owner: postgres
--

SELECT pg_catalog.setval('geozonepolygonpoint_geozonepolygonpointid_seq', 1, false);


--
-- TOC entry 2175 (class 0 OID 16442)
-- Dependencies: 182
-- Data for Name: geozonetype; Type: TABLE DATA; Schema: dbo; Owner: postgres
--

COPY geozonetype (geozonetypeid, name, description) FROM stdin;
\.


--
-- TOC entry 2261 (class 0 OID 0)
-- Dependencies: 181
-- Name: geozonetype_geozonetypeid_seq; Type: SEQUENCE SET; Schema: dbo; Owner: postgres
--

SELECT pg_catalog.setval('geozonetype_geozonetypeid_seq', 1, false);


--
-- TOC entry 2189 (class 0 OID 16513)
-- Dependencies: 196
-- Data for Name: guardian; Type: TABLE DATA; Schema: dbo; Owner: postgres
--

COPY guardian (guardianid, givenname, familyname, friendlyname, mobilenumber, phonenumber, emailaddress, lastknownlatitude, lastknownlongtitude, lastknownetaseconds, lastrecordedlocationtimestamp) FROM stdin;
\.


--
-- TOC entry 2262 (class 0 OID 0)
-- Dependencies: 195
-- Name: guardian_guardianid_seq; Type: SEQUENCE SET; Schema: dbo; Owner: postgres
--

SELECT pg_catalog.setval('guardian_guardianid_seq', 1, true);


--
-- TOC entry 2196 (class 0 OID 16548)
-- Dependencies: 203
-- Data for Name: guardiandevice; Type: TABLE DATA; Schema: dbo; Owner: postgres
--

COPY guardiandevice (guardiandeviceid, guardianid, deviceappid, deviceoperatingsystemid) FROM stdin;
\.


--
-- TOC entry 2263 (class 0 OID 0)
-- Dependencies: 202
-- Name: guardiandevice_guardiandeviceid_seq; Type: SEQUENCE SET; Schema: dbo; Owner: postgres
--

SELECT pg_catalog.setval('guardiandevice_guardiandeviceid_seq', 1, false);


--
-- TOC entry 2194 (class 0 OID 16541)
-- Dependencies: 201
-- Data for Name: guardianstudentcircle; Type: TABLE DATA; Schema: dbo; Owner: postgres
--

COPY guardianstudentcircle (guardianid, studentcircleid, isaccepted) FROM stdin;
\.


--
-- TOC entry 2187 (class 0 OID 16502)
-- Dependencies: 194
-- Data for Name: kiosk; Type: TABLE DATA; Schema: dbo; Owner: postgres
--

COPY kiosk (kioskid, parentkioskid, parentportnumber, schoolid, name, latitude, longtitude) FROM stdin;
\.


--
-- TOC entry 2264 (class 0 OID 0)
-- Dependencies: 193
-- Name: kiosk_kioskid_seq; Type: SEQUENCE SET; Schema: dbo; Owner: postgres
--

SELECT pg_catalog.setval('kiosk_kioskid_seq', 1, false);


--
-- TOC entry 2173 (class 0 OID 16434)
-- Dependencies: 180
-- Data for Name: pickupschedule; Type: TABLE DATA; Schema: dbo; Owner: postgres
--

COPY pickupschedule (pickupscheduleid, studentid, startdate, enddate) FROM stdin;
\.


--
-- TOC entry 2265 (class 0 OID 0)
-- Dependencies: 179
-- Name: pickupschedule_pickupscheduleid_seq; Type: SEQUENCE SET; Schema: dbo; Owner: postgres
--

SELECT pg_catalog.setval('pickupschedule_pickupscheduleid_seq', 1, false);


--
-- TOC entry 2185 (class 0 OID 16494)
-- Dependencies: 192
-- Data for Name: pickupschedulesession; Type: TABLE DATA; Schema: dbo; Owner: postgres
--

COPY pickupschedulesession (pickupschedulesessionid, pickupscheduleid, guardianid, sessiondate) FROM stdin;
\.


--
-- TOC entry 2266 (class 0 OID 0)
-- Dependencies: 191
-- Name: pickupschedulesession_pickupschedulesessionid_seq; Type: SEQUENCE SET; Schema: dbo; Owner: postgres
--

SELECT pg_catalog.setval('pickupschedulesession_pickupschedulesessionid_seq', 1, false);


--
-- TOC entry 2193 (class 0 OID 16532)
-- Dependencies: 200
-- Data for Name: school; Type: TABLE DATA; Schema: dbo; Owner: postgres
--

COPY school (schoolid, name, address, contactname, contactemail, contactnumber, stateid) FROM stdin;
\.


--
-- TOC entry 2267 (class 0 OID 0)
-- Dependencies: 199
-- Name: school_schoolid_seq; Type: SEQUENCE SET; Schema: dbo; Owner: postgres
--

SELECT pg_catalog.setval('school_schoolid_seq', 1, false);


--
-- TOC entry 2171 (class 0 OID 16423)
-- Dependencies: 178
-- Data for Name: schoolgeozone; Type: TABLE DATA; Schema: dbo; Owner: postgres
--

COPY schoolgeozone (schoolgeozoneid, geozonetypeid, schoolid, name, guardianmessage, description) FROM stdin;
\.


--
-- TOC entry 2268 (class 0 OID 0)
-- Dependencies: 177
-- Name: schoolgeozone_schoolgeozoneid_seq; Type: SEQUENCE SET; Schema: dbo; Owner: postgres
--

SELECT pg_catalog.setval('schoolgeozone_schoolgeozoneid_seq', 1, false);


--
-- TOC entry 2169 (class 0 OID 16412)
-- Dependencies: 176
-- Data for Name: state; Type: TABLE DATA; Schema: dbo; Owner: postgres
--

COPY state (stateid, name, countryid) FROM stdin;
\.


--
-- TOC entry 2269 (class 0 OID 0)
-- Dependencies: 175
-- Name: state_stateid_seq; Type: SEQUENCE SET; Schema: dbo; Owner: postgres
--

SELECT pg_catalog.setval('state_stateid_seq', 1, false);


--
-- TOC entry 2183 (class 0 OID 16483)
-- Dependencies: 190
-- Data for Name: student; Type: TABLE DATA; Schema: dbo; Owner: postgres
--

COPY student (studentid, studentschoolid, rfid, schoolid, mobileappid, mobilenumber, givenname, commonname, familyname, dateofbirth) FROM stdin;
\.


--
-- TOC entry 2270 (class 0 OID 0)
-- Dependencies: 189
-- Name: student_studentid_seq; Type: SEQUENCE SET; Schema: dbo; Owner: postgres
--

SELECT pg_catalog.setval('student_studentid_seq', 1, false);


--
-- TOC entry 2167 (class 0 OID 16401)
-- Dependencies: 174
-- Data for Name: studentcircle; Type: TABLE DATA; Schema: dbo; Owner: postgres
--

COPY studentcircle (studentcircleid, studentid, name, circletypeid) FROM stdin;
\.


--
-- TOC entry 2271 (class 0 OID 0)
-- Dependencies: 173
-- Name: studentcircle_studentcircleid_seq; Type: SEQUENCE SET; Schema: dbo; Owner: postgres
--

SELECT pg_catalog.setval('studentcircle_studentcircleid_seq', 1, false);


--
-- TOC entry 2191 (class 0 OID 16524)
-- Dependencies: 198
-- Data for Name: studentkiosknotification; Type: TABLE DATA; Schema: dbo; Owner: postgres
--

COPY studentkiosknotification (studentkiosknotificationid, studentid, kioskid, notificationtimestamp, response) FROM stdin;
\.


--
-- TOC entry 2272 (class 0 OID 0)
-- Dependencies: 197
-- Name: studentkiosknotification_studentkiosknotificationid_seq; Type: SEQUENCE SET; Schema: dbo; Owner: postgres
--

SELECT pg_catalog.setval('studentkiosknotification_studentkiosknotificationid_seq', 1, false);


--
-- TOC entry 2019 (class 2606 OID 16480)
-- Name: pk_circletype; Type: CONSTRAINT; Schema: dbo; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY circletype
    ADD CONSTRAINT pk_circletype PRIMARY KEY (circletypeid);


--
-- TOC entry 2017 (class 2606 OID 16469)
-- Name: pk_country; Type: CONSTRAINT; Schema: dbo; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY country
    ADD CONSTRAINT pk_country PRIMARY KEY (countryid);


--
-- TOC entry 2015 (class 2606 OID 16458)
-- Name: pk_geozonepolygonpoint; Type: CONSTRAINT; Schema: dbo; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY geozonepolygonpoint
    ADD CONSTRAINT pk_geozonepolygonpoint PRIMARY KEY (geozonepolygonpointid);


--
-- TOC entry 2013 (class 2606 OID 16450)
-- Name: pk_geozonetype; Type: CONSTRAINT; Schema: dbo; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY geozonetype
    ADD CONSTRAINT pk_geozonetype PRIMARY KEY (geozonetypeid);


--
-- TOC entry 2027 (class 2606 OID 16521)
-- Name: pk_guardian; Type: CONSTRAINT; Schema: dbo; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY guardian
    ADD CONSTRAINT pk_guardian PRIMARY KEY (guardianid);


--
-- TOC entry 2035 (class 2606 OID 16556)
-- Name: pk_guardiandevice; Type: CONSTRAINT; Schema: dbo; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY guardiandevice
    ADD CONSTRAINT pk_guardiandevice PRIMARY KEY (guardiandeviceid);


--
-- TOC entry 2033 (class 2606 OID 16545)
-- Name: pk_guardianstudentcircle; Type: CONSTRAINT; Schema: dbo; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY guardianstudentcircle
    ADD CONSTRAINT pk_guardianstudentcircle PRIMARY KEY (guardianid, studentcircleid);


--
-- TOC entry 2025 (class 2606 OID 16510)
-- Name: pk_kiosk; Type: CONSTRAINT; Schema: dbo; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY kiosk
    ADD CONSTRAINT pk_kiosk PRIMARY KEY (kioskid);


--
-- TOC entry 2037 (class 2606 OID 16567)
-- Name: pk_phoneoperatingsystem; Type: CONSTRAINT; Schema: dbo; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY deviceoperatingsystem
    ADD CONSTRAINT pk_phoneoperatingsystem PRIMARY KEY (deviceoperatingsystemid);


--
-- TOC entry 2011 (class 2606 OID 16439)
-- Name: pk_pickupschedule; Type: CONSTRAINT; Schema: dbo; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY pickupschedule
    ADD CONSTRAINT pk_pickupschedule PRIMARY KEY (pickupscheduleid);


--
-- TOC entry 2023 (class 2606 OID 16499)
-- Name: pk_pickupschedulesession; Type: CONSTRAINT; Schema: dbo; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY pickupschedulesession
    ADD CONSTRAINT pk_pickupschedulesession PRIMARY KEY (pickupschedulesessionid);


--
-- TOC entry 2031 (class 2606 OID 16540)
-- Name: pk_school; Type: CONSTRAINT; Schema: dbo; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY school
    ADD CONSTRAINT pk_school PRIMARY KEY (schoolid);


--
-- TOC entry 2009 (class 2606 OID 16431)
-- Name: pk_schoolgeozone; Type: CONSTRAINT; Schema: dbo; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY schoolgeozone
    ADD CONSTRAINT pk_schoolgeozone PRIMARY KEY (schoolgeozoneid);


--
-- TOC entry 2007 (class 2606 OID 16420)
-- Name: pk_state; Type: CONSTRAINT; Schema: dbo; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY state
    ADD CONSTRAINT pk_state PRIMARY KEY (stateid);


--
-- TOC entry 2021 (class 2606 OID 16491)
-- Name: pk_student; Type: CONSTRAINT; Schema: dbo; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY student
    ADD CONSTRAINT pk_student PRIMARY KEY (studentid);


--
-- TOC entry 2005 (class 2606 OID 16409)
-- Name: pk_studentcircle; Type: CONSTRAINT; Schema: dbo; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY studentcircle
    ADD CONSTRAINT pk_studentcircle PRIMARY KEY (studentcircleid);


--
-- TOC entry 2029 (class 2606 OID 16529)
-- Name: pk_studentkiosknotification; Type: CONSTRAINT; Schema: dbo; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY studentkiosknotification
    ADD CONSTRAINT pk_studentkiosknotification PRIMARY KEY (studentkiosknotificationid);


--
-- TOC entry 2044 (class 2606 OID 16598)
-- Name: fk_geozonepolygonpoint_schoolgeozone; Type: FK CONSTRAINT; Schema: dbo; Owner: postgres
--

ALTER TABLE ONLY geozonepolygonpoint
    ADD CONSTRAINT fk_geozonepolygonpoint_schoolgeozone FOREIGN KEY (geozoneid) REFERENCES schoolgeozone(schoolgeozoneid)
      ON UPDATE NO ACTION ON DELETE CASCADE;


--
-- TOC entry 2055 (class 2606 OID 16653)
-- Name: fk_guardiandevice_deviceoperatingsystem; Type: FK CONSTRAINT; Schema: dbo; Owner: postgres
--

ALTER TABLE ONLY guardiandevice
    ADD CONSTRAINT fk_guardiandevice_deviceoperatingsystem FOREIGN KEY (deviceoperatingsystemid) REFERENCES deviceoperatingsystem(deviceoperatingsystemid)
	ON UPDATE NO ACTION ON DELETE CASCADE;


--
-- TOC entry 2056 (class 2606 OID 16658)
-- Name: fk_guardiandevice_guardian; Type: FK CONSTRAINT; Schema: dbo; Owner: postgres
--

ALTER TABLE ONLY guardiandevice
    ADD CONSTRAINT fk_guardiandevice_guardian FOREIGN KEY (guardianid) REFERENCES guardian(guardianid)
	ON UPDATE NO ACTION ON DELETE CASCADE;


--
-- TOC entry 2053 (class 2606 OID 16643)
-- Name: fk_guardianstudentcircle_guardian; Type: FK CONSTRAINT; Schema: dbo; Owner: postgres
--

ALTER TABLE ONLY guardianstudentcircle
    ADD CONSTRAINT fk_guardianstudentcircle_guardian FOREIGN KEY (guardianid) REFERENCES guardian(guardianid)
	ON UPDATE NO ACTION ON DELETE CASCADE;


--
-- TOC entry 2054 (class 2606 OID 16648)
-- Name: fk_guardianstudentcircle_studentcircle; Type: FK CONSTRAINT; Schema: dbo; Owner: postgres
--

ALTER TABLE ONLY guardianstudentcircle
    ADD CONSTRAINT fk_guardianstudentcircle_studentcircle FOREIGN KEY (studentcircleid) REFERENCES studentcircle(studentcircleid)
	ON UPDATE NO ACTION ON DELETE CASCADE;


--
-- TOC entry 2048 (class 2606 OID 16618)
-- Name: fk_kiosk_kiosk; Type: FK CONSTRAINT; Schema: dbo; Owner: postgres
--

ALTER TABLE ONLY kiosk
    ADD CONSTRAINT fk_kiosk_kiosk FOREIGN KEY (parentkioskid) REFERENCES kiosk(kioskid)
	ON UPDATE NO ACTION ON DELETE CASCADE;


--
-- TOC entry 2049 (class 2606 OID 16623)
-- Name: fk_kiosk_school; Type: FK CONSTRAINT; Schema: dbo; Owner: postgres
--

ALTER TABLE ONLY kiosk
    ADD CONSTRAINT fk_kiosk_school FOREIGN KEY (schoolid) REFERENCES school(schoolid)
	ON UPDATE NO ACTION ON DELETE CASCADE;


--
-- TOC entry 2043 (class 2606 OID 16593)
-- Name: fk_pickupschedule_student; Type: FK CONSTRAINT; Schema: dbo; Owner: postgres
--

ALTER TABLE ONLY pickupschedule
    ADD CONSTRAINT fk_pickupschedule_student FOREIGN KEY (studentid) REFERENCES student(studentid)
	ON UPDATE NO ACTION ON DELETE CASCADE;


--
-- TOC entry 2046 (class 2606 OID 16608)
-- Name: fk_pickupschedulesession_guardian; Type: FK CONSTRAINT; Schema: dbo; Owner: postgres
--

ALTER TABLE ONLY pickupschedulesession
    ADD CONSTRAINT fk_pickupschedulesession_guardian FOREIGN KEY (guardianid) REFERENCES guardian(guardianid)
	ON UPDATE NO ACTION ON DELETE CASCADE;


--
-- TOC entry 2047 (class 2606 OID 16613)
-- Name: fk_pickupschedulesession_pickupschedule; Type: FK CONSTRAINT; Schema: dbo; Owner: postgres
--

ALTER TABLE ONLY pickupschedulesession
    ADD CONSTRAINT fk_pickupschedulesession_pickupschedule FOREIGN KEY (pickupscheduleid) REFERENCES pickupschedule(pickupscheduleid)
	ON UPDATE NO ACTION ON DELETE CASCADE;


--
-- TOC entry 2052 (class 2606 OID 16638)
-- Name: fk_school_state; Type: FK CONSTRAINT; Schema: dbo; Owner: postgres
--

ALTER TABLE ONLY school
    ADD CONSTRAINT fk_school_state FOREIGN KEY (stateid) REFERENCES state(stateid)
	ON UPDATE NO ACTION ON DELETE CASCADE;


--
-- TOC entry 2041 (class 2606 OID 16583)
-- Name: fk_schoolgeozone_geozonetype; Type: FK CONSTRAINT; Schema: dbo; Owner: postgres
--

ALTER TABLE ONLY schoolgeozone
    ADD CONSTRAINT fk_schoolgeozone_geozonetype FOREIGN KEY (geozonetypeid) REFERENCES geozonetype(geozonetypeid)
	ON UPDATE NO ACTION ON DELETE CASCADE;


--
-- TOC entry 2042 (class 2606 OID 16588)
-- Name: fk_schoolgeozone_school; Type: FK CONSTRAINT; Schema: dbo; Owner: postgres
--

ALTER TABLE ONLY schoolgeozone
    ADD CONSTRAINT fk_schoolgeozone_school FOREIGN KEY (schoolid) REFERENCES school(schoolid)
	ON UPDATE NO ACTION ON DELETE CASCADE;


--
-- TOC entry 2040 (class 2606 OID 16578)
-- Name: fk_state_country; Type: FK CONSTRAINT; Schema: dbo; Owner: postgres
--

ALTER TABLE ONLY state
    ADD CONSTRAINT fk_state_country FOREIGN KEY (countryid) REFERENCES country(countryid)
	ON UPDATE NO ACTION ON DELETE CASCADE;


--
-- TOC entry 2045 (class 2606 OID 16603)
-- Name: fk_student_school; Type: FK CONSTRAINT; Schema: dbo; Owner: postgres
--

ALTER TABLE ONLY student
    ADD CONSTRAINT fk_student_school FOREIGN KEY (schoolid) REFERENCES school(schoolid)
	ON UPDATE NO ACTION ON DELETE CASCADE;


--
-- TOC entry 2038 (class 2606 OID 16568)
-- Name: fk_studentcircle_circletype; Type: FK CONSTRAINT; Schema: dbo; Owner: postgres
--

ALTER TABLE ONLY studentcircle
    ADD CONSTRAINT fk_studentcircle_circletype FOREIGN KEY (circletypeid) REFERENCES circletype(circletypeid)
	ON UPDATE NO ACTION ON DELETE CASCADE;


--
-- TOC entry 2039 (class 2606 OID 16573)
-- Name: fk_studentcircle_student; Type: FK CONSTRAINT; Schema: dbo; Owner: postgres
--

ALTER TABLE ONLY studentcircle
    ADD CONSTRAINT fk_studentcircle_student FOREIGN KEY (studentid) REFERENCES student(studentid)
	ON UPDATE NO ACTION ON DELETE CASCADE;


--
-- TOC entry 2050 (class 2606 OID 16628)
-- Name: fk_studentkiosknotification_kiosk; Type: FK CONSTRAINT; Schema: dbo; Owner: postgres
--

ALTER TABLE ONLY studentkiosknotification
    ADD CONSTRAINT fk_studentkiosknotification_kiosk FOREIGN KEY (kioskid) REFERENCES kiosk(kioskid)
	ON UPDATE NO ACTION ON DELETE CASCADE;

--
-- TOC entry 2051 (class 2606 OID 16633)
-- Name: fk_studentkiosknotification_student; Type: FK CONSTRAINT; Schema: dbo; Owner: postgres
--

ALTER TABLE ONLY studentkiosknotification
    ADD CONSTRAINT fk_studentkiosknotification_student FOREIGN KEY (studentid) REFERENCES student(studentid)
	ON UPDATE NO ACTION ON DELETE CASCADE;


--
-- TOC entry 2204 (class 0 OID 0)
-- Dependencies: 7
-- Name: dbo; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA dbo FROM PUBLIC;
REVOKE ALL ON SCHEMA dbo FROM postgres;
GRANT ALL ON SCHEMA dbo TO postgres;
GRANT ALL ON SCHEMA dbo TO web;


--
-- TOC entry 2206 (class 0 OID 0)
-- Dependencies: 5
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- TOC entry 2208 (class 0 OID 0)
-- Dependencies: 188
-- Name: circletype; Type: ACL; Schema: dbo; Owner: postgres
--

REVOKE ALL ON TABLE circletype FROM PUBLIC;
REVOKE ALL ON TABLE circletype FROM postgres;
GRANT ALL ON TABLE circletype TO postgres;
GRANT ALL ON TABLE circletype TO web;


--
-- TOC entry 2210 (class 0 OID 0)
-- Dependencies: 187
-- Name: circletype_circletypeid_seq; Type: ACL; Schema: dbo; Owner: postgres
--

REVOKE ALL ON SEQUENCE circletype_circletypeid_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE circletype_circletypeid_seq FROM postgres;
GRANT ALL ON SEQUENCE circletype_circletypeid_seq TO postgres;
GRANT ALL ON SEQUENCE circletype_circletypeid_seq TO web;


--
-- TOC entry 2211 (class 0 OID 0)
-- Dependencies: 186
-- Name: country; Type: ACL; Schema: dbo; Owner: postgres
--

REVOKE ALL ON TABLE country FROM PUBLIC;
REVOKE ALL ON TABLE country FROM postgres;
GRANT ALL ON TABLE country TO postgres;
GRANT ALL ON TABLE country TO web;


--
-- TOC entry 2213 (class 0 OID 0)
-- Dependencies: 185
-- Name: country_countryid_seq; Type: ACL; Schema: dbo; Owner: postgres
--

REVOKE ALL ON SEQUENCE country_countryid_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE country_countryid_seq FROM postgres;
GRANT ALL ON SEQUENCE country_countryid_seq TO postgres;
GRANT ALL ON SEQUENCE country_countryid_seq TO web;


--
-- TOC entry 2214 (class 0 OID 0)
-- Dependencies: 205
-- Name: deviceoperatingsystem; Type: ACL; Schema: dbo; Owner: postgres
--

REVOKE ALL ON TABLE deviceoperatingsystem FROM PUBLIC;
REVOKE ALL ON TABLE deviceoperatingsystem FROM postgres;
GRANT ALL ON TABLE deviceoperatingsystem TO postgres;
GRANT ALL ON TABLE deviceoperatingsystem TO web;


--
-- TOC entry 2216 (class 0 OID 0)
-- Dependencies: 204
-- Name: deviceoperatingsystem_deviceoperatingsystemid_seq; Type: ACL; Schema: dbo; Owner: postgres
--

REVOKE ALL ON SEQUENCE deviceoperatingsystem_deviceoperatingsystemid_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE deviceoperatingsystem_deviceoperatingsystemid_seq FROM postgres;
GRANT ALL ON SEQUENCE deviceoperatingsystem_deviceoperatingsystemid_seq TO postgres;
GRANT ALL ON SEQUENCE deviceoperatingsystem_deviceoperatingsystemid_seq TO web;


--
-- TOC entry 2217 (class 0 OID 0)
-- Dependencies: 184
-- Name: geozonepolygonpoint; Type: ACL; Schema: dbo; Owner: postgres
--

REVOKE ALL ON TABLE geozonepolygonpoint FROM PUBLIC;
REVOKE ALL ON TABLE geozonepolygonpoint FROM postgres;
GRANT ALL ON TABLE geozonepolygonpoint TO postgres;
GRANT ALL ON TABLE geozonepolygonpoint TO web;


--
-- TOC entry 2219 (class 0 OID 0)
-- Dependencies: 183
-- Name: geozonepolygonpoint_geozonepolygonpointid_seq; Type: ACL; Schema: dbo; Owner: postgres
--

REVOKE ALL ON SEQUENCE geozonepolygonpoint_geozonepolygonpointid_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE geozonepolygonpoint_geozonepolygonpointid_seq FROM postgres;
GRANT ALL ON SEQUENCE geozonepolygonpoint_geozonepolygonpointid_seq TO postgres;
GRANT ALL ON SEQUENCE geozonepolygonpoint_geozonepolygonpointid_seq TO web;


--
-- TOC entry 2220 (class 0 OID 0)
-- Dependencies: 182
-- Name: geozonetype; Type: ACL; Schema: dbo; Owner: postgres
--

REVOKE ALL ON TABLE geozonetype FROM PUBLIC;
REVOKE ALL ON TABLE geozonetype FROM postgres;
GRANT ALL ON TABLE geozonetype TO postgres;
GRANT ALL ON TABLE geozonetype TO web;


--
-- TOC entry 2222 (class 0 OID 0)
-- Dependencies: 181
-- Name: geozonetype_geozonetypeid_seq; Type: ACL; Schema: dbo; Owner: postgres
--

REVOKE ALL ON SEQUENCE geozonetype_geozonetypeid_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE geozonetype_geozonetypeid_seq FROM postgres;
GRANT ALL ON SEQUENCE geozonetype_geozonetypeid_seq TO postgres;
GRANT ALL ON SEQUENCE geozonetype_geozonetypeid_seq TO web;


--
-- TOC entry 2223 (class 0 OID 0)
-- Dependencies: 196
-- Name: guardian; Type: ACL; Schema: dbo; Owner: postgres
--

REVOKE ALL ON TABLE guardian FROM PUBLIC;
REVOKE ALL ON TABLE guardian FROM postgres;
GRANT ALL ON TABLE guardian TO postgres;
GRANT ALL ON TABLE guardian TO web;


--
-- TOC entry 2225 (class 0 OID 0)
-- Dependencies: 195
-- Name: guardian_guardianid_seq; Type: ACL; Schema: dbo; Owner: postgres
--

REVOKE ALL ON SEQUENCE guardian_guardianid_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE guardian_guardianid_seq FROM postgres;
GRANT ALL ON SEQUENCE guardian_guardianid_seq TO postgres;
GRANT ALL ON SEQUENCE guardian_guardianid_seq TO web;


--
-- TOC entry 2226 (class 0 OID 0)
-- Dependencies: 203
-- Name: guardiandevice; Type: ACL; Schema: dbo; Owner: postgres
--

REVOKE ALL ON TABLE guardiandevice FROM PUBLIC;
REVOKE ALL ON TABLE guardiandevice FROM postgres;
GRANT ALL ON TABLE guardiandevice TO postgres;
GRANT ALL ON TABLE guardiandevice TO web;


--
-- TOC entry 2228 (class 0 OID 0)
-- Dependencies: 202
-- Name: guardiandevice_guardiandeviceid_seq; Type: ACL; Schema: dbo; Owner: postgres
--

REVOKE ALL ON SEQUENCE guardiandevice_guardiandeviceid_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE guardiandevice_guardiandeviceid_seq FROM postgres;
GRANT ALL ON SEQUENCE guardiandevice_guardiandeviceid_seq TO postgres;
GRANT ALL ON SEQUENCE guardiandevice_guardiandeviceid_seq TO web;


--
-- TOC entry 2229 (class 0 OID 0)
-- Dependencies: 201
-- Name: guardianstudentcircle; Type: ACL; Schema: dbo; Owner: postgres
--

REVOKE ALL ON TABLE guardianstudentcircle FROM PUBLIC;
REVOKE ALL ON TABLE guardianstudentcircle FROM postgres;
GRANT ALL ON TABLE guardianstudentcircle TO postgres;
GRANT ALL ON TABLE guardianstudentcircle TO web;


--
-- TOC entry 2230 (class 0 OID 0)
-- Dependencies: 194
-- Name: kiosk; Type: ACL; Schema: dbo; Owner: postgres
--

REVOKE ALL ON TABLE kiosk FROM PUBLIC;
REVOKE ALL ON TABLE kiosk FROM postgres;
GRANT ALL ON TABLE kiosk TO postgres;
GRANT ALL ON TABLE kiosk TO web;


--
-- TOC entry 2232 (class 0 OID 0)
-- Dependencies: 193
-- Name: kiosk_kioskid_seq; Type: ACL; Schema: dbo; Owner: postgres
--

REVOKE ALL ON SEQUENCE kiosk_kioskid_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE kiosk_kioskid_seq FROM postgres;
GRANT ALL ON SEQUENCE kiosk_kioskid_seq TO postgres;
GRANT ALL ON SEQUENCE kiosk_kioskid_seq TO web;


--
-- TOC entry 2233 (class 0 OID 0)
-- Dependencies: 180
-- Name: pickupschedule; Type: ACL; Schema: dbo; Owner: postgres
--

REVOKE ALL ON TABLE pickupschedule FROM PUBLIC;
REVOKE ALL ON TABLE pickupschedule FROM postgres;
GRANT ALL ON TABLE pickupschedule TO postgres;
GRANT ALL ON TABLE pickupschedule TO web;


--
-- TOC entry 2235 (class 0 OID 0)
-- Dependencies: 179
-- Name: pickupschedule_pickupscheduleid_seq; Type: ACL; Schema: dbo; Owner: postgres
--

REVOKE ALL ON SEQUENCE pickupschedule_pickupscheduleid_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE pickupschedule_pickupscheduleid_seq FROM postgres;
GRANT ALL ON SEQUENCE pickupschedule_pickupscheduleid_seq TO postgres;
GRANT ALL ON SEQUENCE pickupschedule_pickupscheduleid_seq TO web;


--
-- TOC entry 2236 (class 0 OID 0)
-- Dependencies: 192
-- Name: pickupschedulesession; Type: ACL; Schema: dbo; Owner: postgres
--

REVOKE ALL ON TABLE pickupschedulesession FROM PUBLIC;
REVOKE ALL ON TABLE pickupschedulesession FROM postgres;
GRANT ALL ON TABLE pickupschedulesession TO postgres;
GRANT ALL ON TABLE pickupschedulesession TO web;


--
-- TOC entry 2238 (class 0 OID 0)
-- Dependencies: 191
-- Name: pickupschedulesession_pickupschedulesessionid_seq; Type: ACL; Schema: dbo; Owner: postgres
--

REVOKE ALL ON SEQUENCE pickupschedulesession_pickupschedulesessionid_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE pickupschedulesession_pickupschedulesessionid_seq FROM postgres;
GRANT ALL ON SEQUENCE pickupschedulesession_pickupschedulesessionid_seq TO postgres;
GRANT ALL ON SEQUENCE pickupschedulesession_pickupschedulesessionid_seq TO web;


--
-- TOC entry 2239 (class 0 OID 0)
-- Dependencies: 200
-- Name: school; Type: ACL; Schema: dbo; Owner: postgres
--

REVOKE ALL ON TABLE school FROM PUBLIC;
REVOKE ALL ON TABLE school FROM postgres;
GRANT ALL ON TABLE school TO postgres;
GRANT ALL ON TABLE school TO web;


--
-- TOC entry 2241 (class 0 OID 0)
-- Dependencies: 199
-- Name: school_schoolid_seq; Type: ACL; Schema: dbo; Owner: postgres
--

REVOKE ALL ON SEQUENCE school_schoolid_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE school_schoolid_seq FROM postgres;
GRANT ALL ON SEQUENCE school_schoolid_seq TO postgres;
GRANT ALL ON SEQUENCE school_schoolid_seq TO web;


--
-- TOC entry 2242 (class 0 OID 0)
-- Dependencies: 178
-- Name: schoolgeozone; Type: ACL; Schema: dbo; Owner: postgres
--

REVOKE ALL ON TABLE schoolgeozone FROM PUBLIC;
REVOKE ALL ON TABLE schoolgeozone FROM postgres;
GRANT ALL ON TABLE schoolgeozone TO postgres;
GRANT ALL ON TABLE schoolgeozone TO web;


--
-- TOC entry 2244 (class 0 OID 0)
-- Dependencies: 177
-- Name: schoolgeozone_schoolgeozoneid_seq; Type: ACL; Schema: dbo; Owner: postgres
--

REVOKE ALL ON SEQUENCE schoolgeozone_schoolgeozoneid_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE schoolgeozone_schoolgeozoneid_seq FROM postgres;
GRANT ALL ON SEQUENCE schoolgeozone_schoolgeozoneid_seq TO postgres;
GRANT ALL ON SEQUENCE schoolgeozone_schoolgeozoneid_seq TO web;


--
-- TOC entry 2245 (class 0 OID 0)
-- Dependencies: 176
-- Name: state; Type: ACL; Schema: dbo; Owner: postgres
--

REVOKE ALL ON TABLE state FROM PUBLIC;
REVOKE ALL ON TABLE state FROM postgres;
GRANT ALL ON TABLE state TO postgres;
GRANT ALL ON TABLE state TO web;


--
-- TOC entry 2247 (class 0 OID 0)
-- Dependencies: 175
-- Name: state_stateid_seq; Type: ACL; Schema: dbo; Owner: postgres
--

REVOKE ALL ON SEQUENCE state_stateid_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE state_stateid_seq FROM postgres;
GRANT ALL ON SEQUENCE state_stateid_seq TO postgres;
GRANT ALL ON SEQUENCE state_stateid_seq TO web;


--
-- TOC entry 2248 (class 0 OID 0)
-- Dependencies: 190
-- Name: student; Type: ACL; Schema: dbo; Owner: postgres
--

REVOKE ALL ON TABLE student FROM PUBLIC;
REVOKE ALL ON TABLE student FROM postgres;
GRANT ALL ON TABLE student TO postgres;
GRANT ALL ON TABLE student TO web;


--
-- TOC entry 2250 (class 0 OID 0)
-- Dependencies: 189
-- Name: student_studentid_seq; Type: ACL; Schema: dbo; Owner: postgres
--

REVOKE ALL ON SEQUENCE student_studentid_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE student_studentid_seq FROM postgres;
GRANT ALL ON SEQUENCE student_studentid_seq TO postgres;
GRANT ALL ON SEQUENCE student_studentid_seq TO web;


--
-- TOC entry 2251 (class 0 OID 0)
-- Dependencies: 174
-- Name: studentcircle; Type: ACL; Schema: dbo; Owner: postgres
--

REVOKE ALL ON TABLE studentcircle FROM PUBLIC;
REVOKE ALL ON TABLE studentcircle FROM postgres;
GRANT ALL ON TABLE studentcircle TO postgres;
GRANT ALL ON TABLE studentcircle TO web;


--
-- TOC entry 2253 (class 0 OID 0)
-- Dependencies: 173
-- Name: studentcircle_studentcircleid_seq; Type: ACL; Schema: dbo; Owner: postgres
--

REVOKE ALL ON SEQUENCE studentcircle_studentcircleid_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE studentcircle_studentcircleid_seq FROM postgres;
GRANT ALL ON SEQUENCE studentcircle_studentcircleid_seq TO postgres;
GRANT ALL ON SEQUENCE studentcircle_studentcircleid_seq TO web;


--
-- TOC entry 2254 (class 0 OID 0)
-- Dependencies: 198
-- Name: studentkiosknotification; Type: ACL; Schema: dbo; Owner: postgres
--

REVOKE ALL ON TABLE studentkiosknotification FROM PUBLIC;
REVOKE ALL ON TABLE studentkiosknotification FROM postgres;
GRANT ALL ON TABLE studentkiosknotification TO postgres;
GRANT ALL ON TABLE studentkiosknotification TO web;


--
-- TOC entry 2256 (class 0 OID 0)
-- Dependencies: 197
-- Name: studentkiosknotification_studentkiosknotificationid_seq; Type: ACL; Schema: dbo; Owner: postgres
--

REVOKE ALL ON SEQUENCE studentkiosknotification_studentkiosknotificationid_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE studentkiosknotification_studentkiosknotificationid_seq FROM postgres;
GRANT ALL ON SEQUENCE studentkiosknotification_studentkiosknotificationid_seq TO postgres;
GRANT ALL ON SEQUENCE studentkiosknotification_studentkiosknotificationid_seq TO web;


-- Completed on 2015-07-07 12:49:29

--
-- PostgreSQL database dump complete
--

\connect template1

SET default_transaction_read_only = off;

--
-- PostgreSQL database dump
--

-- Dumped from database version 9.4.4
-- Dumped by pg_dump version 9.4.0
-- Started on 2015-07-07 12:49:29

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 1989 (class 1262 OID 1)
-- Dependencies: 1988
-- Name: template1; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON DATABASE template1 IS 'default template for new databases';


--
-- TOC entry 172 (class 3079 OID 11855)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 1992 (class 0 OID 0)
-- Dependencies: 172
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- TOC entry 1991 (class 0 OID 0)
-- Dependencies: 5
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2015-07-07 12:49:29

--
-- PostgreSQL database dump complete
--

-- Completed on 2015-07-07 12:49:29

--
-- PostgreSQL database cluster dump complete
--

