using System.Collections.Generic;

namespace StudentPickup.Core.PushNotifications
{
    /// <summary>
    /// An interface for push notification adapters to connect to mobile push notification services
    /// </summary>
    public interface IPushNotificationAdapter
    {
        /// <summary>
        /// Send a push notification to the specified device
        /// </summary>
        /// <param name="deviceId">The unique identifier for the device registered with the service</param>
        /// <param name="pushNotificationType">The type of the push notification to send</param>
        /// <param name="alert">The alert message in the push notitification</param>
        bool SendPushNotification(string deviceId, PushNotificationTypes pushNotificationType, string alert);

        /// <summary>
        /// Send a push notification to the specified device
        /// </summary>
        /// <param name="deviceId">The unique identifier for the device registered with the service</param>
        /// <param name="pushNotificationType">The type of the push notification to send</param>
        /// <param name="alert">The alert message in the push notitification</param>
        /// <param name="customData">The custom data to be sent to the device as JSON</param>
        bool SendPushNotification(string deviceId, PushNotificationTypes pushNotificationType, string alert, Dictionary<string,string> customData);
    }
}