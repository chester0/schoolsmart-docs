﻿namespace StudentPickup.Core.Models
{
    public class KioskResponse
    {
        /*  Tone rules (may be extracted into an enum later)
         *  0 = None
         *  1 = Very Positive
         *  2 = Negative
         *  3 = Very Negative
         */
        public int Tone { get; set; }
        public bool ResponseRequired { get; set; }
        public string Message { get; set; }
        public string ButtonOneText { get; set; }
        public string ButtonTwoText { get; set; }
        public string ButtonThreeText { get; set; }
        // we should probably add a service trigger here, as a catch for comms errors and such
    }
}
