﻿using System.Threading.Tasks;

namespace StudentPickup.Core.Mapping
{
    /// <summary>
    /// A mapping adapter that provides mapping based calculations
    /// </summary>
    public interface IMapsTarget
    {
        /// <summary>
        /// Uses a mapping service to calculate the ETA time between two geographical points
        /// </summary>
        /// <param name="srcLat">Source Latitude</param>
        /// <param name="srcLon">Source Longitude</param>
        /// <param name="destLat">Destination Latitude</param>
        /// <param name="destLon">Destination Longitude</param>
        /// <returns>Estimated time of arrival in seconds.</returns>
        Task<int> GetEtaBetweenPoints(decimal? srcLat, decimal? srcLon, decimal? destLat, decimal? destLon);
    }
}