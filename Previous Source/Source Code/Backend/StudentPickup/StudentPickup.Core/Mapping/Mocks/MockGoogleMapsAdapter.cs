﻿using System.Threading.Tasks;

namespace StudentPickup.Core.Mapping.Mocks
{
    /// <summary>
    /// Class mocking the functionality of the GoogleMapsApi Wrapper Adapter
    /// </summary>
    public class MockMapsAdapter : IMapsTarget
    {
        public Task<int> GetEtaBetweenPoints(decimal? srcLat, decimal? srcLon, decimal? destLat, decimal? destLon)
        {
            var gmaps = new MockMapsAdaptee();
            return gmaps.GetEtaBetweenPoints(srcLat, srcLon, destLat, destLon);
        }
    }
}