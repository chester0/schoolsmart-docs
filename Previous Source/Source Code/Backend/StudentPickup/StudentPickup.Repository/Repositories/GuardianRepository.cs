﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Repository.Pattern.Repositories;
using StudentPickup.Data;

namespace StudentPickup.Repository.Repositories
{
    /// <summary>
    /// The repository to access the Guardian table in the database
    /// </summary>
    public static class GuardianRepository
    {
        /// <summary>
        /// Updates the guardians last known location
        /// </summary>
        /// <param name="repository">The repository to update the Guardian in</param>
        /// <param name="guardianId">The unique identifier of the Guardian to update</param>
        /// <param name="latitude">The latitude of the Guardians location</param>
        /// <param name="longitude">The longitude of the Guardians location</param>
        /// <returns>The guardian with the updated location</returns>
        public static async Task<Guardian> UpdateGuardianLocation(this IRepositoryAsync<Guardian> repository, int guardianId,
            decimal latitude, decimal longitude)
        {
            var guardian = await repository.FindAsync(guardianId);
            if (guardian == null)
                throw new ArgumentException("Guardian doesn't exist.");

            guardian.LastKnownLatitude = latitude;
            guardian.LastKnownLongtitude = longitude;
            guardian.LastRecordedLocationTimestamp = DateTime.UtcNow;

            repository.Update(guardian);

            return guardian;
        }

        /// <summary>
        /// Gets the guardian by their id if their location has been updated within the past two minutes
        /// </summary>
        /// <param name="repository">The repository to find the guardian in</param>
        /// <param name="guardianId">The unique identifier of the guardian to search for</param>
        /// <returns>The object of the guardian if their location has been updated today, 
        /// Null if the guardian with that id hasn't had their location updated today.</returns>
        public static async Task<Guardian> GetGuardianUpdatedRecentlyById(this IRepositoryAsync<Guardian> repository,
            int guardianId)
        {
            var twoMinutesAgo = DateTime.UtcNow.AddMinutes(-2);
            return await repository.Queryable()
                    .SingleOrDefaultAsync(
                        g =>
                            g.GuardianId == guardianId &&
                            g.LastRecordedLocationTimestamp.HasValue &&
                            g.LastRecordedLocationTimestamp > twoMinutesAgo);
        }

        /// <summary>
        /// Get the guardian scheduled to pickup the student today
        /// </summary>
        /// <param name="repository">The repository to find the guardian in</param>
        /// <param name="studentId">The unique identifier of the student</param>
        /// <returns>The guardian scheduled to pickup the student today.</returns>
        public static async Task<Guardian> GetScheduledGuardianTodayByStudentId(this IRepositoryAsync<Guardian> repository, int studentId)
        {
            return await repository
                .Queryable()
                .FirstOrDefaultAsync(guardian => guardian.PickupScheduleSessions.Any(
                    pickupScheduleSession => pickupScheduleSession.PickupSchedule.StudentId == studentId &&
                                             DbFunctions.TruncateTime(pickupScheduleSession.SessionDate) ==
                                             DbFunctions.TruncateTime(DateTime.Now)));
        }

        /// <summary>
        /// Gets a list of all guardians in a trusted circle corresponding to the passed studentId
        /// </summary>
        /// <param name="repository">The repository to find the guardian in</param>
        /// <param name="studentId">The unique identifier of the student that owns the trusted circle</param>
        /// <returns>A list of guardians that belong to the trusted circle</returns>
        public async static Task<List<Guardian>> GetTrustedGuardiansByStudentId(this IRepositoryAsync<Guardian> repository, int studentId)
        {
            return await repository.Queryable()
                .Where(
                    guardian => guardian.GuardianStudentCircles
                        .Any(
                            studentCircle =>
                                studentCircle.StudentCircle.CircleTypeId == CircleTypes.Trusted &&
                                studentCircle.StudentCircle.StudentId == studentId))
                .ToListAsync();
        }

        /// <summary>
        /// Retrieves the guardian associated with the phone number
        /// </summary>
        /// <param name="repository">The repository to find the guardian in</param>
        /// <param name="mobileNumber">The mobile number of the guardian to find</param>
        /// <returns>Guardian associated with phone number</returns>
        public async static Task<Guardian> GetGuardianByMobileNumber(this IRepositoryAsync<Guardian> repository, string mobileNumber)
        {
            return await repository
                .Queryable()
                .SingleAsync(guardian => guardian.MobileNumber == mobileNumber);
        }

        /// <summary>
        /// Gets the guardians in the students specific circle
        /// </summary>
        /// <param name="repository">The repository to find the guardians in</param>
        /// <param name="studentId">The unique identifier of the student</param>
        /// <param name="circleType">The type of circle to find the guardians in</param>
        /// <returns>The guardians in the students circle</returns>
        public async static Task<List<Guardian>> GetGuardiansInCircleByStudentId(this IRepositoryAsync<Guardian> repository,
            int studentId, CircleTypes circleType)
        {
            return await repository
                .Queryable()
                .Where(
                    g =>
                        g.GuardianStudentCircles.Any(
                            c => c.StudentCircle.StudentId == studentId && c.StudentCircle.CircleTypeId == circleType))
                .ToListAsync();
        }

        /// <summary>
        /// Gets the guardians in the students specific circle by the students pickup schedule
        /// </summary>
        /// <param name="repository">The repository to find the guardians in</param>
        /// <param name="pickupScheduleId">The unique identifier of the pickup schedule</param>
        /// <returns>The guardians in the students circle</returns>
        public async static Task<List<Guardian>> GetGuardiansInCircleByPickupScheduleId(this IRepositoryAsync<Guardian> repository,
            int pickupScheduleId)
        {
            return await repository
                .Queryable()
                .Where(
                    g =>
                        g.GuardianStudentCircles.Any(
                            c =>
                                c.StudentCircle.Student.PickupSchedules.Any(s => s.PickupScheduleId == pickupScheduleId)))
                .ToListAsync();
        }
    }
}
