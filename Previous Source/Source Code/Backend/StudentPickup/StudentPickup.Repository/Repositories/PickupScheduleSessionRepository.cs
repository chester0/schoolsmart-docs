﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Repository.Pattern.Repositories;
using StudentPickup.Data;

namespace StudentPickup.Repository.Repositories
{
    /// <summary>
    /// The repository to access the Guardian table in the database
    /// </summary>
    public static class PickupScheduleSessionRepository
    {
        /// <summary>
        /// Gets the latest schedules of the guardian by Id including the students, schools and schools geo zone details
        /// </summary>
        /// <param name="repository">The repository to find the schedules in in</param>
        /// <param name="guardianId">The unique identifier of the guardian to search for</param>
        /// <returns>Guardian with details</returns>
        public async static Task<List<PickupScheduleSession>> GetLatestSchedulesByGuardianId(this IRepositoryAsync<PickupScheduleSession> repository,
            int guardianId)
        {
            return await repository
                .Queryable()
                .Include(
                    pickupScheduleSession =>
                        pickupScheduleSession.PickupSchedule.Student.School.SchoolGeoZones.Select(
                            schoolGeoZone => schoolGeoZone.GeoZonePolygonPoints
                            )
                )
                .Where(
                    pickupScheduleSession => pickupScheduleSession.GuardianId == guardianId
                                             && DbFunctions.TruncateTime(pickupScheduleSession.SessionDate) ==
                                             DbFunctions.TruncateTime(
                                                 pickupScheduleSession.Guardian.PickupScheduleSessions
                                                     .Max(s => s.SessionDate)
                                                 )
                )
                .ToListAsync();
        }
    }
}
