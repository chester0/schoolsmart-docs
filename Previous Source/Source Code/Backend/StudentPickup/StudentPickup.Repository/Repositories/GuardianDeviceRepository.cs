﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Repository.Pattern.Ef6;
using Repository.Pattern.Repositories;
using StudentPickup.Data;

namespace StudentPickup.Repository.Repositories
{
    /// <summary>
    /// The repository to access the Guardian Device table in the database
    /// </summary>
    public static class GuardianDeviceRepository
    {
        /// <summary>
        /// Gets all devices for the guardian scheduled to pickup the student today
        /// </summary>
        /// <param name="repository">The generic Guardian Device repository to be injected</param>
        /// <param name="guardianId">The id of the guardian to find the devices for</param>
        /// <returns>An enumeration of guardian devices</returns>
        public static async Task<List<GuardianDevice>> GetScheduledGuardianDeviceByGuardian(this IRepositoryAsync<GuardianDevice> repository, int guardianId)
        {
            return await repository
                .Queryable()
                .Where(
                    guardianDevice =>
                        guardianDevice.Guardian.PickupScheduleSessions.Any(
                            pickupScheduleSession => pickupScheduleSession.GuardianId == guardianId &&
                                                     DbFunctions.TruncateTime(pickupScheduleSession.SessionDate) ==
                                                     DbFunctions.TruncateTime(DateTime.Now)
                            )
                )
                .ToListAsync();
        }

        /// <summary>
        /// If the guardian doesn't already have a registered device, register one. If they do update the existing device
        /// with the new device app id and operating system details.
        /// </summary>
        /// <param name="repository">The generic Guardian Device repository to be injected</param>
        /// <param name="guardianId">Guardian who owns the device</param>
        /// <param name="deviceAppId">The registered ID with the device push notification service</param>
        /// <param name="deviceOperatingSystemId">The operating system of the device</param>
        /// <returns>New or updated device for guardian</returns>
        public static async Task<GuardianDevice> UpsertGuardianDevice(this IRepositoryAsync<GuardianDevice> repository,
            int guardianId, string deviceAppId, DeviceOperatingSystems deviceOperatingSystemId)
        {
            var guardianDevice = await repository
                .Queryable()
                .Where(gd => gd.GuardianId == guardianId)
                .SingleOrDefaultAsync();

            // If the guardian doesn't have a device insert, otherwise update
            if (guardianDevice == null)
            {
                // Insert new device
                guardianDevice = new GuardianDevice
                {
                    GuardianId = guardianId,
                    DeviceAppId = deviceAppId,
                    DeviceOperatingSystemId = deviceOperatingSystemId
                };

                repository.Insert(guardianDevice);
            }
            else
            {
                // Update existing device
                guardianDevice.DeviceAppId = deviceAppId;
                guardianDevice.DeviceOperatingSystemId = deviceOperatingSystemId;

                repository.Update(guardianDevice);
            }

            return guardianDevice;
        }
        
        /// <summary>
        /// Updates the device subscription id based on the old subscription id
        /// </summary>
        /// <param name="repository">The repository to update the device in</param>
        /// <param name="oldSubscriptionId">Previous device subscription id</param>
        /// <param name="newSubscriptionId">New device subscription id</param>
        /// <returns>Updated guardian device</returns>
        public static async Task<GuardianDevice> UpdateGuardianDeviceSubscriptionId(this IRepositoryAsync<GuardianDevice> repository, 
            string oldSubscriptionId, string newSubscriptionId)
        {
            var guardianDevice = await repository
                .Queryable()
                .Where(g => g.DeviceAppId == oldSubscriptionId)
                .SingleAsync();

            guardianDevice.DeviceAppId = newSubscriptionId;

            repository.Update(guardianDevice);
            return guardianDevice;
        }

        /// <summary>
        /// Updates the device subscription id based on the old subscription id
        /// </summary>
        /// <param name="repository">The repository to update the device in</param>
        /// <param name="subscriptionId">Previous device subscription id</param>
        /// <returns>Updated guardian device</returns>
        public static async Task<GuardianDevice> RemoveGuardianDeviceBySubscriptionId(this IRepositoryAsync<GuardianDevice> repository,
            string subscriptionId)
        {
            var guardianDevice = await repository
                .Queryable()
                .Where(g => g.DeviceAppId == subscriptionId)
                .SingleAsync();
            
            repository.Delete(guardianDevice);
            return guardianDevice;
        }
    }
}
