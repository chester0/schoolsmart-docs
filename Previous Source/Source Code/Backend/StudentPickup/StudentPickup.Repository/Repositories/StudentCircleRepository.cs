﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Runtime.Remoting.Channels;
using System.Threading.Tasks;
using Repository.Pattern.Repositories;
using StudentPickup.Data;

namespace StudentPickup.Repository.Repositories
{
    /// <summary>
    /// The repository to access the Student Circle table in the database
    /// </summary>
    public static class StudentCircleRepository
    {
        /// <summary>
        /// Gets a specific student circle by type
        /// </summary>
        /// <param name="repository">The repository to get the Student Circle from</param>
        /// <param name="studentId">Unique identifier of student to get circle for</param>
        /// <param name="circleType">The type of circle to get</param>
        /// <returns>Particular circle for the student</returns>
        public static async Task<StudentCircle> GetStudentCircleByStudentId(
            this IRepositoryAsync<StudentCircle> repository, int studentId, CircleTypes circleType)
        {
            var trustedCircle = await repository.Queryable()
                .Where(s => s.StudentId == studentId && s.CircleTypeId == circleType)
                .SingleOrDefaultAsync();

            if (trustedCircle != null) return trustedCircle;

            var circleName = "";
            switch (circleType)
            {
                case CircleTypes.Trusted:
                    circleName = "Trusted Circle";
                    break;
                case CircleTypes.Scheduled:
                    circleName = "Scheduled Circle";
                    break;
            }

            // If a circle for the requested type doesn't exist, then create it.
            // This could also be done when the student is created however if in the future
            // more circle types are added this will be needed anyway
            trustedCircle = new StudentCircle
            {
                StudentId = studentId,
                CircleTypeId = circleType,
                Name = circleName
            };
            repository.Insert(trustedCircle);
            return trustedCircle;
        }
    }
}
