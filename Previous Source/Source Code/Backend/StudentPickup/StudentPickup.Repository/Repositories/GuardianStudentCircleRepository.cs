﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Repository.Pattern.Repositories;
using StudentPickup.Data;

namespace StudentPickup.Repository.Repositories
{
    /// <summary>
    /// The repository to access the Guardian Student Circle table in the database
    /// </summary>
    public static class GuardianStudentCircleRepository
    {
        /// <summary>
        /// Checks if a guardian exists in a particular student circle
        /// </summary>
        /// <param name="repository">The repository to get the Student Circle from</param>
        /// <param name="guardianId">Unqique identifier of guardian to find</param>
        /// <param name="studentCircleId">Unqique identifier of student circle to find guardian in</param>
        /// <returns>Whether the guardian exists in the students particular circle</returns>
        public static async Task<bool> GuardianExistsInStudentCircle(
            this IRepositoryAsync<GuardianStudentCircle> repository, int studentCircleId, int guardianId)
        {
            return await repository.Queryable()
                .AnyAsync(
                    g =>
                        g.GuardianId == guardianId && g.StudentCircleId == studentCircleId);
        }

        /// <summary>
        /// Removes a guardian from the students particular circle
        /// </summary>
        /// <param name="repository">The repository to get the Student Circle from</param>
        /// <param name="studentCircleId">Unqique identifier of student circle to find guardian in</param>
        /// <param name="guardianId">Unqique identifier of guardian to remove</param>
        /// <returns></returns>
        public static async Task RemoveGuardianFromStudentCircle(
            this IRepositoryAsync<GuardianStudentCircle> repository, int studentCircleId, int guardianId)
        {
            var guardianStudentCircle = await repository.Queryable()
                .Where(
                    g =>
                        g.GuardianId == guardianId && g.StudentCircleId == studentCircleId)
                .SingleOrDefaultAsync();

            if(guardianStudentCircle == null)
                throw new Exception("Guardian doesn't exist in students circle");

            repository.Delete(guardianStudentCircle);
        }

    }
}
