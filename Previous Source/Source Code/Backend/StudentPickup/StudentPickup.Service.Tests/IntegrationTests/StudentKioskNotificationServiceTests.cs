﻿using System;
using System.IO;
using Effort;
using Effort.DataLoaders;
using Repository.Pattern.Ef6;
using StudentPickup.Core.Mapping;
using StudentPickup.Core.Mapping.Mocks;
using StudentPickup.Core.Models;
using StudentPickup.Core.PushNotifications;
using StudentPickup.Core.PushNotifications.Mocks;
using StudentPickup.Data;
using Xunit;

namespace StudentPickup.Service.Tests.IntegrationTests
{
    public class StudentKioskNotificationServiceTests
    {
        private readonly IStudentKioskNotificationService _studentKioskNotificationService;
        private readonly UnitOfWork _unitOfWork;

        public StudentKioskNotificationServiceTests()
        {
            // Loads csv files from TestData folder, csv file per table
            IDataLoader loader = new CsvDataLoader(Path.Combine(AppDomain.CurrentDomain.SetupInformation.ApplicationBase, "TestData"));

            // Setup In Memory Database Context
            var connection = EntityConnectionFactory.CreateTransient("name=StudentPickupEntities", loader);
            var studentPickupContext = new StudentPickupEntities(connection);
            _unitOfWork = new UnitOfWork(studentPickupContext);

            // Setup Providers
            IPushNotificationProvider pushNotificationProvider = new MockPushNotificationProvider();
            IMapsTarget mapsTarget = new MockMapsAdapter();

            // Setup Repositories
            var studentKioskNotificationRepository = new Repository<StudentKioskNotification>(studentPickupContext, _unitOfWork);
            var guardianRepository = new Repository<Guardian>(studentPickupContext, _unitOfWork);
            var guardianDeviceRepository = new Repository<GuardianDevice>(studentPickupContext, _unitOfWork);
            var studentRepository = new Repository<Student>(studentPickupContext, _unitOfWork);
            var schoolRepository = new Repository<School>(studentPickupContext, _unitOfWork);
            var kioskRepository = new Repository<Kiosk>(studentPickupContext, _unitOfWork);

            // Setup Services
            var guardianDeviceService = new GuardianDeviceService(guardianDeviceRepository, pushNotificationProvider);
            var guardianService = new GuardianService(guardianRepository, mapsTarget, guardianDeviceService);
            var studentService = new StudentService(studentRepository);
            var schoolService = new SchoolService(schoolRepository);
            var kioskService = new KioskService(kioskRepository);
            _studentKioskNotificationService = new StudentKioskNotificationService(studentKioskNotificationRepository,
                guardianService, studentService, schoolService, kioskService);
        }
        
        [Fact]
        public async void GetTimeSinceLastStudentTransactionWhenNoPriorEntryForStudent()
        {
            var timeTaken = await _studentKioskNotificationService.GetTimeSinceLastStudentTransaction(79999999);
            Assert.Equal(0, timeTaken);
        }

        [Fact]
        public async void GetTimeSinceLastStudentTransactionWhenSingleEntryPresent()
        {
            // first entry should be about 10 minutes before now
            var notification = await _studentKioskNotificationService.FindAsync(72);
            notification.NotificationTimestamp = DateTime.UtcNow.AddSeconds(-600);
            _studentKioskNotificationService.Update(notification);
            await _unitOfWork.SaveChangesAsync();

            // compare and find total time (should be 600 seconds), 
            //testing between 598 and 602 seconds to allow time to update context, can be slow at times
            var timeTaken = await _studentKioskNotificationService.GetTimeSinceLastStudentTransaction(7);
            Assert.InRange(timeTaken, 598, 602);
        }

        [Fact]
        public async void CheckPriorInteractionExistsTodayWhenInteractionPresent()
        {
            // update kiosknotification 70
            var notification = await _studentKioskNotificationService.FindAsync(70);
            notification.NotificationTimestamp = DateTime.UtcNow.AddHours(-1);
            _studentKioskNotificationService.Update(notification);
            await _unitOfWork.SaveChangesAsync();

            // check that kiosknotification 70 has been updated and can now be seen
            var interactionExists = await _studentKioskNotificationService.CheckPriorInteractionExistsToday(7, 7);
            Assert.True(interactionExists);
        }

        [Fact]
        public async void CheckPriorInteractionExistsTodayWhenInteractionNotPresent()
        {
            // if this test is ever wrong all of a sudden it means someone in the mock db put student id 79999999
            var interactionExists = await _studentKioskNotificationService.CheckPriorInteractionExistsToday(79999999, 7);
            Assert.False(interactionExists);
        }
        
        [Fact]
        public async void RegisterKioskNotificationKioskDoesNotExist()
        {
            var kioskRequest = new KioskRequest
            {
                KioskId = 4,
                PortId = 4,
                Rfid = "four",
                StudentId = 4,
                SchoolId = 4
            };
            await Assert.ThrowsAsync<ArgumentException>(async () =>
            {
                await _studentKioskNotificationService.RegisterKioskNotification(kioskRequest);
            });
        }
        
    }
}

