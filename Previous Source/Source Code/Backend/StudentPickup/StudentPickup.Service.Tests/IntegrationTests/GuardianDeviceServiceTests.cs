﻿using System;
using System.IO;
using Effort;
using Effort.DataLoaders;
using Repository.Pattern.Ef6;
using StudentPickup.Core.Mapping;
using StudentPickup.Core.Mapping.Mocks;
using StudentPickup.Core.PushNotifications;
using StudentPickup.Core.PushNotifications.Mocks;
using StudentPickup.Data;
using Xunit;

namespace StudentPickup.Service.Tests.IntegrationTests
{
    public class GuardianDeviceServiceTests
    {
        private readonly IGuardianDeviceService _guardianDeviceService;
        private readonly IGuardianService _guardianService;

        public GuardianDeviceServiceTests()
        {
            // Loads csv files from TestData folder, csv file per table
            IDataLoader loader = new CsvDataLoader(Path.Combine(AppDomain.CurrentDomain.SetupInformation.ApplicationBase, "TestData"));

            // Setup In Memory Database Context
            var connection = EntityConnectionFactory.CreateTransient("name=StudentPickupEntities", loader);
            var studentPickupContext = new StudentPickupEntities(connection);
            var unitOfWork = new UnitOfWork(studentPickupContext);

            // Setup Providers
            IPushNotificationProvider pushNotificationProvider = new MockPushNotificationProvider();
            IMapsTarget mapsTarget = new MockMapsAdapter();

            // Setup Repositories
            var guardianRepository = new Repository<Guardian>(studentPickupContext, unitOfWork);
            var guardianDeviceRepository = new Repository<GuardianDevice>(studentPickupContext, unitOfWork);

            // Setup Services
            _guardianDeviceService = new GuardianDeviceService(guardianDeviceRepository, pushNotificationProvider);
            _guardianService = new GuardianService(guardianRepository, mapsTarget, _guardianDeviceService);
        }

        [Fact]
        public async void SendDeviceNotificationsGuardianWithSingleAndroidDeviceShouldSend()
        {
            var didSend = await _guardianDeviceService.SendDeviceLocationRequestNotifications(1, "Test Student");
            Assert.True(didSend);
        }

        [Fact]
        public async void SendDeviceNotificationsGuardianWithMultipleAndroidDevicesShouldSend()
        {
            var didSend = await _guardianDeviceService.SendDeviceLocationRequestNotifications(11, "Test Student");
            Assert.True(didSend);
        }

        [Fact]
        public async void SendDeviceNotificationsGuardianWithSingleAppleDeviceShouldSend()
        {
            var didSend = await _guardianDeviceService.SendDeviceLocationRequestNotifications(12, "Test Student");
            Assert.True(didSend);
        }

        [Fact]
        public async void SendDeviceNotificationsGuardianWithMultipleAppleDevicesShouldSend()
        {
            var didSend = await _guardianDeviceService.SendDeviceLocationRequestNotifications(13, "Test Student");
            Assert.True(didSend);
        }

        //Tests a student who only has one trusted guardian in circle
        [Fact]
        public async void SendTrustedDevicesNotificationsSingleGuardianShouldSend()
        {
            var trustedGuardianDevices = await _guardianService.GetTrustedGuardiansByStudentId(4);
            var didSend = await _guardianDeviceService.SendTrustedDevicesNotifications("Robert", trustedGuardianDevices);
            Assert.True(didSend);
        }

        //Tests a sudent with multiple trusted guardians in circle
        [Fact]
        public async void SendTrustedDevicesNotificationsMultipleGuardiansShouldSend()
        {
            var trustedGuardianDevices = await _guardianService.GetTrustedGuardiansByStudentId(41);
            var didSend = await _guardianDeviceService.SendTrustedDevicesNotifications("Richard", trustedGuardianDevices);
            Assert.True(didSend);
        }

        //Student with no guardians in trusted circle
        [Fact]
        public async void SendTrustedDevicesNotificationsNoGuardianShoundntSend()
        {
            var trustedGuardianDevices = await _guardianService.GetTrustedGuardiansByStudentId(42);
            var didSend = await _guardianDeviceService.SendTrustedDevicesNotifications("Gary", trustedGuardianDevices);
            Assert.False(didSend);
        }
    }
}

