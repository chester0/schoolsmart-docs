﻿using System;
using System.IO;
using Effort;
using Effort.DataLoaders;
using Repository.Pattern.Ef6;
using StudentPickup.Core.Mapping;
using StudentPickup.Core.Mapping.Mocks;
using StudentPickup.Core.PushNotifications;
using StudentPickup.Core.PushNotifications.Mocks;
using StudentPickup.Data;
using Xunit;

namespace StudentPickup.Service.Tests.IntegrationTests
{
    public class GuardianServiceTests
    {
        private readonly IGuardianService _guardianService;
        private readonly UnitOfWork _unitOfWork;

        public GuardianServiceTests()
        {
            // Loads csv files from TestData folder, csv file per table
            IDataLoader loader = new CsvDataLoader(Path.Combine(AppDomain.CurrentDomain.SetupInformation.ApplicationBase, "TestData"));

            // Setup In Memory Database Context
            var connection = EntityConnectionFactory.CreateTransient("name=StudentPickupEntities", loader);
            var studentPickupContext = new StudentPickupEntities(connection);
            _unitOfWork = new UnitOfWork(studentPickupContext);

            // Setup providers
            IMapsTarget mapsTarget = new MockMapsAdapter();
            IPushNotificationProvider pushNotificationProvider = new MockPushNotificationProvider();

            // Setup Repositories
            var guardianRepository = new Repository<Guardian>(studentPickupContext, _unitOfWork);
            var guardianDeviceRepository = new Repository<GuardianDevice>(studentPickupContext, _unitOfWork);

            // Setup Services
            var guardianDeviceService = new GuardianDeviceService(guardianDeviceRepository, pushNotificationProvider);
            _guardianService = new GuardianService(guardianRepository, mapsTarget, guardianDeviceService);
        }
        
        // todo: Mock db access so there is no delay in execution, try mock timing as well
        //[Fact]
        //public async void GetGuardianEtaGuardianEtaUpdatedUpdatedOneHourAgoShouldReturnGuardian()
        //{
        //    var guardian = await _guardianRepository.FindAsync(15);
        //    guardian.LastKnownEtaSeconds = 1;
        //    guardian.LastKnownLatitude = (decimal)1.1;
        //    guardian.LastKnownLongtitude = (decimal)1.1;
        //    guardian.LastRecordedLocationTimestamp = DateTime.Now.AddHours(-1);
        //    _guardianRepository.Update(guardian);
        //    await _unitOfWork.SaveChangesAsync();

        //    var updatedGuardian = await _studentNotificationRegistration.GetGuardianById(guardian.GuardianId);
        //    Assert.NotNull(updatedGuardian);
        //    Assert.Equal(guardian.GuardianId, updatedGuardian.GuardianId);
        //    Assert.Equal(guardian.LastKnownEtaSeconds, updatedGuardian.LastKnownEtaSeconds);
        //    Assert.Equal(guardian.LastKnownLatitude, updatedGuardian.LastKnownLatitude);
        //    Assert.Equal(guardian.LastKnownLongtitude, updatedGuardian.LastKnownLongtitude);
        //    Assert.Equal(guardian.LastRecordedLocationTimestamp, updatedGuardian.LastRecordedLocationTimestamp);
        //}

        //[Fact]
        //public async void GetGuardianEtaGuardianEtaUpdatedAfterOneSecondShouldReturnGuardian()
        //{
        //    var guardian = new Guardian();
        //    const int guardianId = 15;

        //    // Runs once after 1 seconds
        //    var timer = new Timer(async delegate
        //    {
        //        // Run update guardian query on Timer Test context
        //        guardian = await _guardianRepositoryTimerTest.FindAsync(guardianId);
        //        guardian.LastKnownEtaSeconds = 1;
        //        guardian.LastKnownLatitude = (decimal)1.1;
        //        guardian.LastKnownLongtitude = (decimal)1.1;
        //        guardian.LastRecordedLocationTimestamp = DateTime.Now;
        //        _guardianRepositoryTimerTest.Update(guardian);
        //        await _unitOfWorkTimerTest.SaveChangesAsync();
        //    }, null, 1000, Timeout.Infinite);

        //    // Once the timer runs and updates the location this should run and return the updated guardian
        //    // Get Guardian eta with other context so the queries don't run on the same context
        //    var updatedGuardian = await _studentNotificationRegistration.GetGuardianById(guardianId);
        //    Assert.NotNull(updatedGuardian);
        //    Assert.Equal(guardian.GuardianId, updatedGuardian.GuardianId);
        //    Assert.Equal(guardian.LastKnownEtaSeconds, updatedGuardian.LastKnownEtaSeconds);
        //    Assert.Equal(guardian.LastKnownLatitude, updatedGuardian.LastKnownLatitude);
        //    Assert.Equal(guardian.LastKnownLongtitude, updatedGuardian.LastKnownLongtitude);
        //    Assert.Equal(guardian.LastRecordedLocationTimestamp, updatedGuardian.LastRecordedLocationTimestamp);
        //}

        //[Fact]
        //public async void GetGuardianEtaGuardianEtaUpdatedAfterFiveSecondsShouldReturnGuardian()
        //{
        //    var guardian = new Guardian();
        //    const int guardianId = 15;

        //    // Runs once after 5 seconds
        //    var timer = new Timer(async delegate
        //    {
        //        guardian = await _guardianRepositoryTimerTest.FindAsync(guardianId);
        //        guardian.LastKnownEtaSeconds = 1;
        //        guardian.LastKnownLatitude = (decimal)1.1;
        //        guardian.LastKnownLongtitude = (decimal)1.1;
        //        guardian.LastRecordedLocationTimestamp = DateTime.Now;
        //        _guardianRepositoryTimerTest.Update(guardian);
        //        await _unitOfWorkTimerTest.SaveChangesAsync();
        //    }, null, 5000, Timeout.Infinite);

        //    // Once the timer runs and updates the location this should run and return the updated guardian
        //    var updatedGuardian = await _studentNotificationRegistration.GetGuardianById(guardianId);
        //    Assert.NotNull(updatedGuardian);
        //    Assert.Equal(guardian.GuardianId, updatedGuardian.GuardianId);
        //    Assert.Equal(guardian.LastKnownEtaSeconds, updatedGuardian.LastKnownEtaSeconds);
        //    Assert.Equal(guardian.LastKnownLatitude, updatedGuardian.LastKnownLatitude);
        //    Assert.Equal(guardian.LastKnownLongtitude, updatedGuardian.LastKnownLongtitude);
        //    Assert.Equal(guardian.LastRecordedLocationTimestamp, updatedGuardian.LastRecordedLocationTimestamp);
        //}

        //[Fact]
        //public async void GetGuardianEtaGuardianEtaUpdatedAfterNineSecondsShouldReturnGuardian()
        //{
        //    var guardian = new Guardian();
        //    const int guardianId = 15;

        //    // Runs once after 9 seconds
        //    var timer = new Timer(async delegate
        //    {
        //        guardian = await _guardianRepositoryTimerTest.FindAsync(guardianId);
        //        guardian.LastKnownEtaSeconds = 1;
        //        guardian.LastKnownLatitude = (decimal)1.1;
        //        guardian.LastKnownLongtitude = (decimal)1.1;
        //        guardian.LastRecordedLocationTimestamp = DateTime.Now;
        //        _guardianRepositoryTimerTest.Update(guardian);
        //        await _unitOfWorkTimerTest.SaveChangesAsync();
        //    }, null, 9000, Timeout.Infinite);

        //    // Once the timer runs and updates the location this should run and return the updated guardian
        //    var updatedGuardian = await _studentNotificationRegistration.GetGuardianById(guardianId);
        //    Assert.NotNull(updatedGuardian);
        //    Assert.Equal(guardian.GuardianId, updatedGuardian.GuardianId);
        //    Assert.Equal(guardian.LastKnownEtaSeconds, updatedGuardian.LastKnownEtaSeconds);
        //    Assert.Equal(guardian.LastKnownLatitude, updatedGuardian.LastKnownLatitude);
        //    Assert.Equal(guardian.LastKnownLongtitude, updatedGuardian.LastKnownLongtitude);
        //    Assert.Equal(guardian.LastRecordedLocationTimestamp, updatedGuardian.LastRecordedLocationTimestamp);
        //}

        //[Fact]
        //public async void GetGuardianEtaGuardianEtaUpdatedAfterTenSecondsShouldReturnGuardian()
        //{
        //    var guardian = new Guardian();
        //    const int guardianId = 15;

        //    // Runs once after 10 seconds
        //    var timer = new Timer(async delegate
        //    {
        //        guardian = await _guardianRepositoryTimerTest.FindAsync(guardianId);
        //        guardian.LastKnownEtaSeconds = 1;
        //        guardian.LastKnownLatitude = (decimal)1.1;
        //        guardian.LastKnownLongtitude = (decimal)1.1;
        //        guardian.LastRecordedLocationTimestamp = DateTime.Now;
        //        _guardianRepositoryTimerTest.Update(guardian);
        //        await _unitOfWorkTimerTest.SaveChangesAsync();
        //    }, null, 10000, Timeout.Infinite);

        //    // Once the timer runs and updates the location this should run and return the updated guardian
        //    var updatedGuardian = await _studentNotificationRegistration.GetGuardianById(guardianId);
        //    Assert.NotNull(updatedGuardian);
        //    Assert.Equal(guardian.GuardianId, updatedGuardian.GuardianId);
        //    Assert.Equal(guardian.LastKnownEtaSeconds, updatedGuardian.LastKnownEtaSeconds);
        //    Assert.Equal(guardian.LastKnownLatitude, updatedGuardian.LastKnownLatitude);
        //    Assert.Equal(guardian.LastKnownLongtitude, updatedGuardian.LastKnownLongtitude);
        //    Assert.Equal(guardian.LastRecordedLocationTimestamp, updatedGuardian.LastRecordedLocationTimestamp);
        //}

        //[Fact]
        //public async void GetGuardianEtaGuardianEtaUpdatedAfterTwelveSecondsShouldNotReturnGuardian()
        //{
        //    var guardian = new Guardian();
        //    const int guardianId = 15;

        //    // Runs once after 12 seconds
        //    var timer = new Timer(async delegate
        //    {
        //        guardian = await _guardianRepositoryTimerTest.FindAsync(guardianId);
        //        guardian.LastKnownEtaSeconds = 1;
        //        guardian.LastKnownLatitude = (decimal)1.1;
        //        guardian.LastKnownLongtitude = (decimal)1.1;
        //        guardian.LastRecordedLocationTimestamp = DateTime.Now;
        //        _guardianRepositoryTimerTest.Update(guardian);
        //        await _unitOfWorkTimerTest.SaveChangesAsync();
        //    }, null, 12000, Timeout.Infinite);

        //    // Once the timer runs and updates the location this should run and return the updated guardian
        //    var updatedGuardian = await _studentNotificationRegistration.GetGuardianById(guardianId);
        //    Assert.Null(updatedGuardian);
        //}
    }
}

