﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.IO;
using PushSharp;
using PushSharp.Apple;
using PushSharp.Core;
using Repository.Pattern.UnitOfWork;
using StudentPickup.Core.PushNotifications;
using StudentPickup.Service;

namespace StudentPickup.ApplePushNotificationProvider
{
    /// <summary>
    /// An adapter for the Apple Push Notification service to send push notifications to iOS devices
    /// </summary>
    [Export(typeof(IPushNotificationAdapter))]
    [ExportMetadata("OperatingSystemName", "Apple")]
    [ExportMetadata("OperatingSystemId", 2)]
    public class ApplePushNotificationAdapter : PushNotificationAdapter, IPushNotificationAdapter
    {
        private readonly IGuardianDeviceService _guardianDeviceService;
        private readonly IUnitOfWorkAsync _unitOfWorkAsync;

        [ImportingConstructor]
        public ApplePushNotificationAdapter(IGuardianDeviceService guardianDeviceService, IUnitOfWorkAsync unitOfWorkAsync)
        {
            _guardianDeviceService = guardianDeviceService;
            _unitOfWorkAsync = unitOfWorkAsync;
        }

        /// <summary>
        /// Send a push notification to an Android device registered with the Google Cloud Messaging Service
        /// </summary>
        /// <param name="deviceId">The unique identifier of the device registered with the Google Cloud Messaging Service</param>
        /// <param name="pushNotificationType">The type of the push notification to send</param>
        /// <param name="alert">The alert message in the push notitification</param>
        public bool SendPushNotification(string deviceId, PushNotificationTypes pushNotificationType, string alert)
        {
            return SendPushNotification(deviceId, pushNotificationType, alert, null);
        }

        /// <summary>
        /// Send push notification to an iOS device registed with Apple Push Notification Service
        /// </summary>
        /// <param name="deviceId">The unique identifier of the device registered with the Apple Push Notification service</param>
        /// <param name="pushNotificationType">The type of the push notification to send</param>
        /// <param name="alert">The alert message in the push notitification</param>
        /// <param name="customData">The custom data to be sent to the device as JSON</param>
        public bool SendPushNotification(string deviceId, PushNotificationTypes pushNotificationType, string alert, Dictionary<string, string> customData)
        {
            try
            {
                // Create our push services broker
                var push = new PushBroker();

                // Wire up the events for all the services that the broker registers
                push.OnChannelException += ChannelException;
                push.OnServiceException += ServiceException;
                push.OnNotificationFailed += NotificationFailed;
                push.OnDeviceSubscriptionExpired += DeviceSubscriptionExpired;
                push.OnDeviceSubscriptionChanged += DeviceSubscriptionChanged;


                var appleCert = File.ReadAllBytes("Insert Apple Cert");
                push.RegisterAppleService(new ApplePushChannelSettings(appleCert, "pwd"));

                var notification = new AppleNotification()
                    .ForDeviceToken(deviceId)
                    .WithAlert(alert)
                    .WithBadge(7)
                    .WithSound("sound.caf");

                if (customData != null)
                {
                    foreach (var item in customData)
                    {
                        notification.WithCustomItem(item.Key, item.Value);
                    }
                }

                notification.WithCustomItem("type", GetPushNotificationTypeString(pushNotificationType));

                push.QueueNotification(notification);

                // Stop and wait for the queues to drains
                push.StopAllServices();

                // If all goes well should reach here and return true
                return true;
            }
            catch (Exception ex)
            {
                // Shouldn't fail, that would suggest Apples Push Notification Service is down
                // If it is log error and return false
                LogError(ex);
                return false;
            }
        }
        
        protected virtual async void DeviceSubscriptionChanged(object sender, string oldSubscriptionId, string newSubscriptionId, INotification notification)
        {
            await _guardianDeviceService.UpdateGuardianDeviceSubscriptionId(oldSubscriptionId, newSubscriptionId);
            await _unitOfWorkAsync.SaveChangesAsync();
        }
        
        protected virtual async void DeviceSubscriptionExpired(object sender, string expiredSubscriptionId, DateTime expirationDateUtc, INotification notification)
        {
            await _guardianDeviceService.RemoveGuardianDeviceBySubscriptionId(expiredSubscriptionId);
            await _unitOfWorkAsync.SaveChangesAsync();
        }
    }
}
