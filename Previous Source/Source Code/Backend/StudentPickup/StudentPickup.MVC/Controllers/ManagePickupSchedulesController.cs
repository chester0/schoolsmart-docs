﻿using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web.Mvc;
using Repository.Pattern.UnitOfWork;
using StudentPickup.Data;
using StudentPickup.Service;

namespace StudentPickup.MVC.Controllers
{
    public class ManagePickupSchedulesController : Controller
    {
        private readonly IUnitOfWorkAsync _unitOfWorkAsync;
        private readonly IPickupScheduleService _pickupScheduleService;
        private readonly IStudentService _studentService;

        public ManagePickupSchedulesController(IUnitOfWorkAsync unitOfWorkAsync,
            IPickupScheduleService pickupScheduleService, IStudentService studentService)
        {
            _unitOfWorkAsync = unitOfWorkAsync;
            _pickupScheduleService = pickupScheduleService;
            _studentService = studentService;
        }

        // GET: ManagePickupSchedules
        public async Task<ActionResult> Index()
        {
            var pickupSchedules = _pickupScheduleService.Queryable().Include(p => p.Student);
            return View(await pickupSchedules.ToListAsync());
        }

        // GET: ManagePickupSchedules/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var pickupSchedule = await _pickupScheduleService.FindAsync(id);
            if (pickupSchedule == null)
            {
                return HttpNotFound();
            }
            return View(pickupSchedule);
        }

        // GET: ManagePickupSchedules/Create
        public ActionResult Create()
        {
            ViewBag.StudentId = new SelectList(_studentService.Queryable()
                .AsEnumerable()
                .Select(s =>
                    new
                    {
                        s.StudentId,
                        StudentName = $"{s.GivenName} {s.FamilyName}"
                    }), "StudentId", "StudentName");
            return View();
        }

        // POST: ManagePickupSchedules/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(
            [Bind(Include = "PickupScheduleId,StudentId,StartDate,EndDate")] PickupSchedule pickupSchedule)
        {
            if (ModelState.IsValid)
            {
                _pickupScheduleService.Insert(pickupSchedule);
                await _unitOfWorkAsync.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.StudentId = new SelectList(_studentService.Queryable()
                .AsEnumerable()
                .Select(s =>
                    new
                    {
                        s.StudentId,
                        StudentName = $"{s.GivenName} {s.FamilyName}"
                    }), "StudentId", "StudentName");
            return View(pickupSchedule);
        }

        // GET: ManagePickupSchedules/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var pickupSchedule = await _pickupScheduleService.FindAsync(id);
            if (pickupSchedule == null)
            {
                return HttpNotFound();
            }
            ViewBag.StudentId = new SelectList(_studentService.Queryable()
                .AsEnumerable()
                .Select(s =>
                    new
                    {
                        s.StudentId,
                        StudentName = $"{s.GivenName} {s.FamilyName}"
                    }), "StudentId", "StudentName");
            return View(pickupSchedule);
        }

        // POST: ManagePickupSchedules/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(
            [Bind(Include = "PickupScheduleId,StudentId,StartDate,EndDate")] PickupSchedule pickupSchedule)
        {
            if (ModelState.IsValid)
            {
                _pickupScheduleService.Update(pickupSchedule);
                await _unitOfWorkAsync.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.StudentId = new SelectList(_studentService.Queryable()
                .AsEnumerable()
                .Select(s =>
                    new
                    {
                        s.StudentId,
                        StudentName = $"{s.GivenName} {s.FamilyName}"
                    }), "StudentId", "StudentName");
            return View(pickupSchedule);
        }

        // GET: ManagePickupSchedules/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var pickupSchedule = await _pickupScheduleService.FindAsync(id);
            if (pickupSchedule == null)
            {
                return HttpNotFound();
            }
            return View(pickupSchedule);
        }

        // POST: ManagePickupSchedules/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            var pickupSchedule = await _pickupScheduleService.FindAsync(id);
            _pickupScheduleService.Delete(pickupSchedule);
            await _unitOfWorkAsync.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _unitOfWorkAsync.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}