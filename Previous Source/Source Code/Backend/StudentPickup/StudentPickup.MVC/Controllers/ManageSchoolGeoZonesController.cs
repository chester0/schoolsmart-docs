﻿using System.Data.Entity;
using System.Threading.Tasks;
using System.Net;
using System.Web.Mvc;
using Repository.Pattern.UnitOfWork;
using StudentPickup.Data;
using StudentPickup.Service;

namespace StudentPickup.MVC.Controllers
{
    public class ManageSchoolGeoZonesController : Controller
    {
        private readonly IUnitOfWorkAsync _unitOfWorkAsync;
        private readonly IGeoZoneTypeService _geoZoneTypeService;
        private readonly ISchoolService _schoolService;
        private readonly ISchoolGeoZoneService _schoolGeoZoneService;
        private readonly IGeoZonePolygonPointService _geoZonePolygonPointService;

        public ManageSchoolGeoZonesController(IUnitOfWorkAsync unitOfWorkAsync, ISchoolGeoZoneService schoolGeoZoneService, ISchoolService schoolService, IGeoZoneTypeService geoZoneTypeService, IGeoZonePolygonPointService geoZonePolygonPointService)
        {
            _unitOfWorkAsync = unitOfWorkAsync;
            _schoolService = schoolService;
            _geoZoneTypeService = geoZoneTypeService;
            _geoZonePolygonPointService = geoZonePolygonPointService;
            _schoolGeoZoneService = schoolGeoZoneService;
        }
        // GET: ManageSchoolGeoZones
        public async Task<ActionResult> Index()
        {
            var schoolGeoZones = _schoolGeoZoneService.Queryable().Include(s => s.GeoZoneType).Include(s => s.School);
            return View(await schoolGeoZones.ToListAsync());
        }

        // GET: ManageSchoolGeoZones/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SchoolGeoZone schoolGeoZone = await _schoolGeoZoneService.FindAsync(id);
            if (schoolGeoZone == null)
            {
                return HttpNotFound();
            }
            return View(schoolGeoZone);
        }

        // GET: ManageSchoolGeoZones/Create
        public ActionResult Create()
        {
            ViewBag.GeoZoneTypeId = new SelectList(_geoZoneTypeService.Queryable(), "GeoZoneTypeId", "Name");
            ViewBag.SchoolId = new SelectList(_schoolService.Queryable(), "SchoolId", "Name");
            return View();
        }

        // POST: ManageSchoolGeoZones/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "SchoolGeoZoneId,GeoZoneTypeId,SchoolId,Name,GuardianMessage,Description,GeoZonePolygonPoints")] SchoolGeoZone schoolGeoZone)
        {
            if (ModelState.IsValid)
            {
                foreach (var geoZonePolygonPoint in schoolGeoZone.GeoZonePolygonPoints)
                {
                    _geoZonePolygonPointService.Insert(geoZonePolygonPoint);
                }
                _schoolGeoZoneService.Insert(schoolGeoZone);
                await _unitOfWorkAsync.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.GeoZoneTypeId = new SelectList(_geoZoneTypeService.Queryable(), "GeoZoneTypeId", "Name", schoolGeoZone.GeoZoneTypeId);
            ViewBag.SchoolId = new SelectList(_schoolService.Queryable(), "SchoolId", "Name", schoolGeoZone.SchoolId);
            return View(schoolGeoZone);
        }

        // GET: ManageSchoolGeoZones/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SchoolGeoZone schoolGeoZone = await _schoolGeoZoneService.FindAsync(id);
            if (schoolGeoZone == null)
            {
                return HttpNotFound();
            }
            ViewBag.GeoZoneTypeId = new SelectList(_geoZoneTypeService.Queryable(), "GeoZoneTypeId", "Name", schoolGeoZone.GeoZoneTypeId);
            ViewBag.SchoolId = new SelectList(_schoolService.Queryable(), "SchoolId", "Name", schoolGeoZone.SchoolId);
            return View(schoolGeoZone);
        }

        // POST: ManageSchoolGeoZones/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "SchoolGeoZoneId,GeoZoneTypeId,SchoolId,Name,GuardianMessage,Description")] SchoolGeoZone schoolGeoZone)
        {
            if (ModelState.IsValid)
            {
                _schoolGeoZoneService.Update(schoolGeoZone);
                await _unitOfWorkAsync.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.GeoZoneTypeId = new SelectList(_geoZoneTypeService.Queryable(), "GeoZoneTypeId", "Name", schoolGeoZone.GeoZoneTypeId);
            ViewBag.SchoolId = new SelectList(_schoolService.Queryable(), "SchoolId", "Name", schoolGeoZone.SchoolId);
            return View(schoolGeoZone);
        }

        // GET: ManageSchoolGeoZones/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SchoolGeoZone schoolGeoZone = await _schoolGeoZoneService.FindAsync(id);
            if (schoolGeoZone == null)
            {
                return HttpNotFound();
            }
            return View(schoolGeoZone);
        }

        // POST: ManageSchoolGeoZones/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            SchoolGeoZone schoolGeoZone = await _schoolGeoZoneService.FindAsync(id);
            _schoolGeoZoneService.Delete(schoolGeoZone);
            await _unitOfWorkAsync.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _unitOfWorkAsync.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
