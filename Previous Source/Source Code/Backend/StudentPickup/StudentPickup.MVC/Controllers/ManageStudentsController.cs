﻿using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web.Mvc;
using Repository.Pattern.UnitOfWork;
using StudentPickup.Data;
using StudentPickup.Service;

namespace StudentPickup.MVC.Controllers
{
    public class ManageStudentsController : Controller
    {
        private readonly IUnitOfWorkAsync _unitOfWorkAsync;
        private readonly IStudentService _studentService;
        private readonly ISchoolService _schoolService;

        public ManageStudentsController(IUnitOfWorkAsync unitOfWorkAsync, IStudentService studentService, ISchoolService schoolService)
        {
            _unitOfWorkAsync = unitOfWorkAsync;
            _studentService = studentService;
            _schoolService = schoolService;
        }

        // GET: ManageStudents
        public async Task<ActionResult> Index()
        {
            var students = _studentService.Queryable().Include(s => s.School);
            return View(await students.ToListAsync());
        }

        // GET: ManageStudents/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var student = await _studentService.Queryable().Include(s => s.School).SingleOrDefaultAsync(s => s.StudentId == id);
            if (student == null)
            {
                return HttpNotFound();
            }
            return View(student);
        }

        // GET: ManageStudents/Create
        public ActionResult Create()
        {
            ViewBag.SchoolId = new SelectList(_schoolService.Queryable(), "SchoolId", "Name");
            return View();
        }

        // POST: ManageStudents/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "StudentId,StudentSchoolId,RFID,SchoolId,MobileAppId,MobileNumber,GivenName,CommonName,FamilyName,DateOfBirth")] Student student)
        {
            if (ModelState.IsValid)
            {
                _studentService.Insert(student);
                await _unitOfWorkAsync.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.SchoolId = new SelectList(_schoolService.Queryable(), "SchoolId", "Name", student.SchoolId);
            return View(student);
        }

        // GET: ManageStudents/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var student = await _studentService.Queryable().Include(s => s.School).SingleOrDefaultAsync(s => s.StudentId == id);
            if (student == null)
            {
                return HttpNotFound();
            }
            ViewBag.SchoolId = new SelectList(_schoolService.Queryable(), "SchoolId", "Name", student.SchoolId);
            return View(student);
        }

        // POST: ManageStudents/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "StudentId,StudentSchoolId,RFID,SchoolId,MobileAppId,MobileNumber,GivenName,CommonName,FamilyName,DateOfBirth")] Student student)
        {
            if (ModelState.IsValid)
            {
                _studentService.Update(student);
                await _unitOfWorkAsync.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.SchoolId = new SelectList(_schoolService.Queryable(), "SchoolId", "Name", student.SchoolId);
            return View(student);
        }

        // GET: ManageStudents/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var student = await _studentService.Queryable().Include(s => s.School).SingleOrDefaultAsync(s => s.StudentId == id);
            if (student == null)
            {
                return HttpNotFound();
            }
            return View(student);
        }

        // POST: ManageStudents/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            var student = await _studentService.FindAsync(id);
            _studentService.Delete(student);
            await _unitOfWorkAsync.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _unitOfWorkAsync.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
