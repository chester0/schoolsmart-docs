﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using Repository.Pattern.UnitOfWork;
using StudentPickup.Data;
using StudentPickup.MVC.Models;
using StudentPickup.Service;

namespace StudentPickup.MVC.Controllers
{
    public class GuardianController : ApiController
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private readonly IUnitOfWorkAsync _unitOfWorkAsync;
        private readonly IGuardianDeviceService _guardianDeviceService;
        private readonly IGuardianService _guardianService;
        private readonly IPickupScheduleSessionService _pickupScheduleSessionService;

        public GuardianController(IGuardianDeviceService guardianDeviceService, IUnitOfWorkAsync unitOfWorkAsync, IGuardianService guardianService, IPickupScheduleSessionService pickupScheduleSessionService)
        {
            _guardianDeviceService = guardianDeviceService;
            _unitOfWorkAsync = unitOfWorkAsync;
            _guardianService = guardianService;
            _pickupScheduleSessionService = pickupScheduleSessionService;
        }

        /// <summary>
        /// Updates the guardians current location
        /// </summary>
        /// <param name="guardianLocation">The current location of the guardian and their ID</param>
        /// <returns>Success message</returns>
        [HttpPost]
        public async Task<IHttpActionResult> Location(GuardianLocationModel guardianLocation)
        {
            try
            {
                await _guardianService.UpdateGuardianLocation(guardianLocation.GuardianId, guardianLocation.Latitude,
                    guardianLocation.Longitude);
                await _unitOfWorkAsync.SaveChangesAsync();
                return
                    Json(
                        new
                        {
                            Success = true,
                            Message = "Guardian location updated successfully"
                        });
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return
                    Json(
                        new
                        {
                            Success = false,
                            Message = "An erorr has occured. Please ensure guardian exists."
                        });
            }
        }

        /// <summary>
        /// Upserts Guardians Device, inserts if none there, updates if they already have one
        /// </summary>
        /// <param name="guardianDeviceModel">Guardians device details and ID</param>
        /// <returns>Updated/New Guardian Device</returns>
        [HttpPost]
        public async Task<IHttpActionResult> DeviceRegistration(GuardianDeviceModel guardianDeviceModel)
        {
            try
            {
                var guardianDevice = await _guardianDeviceService.UpsertGuardianDevice(guardianDeviceModel.GuardianId,
                    guardianDeviceModel.DeviceAppId, guardianDeviceModel.DeviceOperatingSystemId);
                await _unitOfWorkAsync.SaveChangesAsync();

                return
                    Json(
                        new
                        {
                            Success = true,
                            GuardianDevice = guardianDevice
                        });
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return
                    Json(
                        new
                        {
                            Success = false,
                            Message = "An erorr has occured. Please ensure guardian exists."
                        });
            }
        }

        /// <summary>
        /// Gets the Guardians details associated with the phone number, assumes phone number is unique
        /// </summary>
        /// <param name="mobileNumber">Mobile number of guardian to search for</param>
        /// <returns>Guardian associated with phone number</returns>
        [HttpGet]
        public async Task<IHttpActionResult> GuardianByMobileNumber([FromUri] string mobileNumber)
        {
            try
            {
                var guardian = await _guardianService.GetGuardianByMobileNumber(mobileNumber);

                return
                    Json(
                        new
                        {
                            Success = true,
                            Guardian = guardian
                        });
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return
                    Json(
                        new
                        {
                            Success = false,
                            Message = "An erorr has occured. Please ensure guardian exists."
                        });
            }
        }

        /// <summary>
        /// Gets the latest schedule associated with the guardian
        /// </summary>
        /// <param name="guardianId">The guardian ID of the guardian to search for</param>
        /// <returns>Guardian with schedule, student, school and geo zone details</returns>
        [HttpGet]
        public async Task<IHttpActionResult> GuardianLatestScheduleById([FromUri] int guardianId)
        {
            try
            {
                var sessions = await _pickupScheduleSessionService.GetLatestSchedulesByGuardianId(guardianId);

                if (sessions.Count == 0)
                    return Json(new
                    {
                        Success = false,
                        Message = "No available schedules"
                    });

                var school = sessions.First().PickupSchedule.Student.School;
                var students = new List<Student>();
                sessions.ForEach(s => students.Add(s.PickupSchedule.Student));

                return
                    Json(
                        new
                        {
                            Success = true,
                            School = new
                            {
                                school.Name,
                                school.Latitude,
                                school.Longtitude,
                                GeoZones = school.SchoolGeoZones.Select(g => new
                                {
                                    g.GeoZoneTypeId,
                                    g.Name,
                                    g.Description,
                                    PolygonPoints = g.GeoZonePolygonPoints.Select(p => new
                                    {
                                        p.Longtitude,
                                        p.Latitude
                                    })
                                })
                            },
                            Students = students.Select(s => new
                            {
                                s.GivenName,
                                s.FamilyName,
                                s.CommonName
                            }),
                            Time = new
                            {
                                sessions.First().SessionDate
                            }
                        });
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return
                    Json(
                        new
                        {
                            Success = false,
                            Message = "An erorr has occured. Please ensure guardian exists."
                        });
            }
        }

        /// <summary>
        /// Gets all the guardians with their names and ids
        /// </summary>
        /// <returns>List of guardians</returns>
        public async Task<IHttpActionResult> GetGuardiansList()
        {
            var guardians = await _guardianService.Queryable()
                .Select(g => new
                {
                    g.GuardianId,
                    GuardianName = g.GivenName + " " + g.FamilyName
                })
                .ToListAsync();

            return Json(guardians);
        }

        /// <summary>
        /// Gets all the guardians with their names and ids in both of the students circles
        /// </summary>
        /// <returns>List of guardians</returns>
        public async Task<IHttpActionResult> GetGuardiansByStudent([FromUri] int studentId)
        {
            var trustedGuardians = await _guardianService.GetGuardiansInCircleByStudentId(studentId, CircleTypes.Trusted);
            var scheduledGuardians = await _guardianService.GetGuardiansInCircleByStudentId(studentId, CircleTypes.Scheduled);

            var guardians = trustedGuardians
                .Select(g => new
                {
                    g.GuardianId,
                    GuardianName = g.GivenName + " " + g.FamilyName,
                    CircleTypeId = CircleTypes.Trusted,
                    CircleTypeName = "Trusted"
                })
                .Union(
                    scheduledGuardians.Select(g => new
                    {
                        g.GuardianId,
                        GuardianName = g.GivenName + " " + g.FamilyName,
                        CircleTypeId = CircleTypes.Scheduled,
                        CircleTypeName = "Scheduled"
                    })
                );

            return Json(guardians);
        }

        /// <summary>
        /// Gets all the guardians with their names and ids in the students trusted circle
        /// </summary>
        /// <returns>List of guardians</returns>
        public async Task<IHttpActionResult> GetGuardiansInCircleByPickupScheduleId([FromUri] int pickupScheduleId)
        {
            var guardians = await _guardianService.GetGuardiansInCircleByPickupScheduleId(pickupScheduleId);

            return Json(guardians
                .Select(g => new
                {
                    g.GuardianId,
                    GuardianName = g.GivenName + " " + g.FamilyName
                })
                );
        }

        /// <summary>
        /// Gets all the guardians with their names and ids in the students trusted circle
        /// </summary>
        /// <returns>List of guardians</returns>
        public async Task<IHttpActionResult> GetTrustedGuardiansByStudent([FromUri] int studentId)
        {
            var guardians = await _guardianService.GetGuardiansInCircleByStudentId(studentId, CircleTypes.Trusted);

            return Json(guardians
                .Select(g => new
                {
                    g.GuardianId,
                    GuardianName = g.GivenName + " " + g.FamilyName
                })
                );
        }

        /// <summary>
        /// Gets all the guardians with their names and ids in the students scheduled circle
        /// </summary>
        /// <returns>List of guardians</returns>
        public async Task<IHttpActionResult> GetScheduledGuardiansByStudent([FromUri] int studentId)
        {
            var guardians = await _guardianService.GetGuardiansInCircleByStudentId(studentId, CircleTypes.Scheduled);

            return Json(guardians
                .Select(g => new
                {
                    g.GuardianId,
                    GuardianName = g.GivenName + " " + g.FamilyName
                })
                );
        }
    }
}
