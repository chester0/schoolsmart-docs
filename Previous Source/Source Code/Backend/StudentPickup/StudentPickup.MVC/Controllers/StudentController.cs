﻿using System;
using System.Data.Entity;
using System.Threading.Tasks;
using System.Web.Http;
using Repository.Pattern.UnitOfWork;
using StudentPickup.Core.Models;
using StudentPickup.Data;
using StudentPickup.MVC.Models;
using StudentPickup.Service;

namespace StudentPickup.MVC.Controllers
{
    public class StudentController : ApiController
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private readonly IStudentKioskNotificationService _studentKioskNotificationService;
        private readonly IGuardianStudentCircleService _guardianStudentCircleService;
        private readonly IUnitOfWorkAsync _unitOfWorkAsync;

        public StudentController(IUnitOfWorkAsync unitOfWorkAsync,
            IStudentKioskNotificationService studentKioskNotificationService,
            IGuardianStudentCircleService guardianStudentCircleService)
        {
            _unitOfWorkAsync = unitOfWorkAsync;
            _studentKioskNotificationService = studentKioskNotificationService;
            _guardianStudentCircleService = guardianStudentCircleService;
        }

        [HttpPost]
        public async Task<IHttpActionResult> PostStudentNotification(KioskRequest request)
        {
            try
            {
                var response = await _studentKioskNotificationService.RegisterKioskNotification(request);
                await _unitOfWorkAsync.SaveChangesAsync();
                return Json(response);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return
                    Json(
                        new
                        {
                            Success = false,
                            Message = "An error has occured. Please contact administrator.",
                            Exception = ex.Message,
                            Stack = ex.StackTrace
                        });
            }
        }

        [HttpGet]
        public async Task<IHttpActionResult> DeleteNotifications()
        {
            var notifications = await _studentKioskNotificationService.Queryable().ToListAsync();
            notifications.ForEach(n => _studentKioskNotificationService.Delete(n));
            await _unitOfWorkAsync.SaveChangesAsync();

            return Ok();
        }
            
        [HttpPost]
        public async Task<IHttpActionResult> AddGuardianToStudentCircle(
            GuardianStudentCircleModel guardianStudentCircleModel)
        {
            try
            {
                await _guardianStudentCircleService.AddGuardianToStudentCircle(guardianStudentCircleModel.GuardianId,
                    guardianStudentCircleModel.StudentId, guardianStudentCircleModel.CircleTypeId);
                await _unitOfWorkAsync.SaveChangesAsync();
                return Json(new {Success = true});
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return
                    Json(
                        new
                        {
                            Success = false,
                            Message = "An error has occured. Please contact administrator."
                        });
            }
        }

        [HttpPost]
        public async Task<IHttpActionResult> RemoveGuardianFromStudentCircle(
            GuardianStudentCircleModel guardianStudentCircleModel)
        {
            try
            {
                await _guardianStudentCircleService.RemoveGuardianFromStudentCircle(guardianStudentCircleModel.GuardianId,
                    guardianStudentCircleModel.StudentId, guardianStudentCircleModel.CircleTypeId);
                await _unitOfWorkAsync.SaveChangesAsync();
                return Json(new { Success = true });
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return
                    Json(
                        new
                        {
                            Success = false,
                            Message = "An error has occured. Please contact administrator."
                        });
            }
        }
    }
}