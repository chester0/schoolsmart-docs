﻿using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web.Mvc;
using Repository.Pattern.UnitOfWork;
using StudentPickup.Data;
using StudentPickup.Service;

namespace StudentPickup.MVC.Controllers
{
    public class ManageGuardiansController : Controller
    {
        private readonly IUnitOfWorkAsync _unitOfWorkAsync;
        private readonly IGuardianService _guardianService;

        public ManageGuardiansController(IUnitOfWorkAsync unitOfWorkAsync, IGuardianService guardianService)
        {
            _unitOfWorkAsync = unitOfWorkAsync;
            _guardianService = guardianService;
        }

        // GET: ManageGuardians
        public async Task<ActionResult> Index()
        {
            return View(await _guardianService.Queryable().ToListAsync());
        }

        // GET: ManageGuardians/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Guardian guardian = await _guardianService.FindAsync(id);
            if (guardian == null)
            {
                return HttpNotFound();
            }
            return View(guardian);
        }

        // GET: ManageGuardians/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ManageGuardians/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "GuardianId,GivenName,FamilyName,FriendlyName,MobileNumber,PhoneNumber,EmailAddress,LastKnownLatitude,LastKnownLongtitude,LastKnownEtaSeconds,LastRecordedLocationTimestamp")] Guardian guardian)
        {
            if (ModelState.IsValid)
            {
                _guardianService.Insert(guardian);
                await _unitOfWorkAsync.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(guardian);
        }

        // GET: ManageGuardians/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Guardian guardian = await _guardianService.FindAsync(id);
            if (guardian == null)
            {
                return HttpNotFound();
            }
            return View(guardian);
        }

        // POST: ManageGuardians/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "GuardianId,GivenName,FamilyName,FriendlyName,MobileNumber,PhoneNumber,EmailAddress,LastKnownLatitude,LastKnownLongtitude,LastKnownEtaSeconds,LastRecordedLocationTimestamp")] Guardian guardian)
        {
            if (ModelState.IsValid)
            {
                _guardianService.Update(guardian);
                await _unitOfWorkAsync.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(guardian);
        }

        // GET: ManageGuardians/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Guardian guardian = await _guardianService.FindAsync(id);
            if (guardian == null)
            {
                return HttpNotFound();
            }
            return View(guardian);
        }

        // POST: ManageGuardians/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Guardian guardian = await _guardianService.FindAsync(id);
            _guardianService.Delete(guardian);
            await _unitOfWorkAsync.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _unitOfWorkAsync.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
