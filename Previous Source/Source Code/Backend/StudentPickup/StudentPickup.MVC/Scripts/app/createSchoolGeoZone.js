﻿var map;
var vertices;

function initialize() {
    var myLatlng = new window.google.maps.LatLng(-35.341700, 149.126884);
    var myOptions = {
        zoom: 4,
        center: myLatlng,
        mapTypeId: window.google.maps.MapTypeId.ROADMAP
    }
    map = new window.google.maps.Map(document.getElementById("map_canvas"), myOptions);
    var drawingManager = new window.google.maps.drawing.DrawingManager({
        drawingModes: window.google.maps.drawing.OverlayType.POLYGON,
        drawingControl: true,
        drawingControlOptions: {
            position: window.google.maps.ControlPosition.TOP_CENTER,
            drawingModes: [window.google.maps.drawing.OverlayType.POLYGON]
        },
        polygonOptions: {
            editable: true
        }
    });
    drawingManager.setMap(map);

    window.google.maps.event.addListener(drawingManager, "overlaycomplete", function (event) {
        var newShape = event.overlay;
        newShape.type = event.type;
    });

    window.google.maps.event.addListener(drawingManager, "overlaycomplete", function (event) {
        overlayClickListener(event.overlay);
        vertices = event.overlay.getPath().getArray();
    });
}

function overlayClickListener(overlay) {
    window.google.maps.event.addListener(overlay, "mouseup", function (event) {
        vertices = overlay.getPath().getArray();
    });
}

$(function () {
    $("#form").submit(function (e) {
        var form = $("#form");
        var geoZonePolygonPoints = [];

        $.each(vertices, function (index, vertex) {
            var lat = vertex.lat();
            var lng = vertex.lng();
            geoZonePolygonPoints.push({
                Longtitude: lng,
                Latitude: lat
            });
        });

        var formData = {};
        jQuery(form).serializeArray().map(function (item) {
            if (formData[item.name]) {
                if (typeof (formData[item.name]) === "string") {
                    formData[item.name] = [formData[item.name]];
                }
                formData[item.name].push(item.value);
            } else {
                formData[item.name] = item.value;
            }
        });

        formData.GeoZonePolygonPoints = geoZonePolygonPoints;

        $.ajax({
            type: form.attr("method"),
            url: form.attr("action"),
            data: formData,
            success: function () {
                alert("Inserted successfully");
            }
        });

        e.preventDefault();
    });
});