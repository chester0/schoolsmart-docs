using System;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Reflection;
using MefContrib.Integration.Unity;
using Microsoft.Practices.Unity;
using Repository.Pattern.DataContext;
using Repository.Pattern.Ef6;
using Repository.Pattern.Repositories;
using Repository.Pattern.UnitOfWork;
using StudentPickup.Core.Mapping;
using StudentPickup.Core.PushNotifications;
using StudentPickup.Data;
using StudentPickup.Service;

namespace StudentPickup.MVC
{
    /// <summary>
    /// Specifies the Unity configuration for the main container.
    /// </summary>
    public class UnityConfig
    {
        #region Unity Container
        private static Lazy<IUnityContainer> container = new Lazy<IUnityContainer>(() =>
        {
            var container = new UnityContainer();
            //path to dlls (used by directory catalog)
            var path = AppDomain.CurrentDomain.SetupInformation.PrivateBinPath;
            //Catalog that combines the other sub-catalogs
            var catalog = new AggregateCatalog();
            catalog.Catalogs.Add(new AssemblyCatalog(Assembly.GetExecutingAssembly()));
            catalog.Catalogs.Add(new DirectoryCatalog(path, "*PushNotificationProvider.dll"));
            // Register the catalog - this handles MEF integration.
            container.RegisterCatalog(catalog);
            RegisterTypes(container);
            return container;
        });

        /// <summary>
        /// Gets the configured Unity container.
        /// </summary>
        public static IUnityContainer GetConfiguredContainer()
        {
            return container.Value;
        }
        #endregion

        /// <summary>Registers the type mappings with the Unity container.</summary>
        /// <param name="container">The unity container to configure.</param>
        /// <remarks>There is no need to register concrete types such as controllers or API controllers (unless you want to 
        /// change the defaults), as Unity allows resolving a concrete type even if it was not previously registered.</remarks>
        public static void RegisterTypes(IUnityContainer container)
        {
            // NOTE: To load from web.config uncomment the line below. Make sure to add a Microsoft.Practices.Unity.Configuration to the using statements.
            // container.LoadConfiguration();

            container
                .RegisterType<IDataContextAsync, StudentPickupEntities>(new PerRequestLifetimeManager(), new InjectionConstructor())
                .RegisterType<IUnitOfWorkAsync, UnitOfWork>(new PerRequestLifetimeManager())
                .RegisterType<IRepositoryAsync<Guardian>, Repository<Guardian>>()
                .RegisterType<IRepositoryAsync<GuardianDevice>, Repository<GuardianDevice>>()
                .RegisterType<IRepositoryAsync<StudentKioskNotification>, Repository<StudentKioskNotification>>()
                .RegisterType<IRepositoryAsync<PickupScheduleSession>, Repository<PickupScheduleSession>>()
                .RegisterType<IRepositoryAsync<Student>, Repository<Student>>()
                .RegisterType<IRepositoryAsync<School>, Repository<School>>()
                .RegisterType<IRepositoryAsync<State>, Repository<State>>()
                .RegisterType<IRepositoryAsync<Kiosk>, Repository<Kiosk>>()
                .RegisterType<IRepositoryAsync<GeoZoneType>, Repository<GeoZoneType>>()
                .RegisterType<IRepositoryAsync<SchoolGeoZone>, Repository<SchoolGeoZone>>()
                .RegisterType<IRepositoryAsync<GeoZonePolygonPoint>, Repository<GeoZonePolygonPoint>>()
                .RegisterType<IRepositoryAsync<GuardianStudentCircle>, Repository<GuardianStudentCircle>>()
                .RegisterType<IRepositoryAsync<StudentCircle>, Repository<StudentCircle>>()
                .RegisterType<IRepositoryAsync<PickupSchedule>, Repository<PickupSchedule>>()
                .RegisterType<IRepositoryAsync<PickupScheduleSession>, Repository<PickupScheduleSession>>()
                .RegisterType<IGuardianDeviceService, GuardianDeviceService>()
                .RegisterType<IGuardianService, GuardianService>()
                .RegisterType<IStudentService, StudentService>()
                .RegisterType<ISchoolService, SchoolService>()
                .RegisterType<IPickupScheduleSessionService, PickupScheduleSessionService>()
                .RegisterType<IStateService, StateService>()
                .RegisterType<IKioskService, KioskService>()
                .RegisterType<IGeoZoneTypeService, GeoZoneTypeService>()
                .RegisterType<ISchoolGeoZoneService, SchoolGeoZoneService>()
                .RegisterType<IGeoZonePolygonPointService, GeoZonePolygonPointService>()
                .RegisterType<IGuardianStudentCircleService, GuardianStudentCircleService>()
                .RegisterType<IPickupScheduleService, PickupScheduleService>()
                .RegisterType<IPickupScheduleSessionService, PickupScheduleSessionService>()
                .RegisterType<IPushNotificationProvider, PushNotificationProvider>()
                .RegisterType<IMapsTarget, MapsAdapter>()
                .RegisterType<IStudentKioskNotificationService, StudentKioskNotificationService>();
        }
    }
}
