﻿namespace StudentPickup.MVC.Models
{
    public class GuardianLocationModel
    {
        public int GuardianId { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
    }
}
