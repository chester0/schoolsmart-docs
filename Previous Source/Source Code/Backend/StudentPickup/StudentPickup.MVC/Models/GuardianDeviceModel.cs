﻿using StudentPickup.Data;

namespace StudentPickup.MVC.Models
{
    public class GuardianDeviceModel
    {
        public int GuardianId { get; set; }
        public string DeviceAppId { get; set; }
        public DeviceOperatingSystems DeviceOperatingSystemId { get; set; }
    }
}
