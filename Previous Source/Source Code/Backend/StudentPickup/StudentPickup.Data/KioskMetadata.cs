﻿using System;
using System.ComponentModel.DataAnnotations;

namespace StudentPickup.Data
{
    [MetadataType(typeof(KioskMetadata))]
    public partial class Kiosk
    {
    }

    public class KioskMetadata
    {
        [Display(Name = "Kiosk Id")]
        public int KioskId { get; set; }
        [Display(Name = "Kiosk Name")]
        public string Name { get; set; }
        [Display(Name = "School Id")]
        public int SchoolId { get; set; }
        [Display(Name = "Parent Kiosk Id")]
        public int? ParentKioskId { get; set; }
        [Display(Name = "Parent Port Number")]
        public int? ParentPortNumber { get; set; }
        [Display(Name = "Latitude")]
        public decimal Latitude { get; set; }
        [Display(Name = "Longtitude")]
        public decimal Longtitude { get; set; }
    }
}
