﻿using System;
using System.ComponentModel.DataAnnotations;

namespace StudentPickup.Data
{
    [MetadataType(typeof(StudentMetadata))]
    public partial class Student
    {
    }

    public class StudentMetadata
    {
        [Display(Name="Student Id")]
        public int StudentId { get; set; }
        [Display(Name = "School Id")]
        public int SchoolId { get; set; }
        [Display(Name = "Student School Id")]
        public int? StudentSchoolId { get; set; }
        [Display(Name = "RFID")]
        public string RFID { get; set; }
        [Display(Name = "Common Name")]
        public string CommonName { get; set; }
        [Display(Name = "Given Name")]
        public string GivenName { get; set; }
        [Display(Name = "Family Name")]
        public string FamilyName { get; set; }
        [Display(Name = "Mobile Number")]
        public string MobileNumber { get; set; }
        [Display(Name = "Date Of Birth")]
        public DateTime DateOfBirth { get; set; }
    }
}
