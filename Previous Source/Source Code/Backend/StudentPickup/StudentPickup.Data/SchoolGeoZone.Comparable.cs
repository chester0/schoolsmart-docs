﻿namespace StudentPickup.Data
{
    using System;

    public partial class SchoolGeoZone : IComparable
    {
        /// <summary>
        /// Returns a comparison between two SchoolGeoZones by their zone type. The order in which the are returned is as follow:
        /// 1. Derated Zone.
        /// 2. Red Zone.
        /// 3. Yellow Zone.
        /// 4. Green Zone. 
        /// </summary>
        /// <param name="obj">The other SchoolGeoZone object.</param>
        /// <returns></returns>
        public int CompareTo(object obj)
        {
            if (obj == null) return 1;

            var schoolGeoZone = obj as SchoolGeoZone;

            if (schoolGeoZone != null)
            {
                var thisGeozoneTypeId = (int)GeoZoneTypeId;
                var otherGeozoneTypeId = (int)schoolGeoZone.GeoZoneTypeId;

                return otherGeozoneTypeId.CompareTo(thisGeozoneTypeId);
            }
            else
            {
                throw new ArgumentException("Object is not a SchoolGeoZone.");
            }
        }
    }
}
