﻿using System.ComponentModel.DataAnnotations;

namespace StudentPickup.Data
{
    [MetadataType(typeof(GuardianMetadata))]
    public partial class Guardian
    {
    }

    public class GuardianMetadata
    {
        [Display(Name="Guardian Id")]
        public int GuardianId { get; set; }
        [Display(Name = "Given Name")]
        public string GivenName { get; set; }
        [Display(Name = "Family Name")]
        public string FamilyName { get; set; }
        [Display(Name = "Friendly Name")]
        public string FriendlyName { get; set; }
        [Display(Name = "Mobile Number")]
        public string MobileNumber { get; set; }
        [Display(Name = "Phone Number")]
        public string PhoneNumber { get; set; }
        [Display(Name = "Email Address")]
        public string EmailAddress { get; set; }
    }
}
