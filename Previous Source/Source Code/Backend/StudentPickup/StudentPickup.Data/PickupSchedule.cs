//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace StudentPickup.Data
{
    using System;
    using System.Collections.Generic;
    using Repository.Pattern.Ef6;
    
    public partial class PickupSchedule : Entity
    {
        public PickupSchedule()
        {
            this.PickupScheduleSessions = new HashSet<PickupScheduleSession>();
        }
    
        public int PickupScheduleId { get; set; }
        public int StudentId { get; set; }
        public Nullable<System.DateTime> StartDate { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
    
        public virtual Student Student { get; set; }
        public virtual ICollection<PickupScheduleSession> PickupScheduleSessions { get; set; }
    }
}
