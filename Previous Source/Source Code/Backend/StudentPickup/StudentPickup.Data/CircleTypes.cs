//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace StudentPickup.Data
{
    using System;
    using Repository.Pattern.Ef6;
    
    public enum CircleTypes : int
    {
        Trusted = 1,
        Scheduled = 2
    }
}
