//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace StudentPickup.Data
{
    using System;
    using System.Collections.Generic;
    using Repository.Pattern.Ef6;
    
    public partial class StudentKioskNotification : Entity
    {
        public int StudentKioskNotificationId { get; set; }
        public int StudentId { get; set; }
        public int KioskId { get; set; }
        public System.DateTime NotificationTimestamp { get; set; }
        public Nullable<int> Response { get; set; }
    
        public virtual Kiosk Kiosk { get; set; }
        public virtual Student Student { get; set; }
    }
}
