using System.Threading.Tasks;
using Service.Pattern;
using StudentPickup.Core.Models;
using StudentPickup.Data;

namespace StudentPickup.Service
{
    /// <summary>
    /// The service to access the StudentKioskNotification table in the database
    /// </summary>
    public interface IStudentKioskNotificationService : IService<StudentKioskNotification>
    {
        /// <summary>
        /// Gets the time in seconds since last student kiosk notification. 
        /// </summary>
        /// <param name="studentId">The unique identifier of the student to find</param>
        /// <returns>The time in seconds since the students last notification</returns>
        Task<int> GetTimeSinceLastStudentTransaction(int studentId);

        /// <summary>
        /// Gets the time in seconds since the first student kiosk notification within the past hour.
        /// </summary>
        /// <param name="studentId">The unique identifier of the student to find</param>
        /// <returns>The time in seconds since the students last notification</returns>
        Task<int> GetTimeSinceFirstStudentTransactionInPastHour(int studentId);

        /// <summary>
        /// Checks whether the user has used the kiosk previously based on time entries in StudentKioskNotification in the db
        /// </summary>
        /// <param name="studentId">Unique identifier for the student</param>
        /// <param name="kioskId">Unique indentifier for the kiosk</param>
        /// <returns>True if the student has contacted the server recently, false otherwise</returns>
        Task<bool> CheckPriorInteractionExistsToday(int studentId, int kioskId);

        /// <summary>
        /// Determines whether any other students need to be picked up and send a question to student
        /// Determines which guardian is scheduled and sends all of their registered devices a notification
        /// Saves a record of the student kiosk notification in the database to handle responses
        /// Spin/Wait to get guardians location after sending push notifications 
        /// If it's a reply from a student need to get original notification 
        /// </summary>
        /// <param name="request">Details of the student request message from the kiosk</param>
        /// <returns>A response containing a message to display to the user, any required inputs and an alert tone</returns>
        Task<KioskResponse> RegisterKioskNotification(KioskRequest request);
    }
}