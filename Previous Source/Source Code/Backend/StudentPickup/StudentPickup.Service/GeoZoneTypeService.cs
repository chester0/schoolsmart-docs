﻿using Repository.Pattern.Repositories;
using Service.Pattern;
using StudentPickup.Data;

namespace StudentPickup.Service
{
    public class GeoZoneTypeService : Service<GeoZoneType>, IGeoZoneTypeService
    {
        private readonly IRepositoryAsync<GeoZoneType> _repository;

        public GeoZoneTypeService(IRepositoryAsync<GeoZoneType> repository)
            : base(repository)
        {
            _repository = repository;
        }
    }
}
