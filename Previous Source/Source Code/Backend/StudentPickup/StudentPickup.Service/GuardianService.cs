﻿using System;
using System.Threading.Tasks;
using Repository.Pattern.Repositories;
using Service.Pattern;
using StudentPickup.Data;
using StudentPickup.Repository.Repositories;
using System.Collections.Generic;
using System.Linq;
using StudentPickup.Core.Mapping;

namespace StudentPickup.Service
{
    public class GuardianService : Service<Guardian>, IGuardianService
    {
        private readonly IRepositoryAsync<Guardian> _repository;
        private readonly IGuardianDeviceService _guardianDeviceService;
        private readonly IMapsTarget _mapsTarget;

        public GuardianService(IRepositoryAsync<Guardian> repository, IMapsTarget mapsTarget,
            IGuardianDeviceService guardianDeviceService)
            : base(repository)
        {
            _repository = repository;
            _mapsTarget = mapsTarget;
            _guardianDeviceService = guardianDeviceService;
        }

        /// <summary>
        /// Updates the guardians last known location
        /// </summary>
        /// <param name="guardianId">The unique identifier of the Guardian to update</param>
        /// <param name="latitude">The latitude of the Guardians location</param>
        /// <param name="longitude">The longitude of the Guardians location</param>
        /// <returns>The guardian with the updated location</returns>
        public async Task<Guardian> UpdateGuardianLocation(int guardianId, decimal latitude, decimal longitude)
        {
            return await _repository.UpdateGuardianLocation(guardianId, latitude, longitude);
        }

        /// <summary>
        /// Gets the guardian by their id if the date of their Last Recored Location Timestamp is today
        /// </summary>
        /// <param name="guardianId">The unique identifier of the guardian to search for</param>
        /// <returns>The object of the guardian if their location has been updated today, 
        /// Null if the guardian with that id hasn't had their location updated today.</returns>
        public async Task<Guardian> GetGuardianUpdatedTodayById(int guardianId)
        {
            return await _repository.GetGuardianUpdatedRecentlyById(guardianId);
        }

        /// <summary>
        /// Get the guardian scheduled to pickup the student today
        /// </summary>
        /// <param name="studentId">The unique identifier of the student</param>
        /// <returns>The guardian scheduled to pickup the student today.</returns>
        public async Task<Guardian> GetScheduledGuardianTodayByStudentId(int studentId)
        {
            return await _repository.GetScheduledGuardianTodayByStudentId(studentId);
        }

        /// <summary>
        /// Get the guardian scheduled to pickup the student today
        /// </summary>
        /// <param name="studentId">The unique identifier of the student</param>
        /// <returns>The guardian scheduled to pickup the student today.</returns>
        public async Task<List<Guardian>> GetTrustedGuardiansByStudentId(int studentId)
        {
            return await _repository.GetTrustedGuardiansByStudentId(studentId);
        }

        /// <summary>
        /// Retrieves the guardian associated with the phone number
        /// </summary>
        /// <param name="mobileNumber">The mobile number of the guardian to find</param>
        /// <returns>Guardian associated with phone number</returns>
        public async Task<Guardian> GetGuardianByMobileNumber(string mobileNumber)
        {
            return await _repository.GetGuardianByMobileNumber(mobileNumber);
        }

        /// <summary>
        /// Gets the guardians in the students specific circle
        /// </summary>
        /// <param name="studentId">The unique identifier of the student</param>
        /// <param name="circleType">The type of circle to find the guardians in</param>
        /// <returns>The guardians in the students circle</returns>
        public async Task<List<Guardian>> GetGuardiansInCircleByStudentId(int studentId, CircleTypes circleType)
        {
            return await _repository.GetGuardiansInCircleByStudentId(studentId, circleType);
        }

        /// <summary>
        /// Gets the guardians in the students specific circle by the students pickup schedule
        /// </summary>
        /// <param name="pickupScheduleId">The unique identifier of the pickup schedule</param>
        /// <returns>The guardians in the students circle</returns>
        public async Task<List<Guardian>> GetGuardiansInCircleByPickupScheduleId(int pickupScheduleId)
        {
            return await _repository.GetGuardiansInCircleByPickupScheduleId(pickupScheduleId);
        }

        /// <summary>
        /// Detects the geozone the guardian is in. 
        /// </summary>
        /// <param name="schoolGeoZones">The geo zones of the school.</param>
        /// <param name="latitude">The latitude of the guardian</param>
        /// <param name="longitude">The longitude of the guardian</param>
        /// <returns>The school geozone the guardian is in, or null if the guardian has not approached the school zone (i.e. not in any defined zone).</returns>
        public static SchoolGeoZone DetectSchoolGeoZone(List<SchoolGeoZone> schoolGeoZones, decimal latitude,
            decimal longitude)
        {
            // Sort the list so that the geozones are in the order: Derated, Red, Yellow, Green. 
            schoolGeoZones.Sort();
            // Now that we've ordered the list based on priority, return first geozone the guardian is in, or return null. 
            return schoolGeoZones.FirstOrDefault(schoolGeoZone =>
            {
                // The order in which the polygon was drawn in (clockwise vs counter-clockwise) can affect
                // the results of the algorithm, as it requires the polygon to be drawn in clockwise
                // So instead of forcing the user to draw it in a certain way test for both ways
                // before determining whether they are in the geo zone.

                // Test if the polygons drawn in ascending order, the order in which they were drawn in
                // passes the algorithm.
                var ascPass = GuardianInGeoZone(schoolGeoZone, latitude, longitude);
                if (ascPass)
                    return true;

                // Otherwise test if the polygons drawn in descending order passes the algorithm.
                schoolGeoZone.GeoZonePolygonPoints = schoolGeoZone.GeoZonePolygonPoints
                    .OrderByDescending(p => p.GeoZonePolygonPointId).ToList();
                return GuardianInGeoZone(schoolGeoZone, latitude, longitude);
            });
        }

        /// <summary>
        /// Finds out whether the guardian is in a specified geozone. 
        /// It uses a modified ray casting algorithm taken from: http://alienryderflex.com/polygon/
        /// </summary>
        /// <param name="schoolGeoZone">The school geozone we are testing against.</param>
        /// <param name="latitude">The latitude of the guardian</param>
        /// <param name="longitude">The longitude of the guardian</param>
        /// <returns>True if the guardian is within the geozone, false otherwise.</returns>
        public static bool GuardianInGeoZone(SchoolGeoZone schoolGeoZone, decimal latitude, decimal longitude)
        {
            var polygonPoints = schoolGeoZone.GeoZonePolygonPoints.ToList();

            int nearCounter;
            var farCounter = polygonPoints.Count - 1;
            var oddNodes = false;

            // For reference, "X" is latitude, "Y" is longitude.
            for (nearCounter = 0; nearCounter < polygonPoints.Count; nearCounter++)
            {
                if (polygonPoints[nearCounter].Longtitude < longitude &&
                    polygonPoints[farCounter].Longtitude >= longitude ||
                    polygonPoints[farCounter].Longtitude < longitude &&
                    polygonPoints[nearCounter].Longtitude >= longitude)
                {
                    if (polygonPoints[nearCounter].Latitude +
                        (longitude - polygonPoints[nearCounter].Longtitude) /
                        (polygonPoints[farCounter].Longtitude - polygonPoints[nearCounter].Longtitude) *
                        (polygonPoints[farCounter].Latitude - polygonPoints[nearCounter].Latitude) <
                        latitude)
                    {
                        oddNodes = !oddNodes;
                    }
                }
            }

            return oddNodes;
        }

        /// <summary>
        /// Gets the guardian object for the specified guardian id if their location has been updated today.
        /// Checks every second for ten seconds in total to allow the device time to send the location to the server.
        /// If after ten seconds the location hasn't been updated it will return null since the guardian can't be reached.
        /// </summary>
        /// <param name="guardianId">The unique identifier of the guardian to get</param>
        /// <returns>The object of the guardian if their location has been updated today, 
        /// Null if the guardian with that id hasn't had their location updated today.</returns>
        public async Task<Guardian> GetGuardianUpdatedRecentlyById(int guardianId)
        {
            var iterationCount = 0;
            const int intervalSeconds = 1;
            const int totalIntervals = 10;

            while (true)
            {
                iterationCount += 1;
                await Task.Delay(TimeSpan.FromSeconds(intervalSeconds));
                var guardian = await GetGuardianUpdatedTodayById(guardianId);
                if (guardian?.LastRecordedLocationTimestamp != null)
                {
                    return guardian;
                }
                if (iterationCount == totalIntervals)
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// Attempts to send a notification to the parent
        /// Collects data from the guardian device to determine what message to return to the student
        /// This data includes ETA of guardian and the known geozone
        /// </summary>
        /// <param name="studentId">The unique identifier of the student that tapped on</param>
        /// <param name="studentName">The name of the student</param>
        /// <param name="timeSinceFirstStudentTransactionPastHour">The time since the student's first transaction in the past hour</param>
        /// <param name="schoolLatitude">The unique latitude of the school</param>
        /// <param name="schoolLongitude">The unique longitude of the school</param>
        /// <param name="schoolGeoZones">The geo zones of the Student's school</param>
        /// <returns>A response and tone to be sent from the kiosk to the student</returns>
        public async Task<Tuple<string, int>> ContactStudentsGuardian(int studentId, string studentName,
            int timeSinceFirstStudentTransactionPastHour, decimal? schoolLatitude, decimal? schoolLongitude,
            List<SchoolGeoZone> schoolGeoZones)
        {
            // contact guardian(s), get ETA and generate response based on that information
            // use guardianstudent circle and pickup schedule session to work out which guardian is scheduled to collect the student/s
            var guardian = await GetScheduledGuardianTodayByStudentId(studentId);
            var guardianId = guardian.GuardianId;

            // the message and tone to send back to the kiosk to display to the student
            Tuple<string, int> etaMessage;

            // contact the guardian device to ask it for it's location
            await _guardianDeviceService.SendDeviceLocationRequestNotifications(guardianId, studentName);

            // get the guardians updated location
            var updatedGuardian = await GetGuardianUpdatedRecentlyById(guardianId);
            if (updatedGuardian != null && !(schoolLatitude == null || schoolLongitude == null))
            {
                // these are checks if the parent is illegally parked or if the student is spamming the kiosk
                var guardianGeoZone = DetectSchoolGeoZone(schoolGeoZones, updatedGuardian.LastKnownLatitude.Value,
                    updatedGuardian.LastKnownLongtitude.Value);
                var isInValidZone = guardianGeoZone == null ||
                                    (guardianGeoZone.GeoZoneTypeId != GeoZoneTypes.DeRatedRed &&
                                     guardianGeoZone.GeoZoneTypeId != GeoZoneTypes.Red);

                int eta;

                // query google maps service for eta
                try
                {
                    eta =
                        await
                            _mapsTarget.GetEtaBetweenPoints(updatedGuardian.LastKnownLatitude,
                                updatedGuardian.LastKnownLongtitude,
                                schoolLatitude, schoolLongitude);
                }
                catch (ArgumentNullException)
                {
                    eta = -1;
                }

                // we have all of our information, let's generate an eta message for the student
                etaMessage = GenerateEtaMessage(eta, guardianGeoZone);

                // if guardian is illegally parked we can scream at them, if not we scream at them positively
                await _guardianDeviceService.SendGeozoneNotification(guardianId, guardianGeoZone, eta, studentName);
            }
            else // we can't find the guardian!
            {
                // if they have been trying for the past fifteen minutes and we still can't find the guardian send them
                // to after school care.
                if (Math.Round(Convert.ToDouble(timeSinceFirstStudentTransactionPastHour) / 60) > 15)
                    etaMessage = Tuple.Create("Your guardian cannot be contacted. \n" +
                                 "Please proceed to after-school care.", 1);
                else
                    etaMessage = Tuple.Create("Your parent or guardian is currently unreachable. \n" +
                                 "Please try again in a few minutes.", 1);

                var trustedGuardians = await GetTrustedGuardiansByStudentId(studentId);
                await _guardianDeviceService.SendTrustedDevicesNotifications(studentName, trustedGuardians);
            }

            return etaMessage;
        }

        /// <summary>
        /// Gets the message to display to the student depending on how far away their guardian is and when was the last time they tapped on.
        /// </summary>
        /// <param name="eta">Time in seconds of parent's location from pickup zone</param>
        /// <param name="guardianGeoZone">Geozone the guardian is in</param>
        /// <returns>A message string to be included in the HTTP response</returns>
        public static Tuple<string, int> GenerateEtaMessage(int eta, SchoolGeoZone guardianGeoZone)
        {
            if (eta < 0)
                throw new ArgumentException("eta must be greater than or equal to 0");

            var message = "";
            var tone = 0;
            var etaMinutes = Math.Round(Convert.ToDouble(eta) / 60);

            // guardian is here/very close and parked in a green zone
            if (etaMinutes <= 3 && (guardianGeoZone != null && guardianGeoZone.GeoZoneTypeId == GeoZoneTypes.Green))
            {
                message = "Your parent or guardian is less than 3 minutes away. Please proceed to the pickup zone.";
            }
            // guardian is close but illegally parked. 
            else if (etaMinutes <= 3 &&
                     (guardianGeoZone != null &&
                      (guardianGeoZone.GeoZoneTypeId == GeoZoneTypes.Red ||
                       guardianGeoZone.GeoZoneTypeId == GeoZoneTypes.DeRatedRed)))
            {
                message =
                    $"Your parent or guardian is is close but not ready yet. \nPlease try again in a few minutes.";
                tone = 1;
            }
            // guardian is less than 15 minutes away and not in a green zone
            else if (etaMinutes <= 15)
            {
                message =
                    $"Your parent or guardian is on their way. \nTheir estimated time of arrival will be in {etaMinutes} minutes. \nPlease try again in a few minutes.";
                tone = 1;
            }
            // guardian is 15+ minutes away
            else if (etaMinutes > 15)
            {
                message = "Please go to school after care. \n" +
                          "Your guardian has been told to pick you up there.";
                tone = 1;
            }

            return Tuple.Create(message, tone);
        }
    }
}