using Service.Pattern;
using StudentPickup.Data;

namespace StudentPickup.Service
{
    /// <summary>
    /// The service to access the GeoZoneType table in the database
    /// </summary>
    public interface IGeoZoneTypeService : IService<GeoZoneType>
    {
    }
}