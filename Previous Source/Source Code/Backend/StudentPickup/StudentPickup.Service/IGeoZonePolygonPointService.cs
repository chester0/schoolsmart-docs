﻿using Service.Pattern;
using StudentPickup.Data;

namespace StudentPickup.Service
{
    /// <summary>
    /// The service to access the GeoZonePolygonPoint table in the database
    /// </summary>
    public interface IGeoZonePolygonPointService : IService<GeoZonePolygonPoint>
    {
    }
}