using System.Collections.Generic;
using System.Threading.Tasks;
using Service.Pattern;
using StudentPickup.Data;

namespace StudentPickup.Service
{
    public interface IGuardianDeviceService : IService<GuardianDevice>
    {
        /// <summary>
        /// Gets all devices for the guardian scheduled to pickup the student today
        /// </summary>
        /// <param name="guardianId">The id of the guardian to find the devices for</param>
        /// <returns>An enumeration of guardian devices</returns>
        Task<List<GuardianDevice>> GetScheduledGuardianDeviceByGuardian(int guardianId);


        /// <summary>
        /// If the guardian doesn't already have a registered device, register one. If they do update the existing device
        /// with the new device app id and operating system details.
        /// </summary>
        /// <param name="guardianId">Guardian who owns the device</param>
        /// <param name="deviceAppId">The registered ID with the device push notification service</param>
        /// <param name="deviceOperatingSystemId">The operating system of the device</param>
        /// <returns>New or updated device for guardian</returns>
        Task<GuardianDevice> UpsertGuardianDevice(int guardianId, string deviceAppId,
            DeviceOperatingSystems deviceOperatingSystemId);

        /// <summary>
        /// Sends out push notifications to guardian devices to ask the device for it's location.
        /// </summary>
        /// <param name="guardianId">Unique identifier for guardian</param>
        /// <param name="studentName">Student's name</param>
        /// <returns>Where or not the notification was sent successfully</returns>
        Task<bool> SendDeviceLocationRequestNotifications(int guardianId, string studentName);

        /// <summary>
        /// Sends out push notifications to guardian devices notifying them there student is ready for pickup or if they are in an invalid zone.
        /// </summary>
        /// <param name="guardianId">Unique identifier for guardian</param>
        /// <param name="guardianGeoZone">The geo zone the guardian is in</param>
        /// <param name="eta">Estimated time of arival in seconds guardian is from school</param>
        /// <param name="studentName">Name of the guardians student</param>
        /// <returns>Whether or not the notification was sent successfully</returns>
        Task<bool> SendGeozoneNotification(int guardianId, SchoolGeoZone guardianGeoZone, int eta, string studentName);

        /// <summary>
        /// Sends out push notifications to trusted guardian devices when scheduled guardian is unreachable.
        /// </summary>
        /// <param name="studentName">Student's name</param>
        /// <param name="trustedGuardians"></param>
        /// <returns>Whether or not the push notifications were sent successfully</returns>
        Task<bool> SendTrustedDevicesNotifications(string studentName, List<Guardian> trustedGuardians);

        /// <summary>
        /// Updates the device subscription id based on the old subscription id
        /// </summary>
        /// <param name="oldSubscriptionId">Previous device subscription id</param>
        /// <param name="newSubscriptionId">New device subscription id</param>
        /// <returns>Updated guardian device</returns>
        Task<GuardianDevice> UpdateGuardianDeviceSubscriptionId(string oldSubscriptionId, string newSubscriptionId);

        /// <summary>
        /// Updates the device subscription id based on the old subscription id
        /// </summary>
        /// <param name="subscriptionId">Previous device subscription id</param>
        /// <returns>Updated guardian device</returns>
        Task<GuardianDevice> RemoveGuardianDeviceBySubscriptionId(string subscriptionId);
    }
}