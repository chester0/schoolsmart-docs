﻿using System;
using System.Threading.Tasks;
using Repository.Pattern.Repositories;
using Repository.Pattern.UnitOfWork;
using Service.Pattern;
using StudentPickup.Data;
using StudentPickup.Repository.Repositories;

namespace StudentPickup.Service
{
    public class GuardianStudentCircleService : Service<GuardianStudentCircle>, IGuardianStudentCircleService
    {
        private readonly IRepositoryAsync<GuardianStudentCircle> _repository;
        private readonly IRepositoryAsync<StudentCircle> _studentCircleRepository;
        private readonly IUnitOfWorkAsync _unitOfWork;

        public GuardianStudentCircleService(IRepositoryAsync<GuardianStudentCircle> repository,
            IRepositoryAsync<StudentCircle> studentCircleRepository, IUnitOfWorkAsync unitOfWork)
            : base(repository)
        {
            _repository = repository;
            _studentCircleRepository = studentCircleRepository;
            _unitOfWork = unitOfWork;
        }

        /// <summary>
        /// Adds a guardian to the students particular circle
        /// </summary>
        /// <param name="guardianId">The unique identifier of the guardian to add</param>
        /// <param name="studentId">The unique identifier of the student</param>
        /// <param name="circleType">The type of circle to add the guardian in</param>
        /// <returns>The added GuardianStudentCircle</returns>
        public async Task<GuardianStudentCircle> AddGuardianToStudentCircle(int guardianId, int studentId, CircleTypes circleType)
        {
            var studentCircle = await _studentCircleRepository.GetStudentCircleByStudentId(studentId, circleType);
            await _unitOfWork.SaveChangesAsync();

            if (await _repository.GuardianExistsInStudentCircle(studentCircle.StudentCircleId, guardianId))
                throw new Exception("Guardian already exists in students circle");

            var guardianStudentCircle = new GuardianStudentCircle
            {
                GuardianId = guardianId,
                StudentCircleId = studentCircle.StudentCircleId
            };
            _repository.Insert(guardianStudentCircle);
            await _unitOfWork.SaveChangesAsync();

            return guardianStudentCircle;
        }

        /// <summary>
        /// Removes a guardian from the students particular circle
        /// </summary>
        /// <param name="guardianId">The unique identifier of the guardian to remove</param>
        /// <param name="studentId">The unique identifier of the student</param>
        /// <param name="circleType">The type of circle to remove the guardian from</param>
        public async Task RemoveGuardianFromStudentCircle(int guardianId, int studentId, CircleTypes circleType)
        {
            var studentCircle = await _studentCircleRepository.GetStudentCircleByStudentId(studentId, circleType);
            await _unitOfWork.SaveChangesAsync();
            
            await _repository.RemoveGuardianFromStudentCircle(studentCircle.StudentCircleId, guardianId);
            await _unitOfWork.SaveChangesAsync();
        }
    }
}
