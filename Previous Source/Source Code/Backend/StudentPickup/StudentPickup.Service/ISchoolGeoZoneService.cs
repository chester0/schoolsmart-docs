using Service.Pattern;
using StudentPickup.Data;

namespace StudentPickup.Service
{
    /// <summary>
    /// The service to access the SchoolGeoZone table in the database
    /// </summary>
    public interface ISchoolGeoZoneService : IService<SchoolGeoZone>
    {
    }
}