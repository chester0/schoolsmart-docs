﻿using System.Threading.Tasks;
using Repository.Pattern.Repositories;
using Service.Pattern;
using StudentPickup.Data;
using StudentPickup.Repository.Repositories;
using System.Collections.Generic;
using System;

namespace StudentPickup.Service
{
    public class PickupScheduleSessionService : Service<PickupScheduleSession>, IPickupScheduleSessionService
    {
        private readonly IRepositoryAsync<PickupScheduleSession> _repository;

        public PickupScheduleSessionService(IRepositoryAsync<PickupScheduleSession> repository)
            : base(repository)
        {
            _repository = repository;
        }

        /// <summary>
        /// Gets the latest schedules of the guardian by Id including the students, schools and schools geo zone details
        /// </summary>
        /// <param name="guardianId">The unique identifier of the guardian to search for</param>
        /// <returns>Guardian with details</returns>
        public async Task<List<PickupScheduleSession>> GetLatestSchedulesByGuardianId(int guardianId)
        {
            return await _repository.GetLatestSchedulesByGuardianId(guardianId);
        }
    }
}
