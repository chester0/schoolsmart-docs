﻿using Repository.Pattern.Repositories;
using Service.Pattern;
using StudentPickup.Data;

namespace StudentPickup.Service
{
    public class PickupScheduleService : Service<PickupSchedule>, IPickupScheduleService
    {
        private readonly IRepositoryAsync<PickupSchedule> _repository;

        public PickupScheduleService(IRepositoryAsync<PickupSchedule> repository)
            : base(repository)
        {
            _repository = repository;
        }
    }
}
