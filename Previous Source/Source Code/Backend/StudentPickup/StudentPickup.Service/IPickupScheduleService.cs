using Service.Pattern;
using StudentPickup.Data;

namespace StudentPickup.Service
{
    /// <summary>
    /// The service to access the Pickup Schedule table in the database
    /// </summary>
    public interface IPickupScheduleService : IService<PickupSchedule>
    {
    }
}