﻿using Repository.Pattern.Repositories;
using Service.Pattern;
using StudentPickup.Data;

namespace StudentPickup.Service
{
    public class KioskService : Service<Kiosk>, IKioskService
    {
        private readonly IRepositoryAsync<Kiosk> _repository;

        public KioskService(IRepositoryAsync<Kiosk> repository)
            : base(repository)
        {
            _repository = repository;
        }
    }
}
