using Service.Pattern;
using StudentPickup.Data;

namespace StudentPickup.Service
{
    /// <summary>
    /// The service to access the State table in the database
    /// </summary>
    public interface IStateService : IService<State>
    {
    }
}