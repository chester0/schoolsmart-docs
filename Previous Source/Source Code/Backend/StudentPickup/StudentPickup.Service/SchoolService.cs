﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Repository.Pattern.Repositories;
using Service.Pattern;
using StudentPickup.Data;
using StudentPickup.Repository.Repositories;

namespace StudentPickup.Service
{
    public class SchoolService : Service<School>, ISchoolService
    {
        private readonly IRepositoryAsync<School> _repository;

        public SchoolService(IRepositoryAsync<School> repository)
            : base(repository)
        {
            _repository = repository;
        }

        /// <summary>
        /// Gets the school by the KioskId including the GeoZones and GeoZonePolygon Points
        /// </summary>
        /// <param name="kioskId">The unique identifier of the kiosk</param>
        /// <returns>The school with the GeoZones and GeoZonePolygon Points</returns>
        public async Task<School> GetSchoolIncludingGeoZonesByKiosk(int kioskId)
        {
            return await _repository.GetSchoolIncludingGeoZonesByKiosk(kioskId);
        }
    }
}
