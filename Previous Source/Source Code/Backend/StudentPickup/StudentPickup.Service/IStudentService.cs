using System.Collections.Generic;
using System.Threading.Tasks;
using Service.Pattern;
using StudentPickup.Data;

namespace StudentPickup.Service
{
    /// <summary>
    /// The service to access the Student table in the database
    /// </summary>
    public interface IStudentService : IService<Student>
    {
        /// <summary>
        /// Gets the extra students that will be picked up in the carpool by the students guardian today. Returns an empty list if no other students are being picked up.
        /// </summary>
        /// <param name="guardianId">The unique identifier of the students guardian</param>
        /// <param name="studentId">The unique identifier of the student</param>
        /// <returns>The students that will be picked up by the guardian today. Empty list if no one other than the student is being picked up.</returns>
        Task<List<Student>> GetCarpoolParticipantsByGuardianToday(int guardianId, int studentId);
        
        /// <summary>
        /// Determines whether the student is being picked up today.
        /// </summary>
        /// <param name="studentId">The unique identifier of the student</param>
        /// <returns>Whether the student is being picked up today.</returns>
        Task<bool> StudentBeingPickedUpToday(int studentId);

        /// <summary>
        /// Determines whether the student is part of a carpool today for the guardian at the particular school.
        /// </summary>
        /// <param name="guardianId">The unique identifier of the guardian</param>
        /// <param name="schoolId">The unique identifier of the school</param>
        /// <returns>Whether the student is in a carpool.</returns>
        Task<bool> IsStudentInCarpoolToday(int guardianId, int schoolId);
        
        /// <summary>
        /// Gets a student by their rfid.
        /// </summary>
        /// <param name="rfid">The rfid of the student to find.</param>
        /// <returns>The student with the rfid.</returns>
        Task<Student> GetStudentByRfid(string rfid);
    }
}