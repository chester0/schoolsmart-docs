﻿using Repository.Pattern.Repositories;
using Service.Pattern;
using StudentPickup.Data;

namespace StudentPickup.Service
{
    public class GeoZonePolygonPointService : Service<GeoZonePolygonPoint>, IGeoZonePolygonPointService
    {
        private readonly IRepositoryAsync<GeoZonePolygonPoint> _repository;

        public GeoZonePolygonPointService(IRepositoryAsync<GeoZonePolygonPoint> repository)
            : base(repository)
        {
            _repository = repository;
        }
    }
}
