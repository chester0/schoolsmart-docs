﻿using Repository.Pattern.Repositories;
using Service.Pattern;
using StudentPickup.Data;

namespace StudentPickup.Service
{
    public class SchoolGeoZoneService : Service<SchoolGeoZone>, ISchoolGeoZoneService
    {
        private readonly IRepositoryAsync<SchoolGeoZone> _repository;

        public SchoolGeoZoneService(IRepositoryAsync<SchoolGeoZone> repository)
            : base(repository)
        {
            _repository = repository;
        }
    }
}
