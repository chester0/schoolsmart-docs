using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Service.Pattern;
using StudentPickup.Data;

namespace StudentPickup.Service
{
    public interface IGuardianService : IService<Guardian>
    {
        /// <summary>
        /// Updates the guardians last known location
        /// </summary>
        /// <param name="guardianId">The unique identifier of the Guardian to update</param>
        /// <param name="latitude">The latitude of the Guardians location</param>
        /// <param name="longitude">The longitude of the Guardians location</param>
        /// <returns>The guardian with the updated location</returns>
        Task<Guardian> UpdateGuardianLocation(int guardianId, decimal latitude, decimal longitude);

        /// <summary>
        /// Gets the guardian by their id if the date of their Last Recored Location Timestamp is today
        /// </summary>
        /// <param name="guardianId">The unique identifier of the guardian to search for</param>
        /// <returns>The object of the guardian if their location has been updated today, 
        /// Null if the guardian with that id hasn't had their location updated today.</returns>
        Task<Guardian> GetGuardianUpdatedTodayById(int guardianId);

        /// <summary>
        /// Get the guardian scheduled to pickup the student today
        /// </summary>
        /// <param name="studentId">The unique identifier of the student</param>
        /// <returns>The guardian scheduled to pickup the student today.</returns>
        Task<Guardian> GetScheduledGuardianTodayByStudentId(int studentId);

        /// <summary>
        /// Get the guardian scheduled to pickup the student today
        /// </summary>
        /// <param name="studentId">The unique identifier of the student</param>
        /// <returns>The guardian scheduled to pickup the student today.</returns>
        Task<List<Guardian>> GetTrustedGuardiansByStudentId(int studentId);

        /// <summary>
        /// Retrieves the guardian associated with the phone number
        /// </summary>
        /// <param name="mobileNumber">The mobile number of the guardian to find</param>
        /// <returns>Guardian associated with phone number</returns>
        Task<Guardian> GetGuardianByMobileNumber(string mobileNumber);

        /// <summary>
        /// Gets the guardians in the students specific circle
        /// </summary>
        /// <param name="studentId">The unique identifier of the student</param>
        /// <param name="circleType">The type of circle to find the guardians in</param>
        /// <returns>The guardians in the students circle</returns>
        Task<List<Guardian>> GetGuardiansInCircleByStudentId(int studentId, CircleTypes circleType);

        /// <summary>
        /// Gets the guardians in the students specific circle by the students pickup schedule
        /// </summary>
        /// <param name="pickupScheduleId">The unique identifier of the pickup schedule</param>
        /// <returns>The guardians in the students circle</returns>
        Task<List<Guardian>> GetGuardiansInCircleByPickupScheduleId(int pickupScheduleId);

        /// <summary>
        /// Gets the guardian object for the specified guardian id if their location has been updated today.
        /// Checks every second for ten seconds in total to allow the device time to send the location to the server.
        /// If after ten seconds the location hasn't been updated it will return null since the guardian can't be reached.
        /// </summary>
        /// <param name="guardianId">The unique identifier of the guardian to get</param>
        /// <returns>The object of the guardian if their location has been updated today, 
        /// Null if the guardian with that id hasn't had their location updated today.</returns>
        Task<Guardian> GetGuardianUpdatedRecentlyById(int guardianId);

        /// <summary>
        /// Attempts to send a notification to the parent
        /// Collects data from the guardian device to determine what message to return to the student
        /// This data includes ETA of guardian and the known geozone
        /// </summary>
        /// <param name="studentId">The unique identifier of the student that tapped on</param>
        /// <param name="studentName">The name of the student</param>
        /// <param name="timeSinceLastStudentTransaction">The time since the student's last transaction</param>
        /// <param name="schoolLatitude">The unique latitude of the school</param>
        /// <param name="schoolLongitude">The unique longitude of the school</param>
        /// <param name="schoolGeoZones">The geo zones of the Student's school</param>
        /// <returns>A response and tone to be sent from the kiosk to the student</returns>
        Task<Tuple<string,int>> ContactStudentsGuardian(int studentId, string studentName,
            int timeSinceLastStudentTransaction, decimal? schoolLatitude, decimal? schoolLongitude,
            List<SchoolGeoZone> schoolGeoZones);
    }
}