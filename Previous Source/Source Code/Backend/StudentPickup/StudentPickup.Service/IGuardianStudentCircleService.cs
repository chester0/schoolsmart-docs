﻿using System.Threading.Tasks;
using Service.Pattern;
using StudentPickup.Data;

namespace StudentPickup.Service
{
    /// <summary>
    /// The service to access the Guardian Student Circle table in the database
    /// </summary>
    public interface IGuardianStudentCircleService : IService<GuardianStudentCircle>
    {
        /// <summary>
        /// Adds a guardian to the students particular circle
        /// </summary>
        /// <param name="guardianId">The unique identifier of the guardian to add</param>
        /// <param name="studentId">The unique identifier of the student</param>
        /// <param name="circleType">The type of circle to add the guardian in</param>
        /// <returns>The added GuardianStudentCircle</returns>
        Task<GuardianStudentCircle> AddGuardianToStudentCircle(int guardianId, int studentId, CircleTypes circleType);

        /// <summary>
        /// Removes a guardian from the students particular circle
        /// </summary>
        /// <param name="guardianId">The unique identifier of the guardian to remove</param>
        /// <param name="studentId">The unique identifier of the student</param>
        /// <param name="circleType">The type of circle to remove the guardian from</param>
        Task RemoveGuardianFromStudentCircle(int guardianId, int studentId, CircleTypes circleType);
    }
}