﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Repository.Pattern.Repositories;
using Service.Pattern;
using StudentPickup.Core.Models;
using StudentPickup.Data;
using StudentPickup.Repository.Repositories;

namespace StudentPickup.Service
{
    public class StudentKioskNotificationService : Service<StudentKioskNotification>, IStudentKioskNotificationService
    {
        private readonly IRepositoryAsync<StudentKioskNotification> _repository;
        private readonly IGuardianService _guardianService;
        private readonly IStudentService _studentService;
        private readonly ISchoolService _schoolService;
        private readonly IKioskService _kioskService;

        public StudentKioskNotificationService(IRepositoryAsync<StudentKioskNotification> repository,
            IGuardianService guardianService, IStudentService studentService, ISchoolService schoolService,
            IKioskService kioskService)
            : base(repository)
        {
            _repository = repository;
            _guardianService = guardianService;
            _studentService = studentService;
            _schoolService = schoolService;
            _kioskService = kioskService;
        }

        /// <summary>
        /// Gets the time in seconds since last student kiosk notification. 
        /// </summary>
        /// <param name="studentId">The unique identifier of the student to find</param>
        /// <returns>The time in seconds since the students last notification</returns>
        public async Task<int> GetTimeSinceLastStudentTransaction(int studentId)
        {
            var lastStudentTimestamp = await _repository.GetLastStudentTransactionTimestamp(studentId);

            return
                (int) (lastStudentTimestamp.HasValue ? (DateTime.UtcNow - lastStudentTimestamp.Value).TotalSeconds : 0);
        }

        /// <summary>
        /// Gets the time in seconds since the first student kiosk notification within the past hour.
        /// </summary>
        /// <param name="studentId">The unique identifier of the student to find</param>
        /// <returns>The time in seconds since the students last notification</returns>
        public async Task<int> GetTimeSinceFirstStudentTransactionInPastHour(int studentId)
        {
            var firstStudentTimestamp = await _repository.GetFirstStudentTransactionTimestampInPastHour(studentId);

            return
                (int)
                    (firstStudentTimestamp.HasValue ? (DateTime.UtcNow - firstStudentTimestamp.Value).TotalSeconds : 0);
        }

        /// <summary>
        /// Checks whether the user has used the kiosk previously based on time entries in StudentKioskNotification in the db
        /// </summary>
        /// <param name="studentId">Unique identifier for the student</param>
        /// <param name="kioskId">Unique indentifier for the kiosk</param>
        /// <returns>True if the student has contacted the server recently, false otherwise</returns>
        public async Task<bool> CheckPriorInteractionExistsToday(int studentId, int kioskId)
        {
            var lastStudentTimestamp = await _repository.GetLastStudentTransactionTimestampByKiosk(studentId, kioskId);

            return DateTime.UtcNow.Date == lastStudentTimestamp?.Date;
        }

        /// <summary>
        /// Determines whether any other students need to be picked up and send a question to student
        /// Determines which guardian is scheduled and sends all of their registered devices a notification
        /// Saves a record of the student kiosk notification in the database to handle responses
        /// Spin/Wait to get guardians location after sending push notifications 
        /// If it's a reply from a student need to get original notification 
        /// </summary>
        /// <param name="request">Details of the student request message from the kiosk</param>
        /// <returns>A response containing a message to display to the user, any required inputs and an alert tone</returns>
        public async Task<KioskResponse> RegisterKioskNotification(KioskRequest request)
        {
            KioskResponse response;

            var student = await _studentService.GetStudentByRfid(request.Rfid);
            var kiosk = await _kioskService.FindAsync(request.KioskId);
            if (student == null)
                throw new ArgumentException("Student does not exist");
            if (kiosk == null)
                throw new ArgumentException("Kiosk does not exist");

            var studentKioskNotification = new StudentKioskNotification
            {
                StudentId = student.StudentId,
                KioskId = request.KioskId,
                NotificationTimestamp = DateTime.UtcNow,
                Response = request.Response
            };

            var guardian = await _guardianService.GetScheduledGuardianTodayByStudentId(student.StudentId);
            // if the student is not being picked up we can complete the transaction
            if (guardian == null)
                return new KioskResponse
                {
                    Message = "You are not being collected by a guardian today. \n" +
                              "Please proceed with arrangements made with your parent or guardian. \n" +
                              "(Take the bus, ride a bike, walk)",
                    Tone = 1
                };

            var school = await _schoolService.GetSchoolIncludingGeoZonesByKiosk(request.KioskId);
            var timeSinceLastTransaction = await GetTimeSinceLastStudentTransaction(student.StudentId);
            var timeSinceFirstStudentTransactionPastHour =
                await GetTimeSinceFirstStudentTransactionInPastHour(student.StudentId);

            if (request.Response == null)
                // handling first message in transaction
                response =
                    await
                        ProcessFirstKioskNotification(timeSinceLastTransaction, timeSinceFirstStudentTransactionPastHour,
                            student, school, guardian.GuardianId);
            else
            // we'll consider this message when the student has confirmed the presence of all passengers
                response =
                    await
                        ProcessSecondKioskNotification(request.Response.Value, timeSinceFirstStudentTransactionPastHour,
                            student, school, request.KioskId);

            // insert a record of the students notification to facilitate replies
            Insert(studentKioskNotification);

            return response;
        }

        /// <summary>
        /// Processes the first kiosk notification that the student makes in the day, returning a response indicating whether another one will be needed
        /// </summary>
        /// <param name="timeSinceLastTransaction">The time since the student's last transaction</param>
        /// <param name="timeSinceFirstStudentTransactionPastHour">The time since the student's first transaction in the past hour</param>
        /// <param name="student">The student who made the request</param>
        /// <param name="school">The students school</param>
        /// <param name="guardianId">The unique identifer of the guardian of the student scheduled to pick them up</param>
        /// <returns>Reponse to return to student</returns>
        private async Task<KioskResponse> ProcessFirstKioskNotification(int timeSinceLastTransaction,
            int timeSinceFirstStudentTransactionPastHour, Student student, School school, int guardianId)
        {
            var response = new KioskResponse
            {
                Message = "",
                ResponseRequired = false,
                Tone = 0
            };

            // if the student has touched on less than 30 seconds ago, tell them to go away and try again in 30 seconds
            if (timeSinceLastTransaction <= 30 && timeSinceLastTransaction != 0)
            {
                response.Message = "You have already touched on recently, we may still be processing your request. \n" +
                                   "Please try again in 30 seconds.";
                response.Tone = 1;
            }
            // use pickupschedule and studentcircle to find if any students are collected in the same group as user studentId
            else if (await _studentService.IsStudentInCarpoolToday(guardianId, school.SchoolId))
            {
                response.Message =
                    "Please confirm you are with the other students in your carpool group for today. \n" +
                    "Press YES if everyone is here. Press NO otherwise.";
                response.ResponseRequired = true;
                response.ButtonOneText = "YES";
                response.ButtonTwoText = "NO";
                response.Tone = 2;
            }
            else
            {
                // contact guardian(s), get ETA and generate response based on that information
                var kioskResponse = await
                    _guardianService.ContactStudentsGuardian(student.StudentId, student.CommonName,
                        timeSinceFirstStudentTransactionPastHour, school.Latitude.Value, school.Longtitude.Value,
                        school.SchoolGeoZones.ToList());
                response.Message = kioskResponse.Item1;
                response.Tone = kioskResponse.Item2;
            }
            return response;
        }

        /// <summary>
        /// Processes the students request to see whether all of the students in the carpool are present
        /// </summary>
        /// <param name="studentResponse">The students response to the first notification</param>
        /// <param name="timeSinceFirstStudentTransactionPastHour">The time since the student's first transaction in the past hour</param>
        /// <param name="student">The student who made the request</param>
        /// <param name="school">The students school</param>
        /// <param name="kioskId">The unique identifer of the kiosk</param>
        /// <returns>The response to return to the student</returns>
        private async Task<KioskResponse> ProcessSecondKioskNotification(int studentResponse,
            int timeSinceFirstStudentTransactionPastHour, Student student, School school, int kioskId)
        {
            var response = new KioskResponse
            {
                Message = "",
                ResponseRequired = false,
                Tone = 0
            };

            var priorInteractionExists = await CheckPriorInteractionExistsToday(student.StudentId, kioskId);
            if (priorInteractionExists)
            {
                switch (studentResponse)
                {
                    case 1:
                        // contact guardian(s), get ETA and generate response based on that information
                        var kioskResponse =
                            await _guardianService.ContactStudentsGuardian(student.StudentId, student.CommonName,
                                timeSinceFirstStudentTransactionPastHour, school.Latitude.Value, school.Longtitude.Value,
                                school.SchoolGeoZones.ToList());
                        response.Message = kioskResponse.Item1;
                        response.Tone = kioskResponse.Item2;
                        break;
                    case 2:
                        // terminate transaction, tell user to check back later once all passengers have gathered
                        response.Message = "Please try again later when all passengers are with you.";
                        response.Tone = 1;
                        break;
                    default:
                        throw new ArgumentException("Invalid kiosk response.");
                }
            }
            else
            {
                throw new Exception("Invalid kiosk response received.");
            }
            return response;
        }
    }
}