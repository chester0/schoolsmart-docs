﻿using System;
using System.IO;
using Effort.DataLoaders;
using Repository.Pattern.Ef6;
using Repository.Pattern.Repositories;
using Repository.Pattern.UnitOfWork;
using StudentPickup.Data;
using StudentPickup.Repository.Repositories;
using Xunit;

namespace StudentPickup.Repository.Tests.IntegrationTests
{
    public class GuardianDeviceRepositoryTests
    {
        //Create in memeory test entity connection to make unit testing easier
        private readonly IUnitOfWorkAsync _unitOfWork;
        private readonly IRepositoryAsync<GuardianDevice> _guardianDeviceRepository;

        public GuardianDeviceRepositoryTests()
        {
            // Loads csv files from TestData folder, csv file per table
            IDataLoader loader = new CsvDataLoader(Path.Combine(AppDomain.CurrentDomain.SetupInformation.ApplicationBase, "TestData"));

            // Setup In Memory Database Context
            var connection = Effort.EntityConnectionFactory.CreateTransient("name=StudentPickupEntities", loader);
            var studentPickupContext = new StudentPickupEntities(connection);
            _unitOfWork = new UnitOfWork(studentPickupContext);

            // Setup Repositories
            _guardianDeviceRepository = new Repository<GuardianDevice>(studentPickupContext, _unitOfWork);
        }

        [Fact]
        public async void UpdateGuardianDeviceSubscriptionIdDeviceExists()
        {
            await _guardianDeviceRepository.UpdateGuardianDeviceSubscriptionId("TEST", "TEST10");
            await _unitOfWork.SaveChangesAsync();
            var updatedDevice = await _guardianDeviceRepository.FindAsync(1);
            Assert.Equal("TEST10", updatedDevice.DeviceAppId);
        }

        [Fact]
        public async void RemoveGuardianDeviceBySubscriptionIdDeviceExists()
        {
            await _guardianDeviceRepository.RemoveGuardianDeviceBySubscriptionId("TEST");
            await _unitOfWork.SaveChangesAsync();
            var updatedDevice = await _guardianDeviceRepository.FindAsync(1);
            Assert.Null(updatedDevice);
        }
    }
}
