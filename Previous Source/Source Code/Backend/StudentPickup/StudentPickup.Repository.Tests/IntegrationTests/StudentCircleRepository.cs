﻿using System;
using System.Data.Entity.Core.EntityClient;
using System.IO;
using Effort.DataLoaders;
using Repository.Pattern.DataContext;
using Repository.Pattern.Ef6;
using Repository.Pattern.Repositories;
using Repository.Pattern.UnitOfWork;
using StudentPickup.Data;
using StudentPickup.Repository.Repositories;
using Xunit;

namespace StudentPickup.Repository.Tests.IntegrationTests
{
    public class StudentCircleRepositoryTests
    {
        //Create in memeory test entity connection to make unit testing easier
        private readonly EntityConnection _connection;
        private readonly IDataContextAsync _studentPickupContext;
        private readonly IUnitOfWorkAsync _unitOfWork;
        private readonly IRepositoryAsync<StudentCircle> _studentCircleRepository;

        public StudentCircleRepositoryTests()
        {
            // Loads csv files from TestData folder, csv file per table
            IDataLoader loader = new CsvDataLoader(Path.Combine(AppDomain.CurrentDomain.SetupInformation.ApplicationBase, "TestData"));

            // Setup In Memory Database Context
            _connection = Effort.EntityConnectionFactory.CreateTransient("name=StudentPickupEntities", loader);
            _studentPickupContext = new StudentPickupEntities(_connection);
            _unitOfWork = new UnitOfWork(_studentPickupContext);

            // Setup Repositories
            _studentCircleRepository = new Repository<StudentCircle>(_studentPickupContext, _unitOfWork);
        }

        public void Dispose()
        {
            _unitOfWork.Dispose();
            _studentPickupContext.Dispose();
            _connection.Dispose();
        }

        [Fact]
        public async void GetStudentCircleByStudentIdTrustedCircleExistsShouldReturnCircle()
        {
            var studentCircle = await _studentCircleRepository.GetStudentCircleByStudentId(13, CircleTypes.Trusted);
            Assert.NotNull(studentCircle);
            Assert.Equal(1, (int)studentCircle.CircleTypeId);
        }

        [Fact]
        public async void GetStudentCircleByStudentIdScheduledCircleExistsShouldReturnCircle()
        {
            var studentCircle = await _studentCircleRepository.GetStudentCircleByStudentId(13, CircleTypes.Scheduled);
            Assert.NotNull(studentCircle);
            Assert.Equal(2, (int)studentCircle.CircleTypeId);
        }

        [Fact]
        public async void GetStudentCircleByStudentIdTrustedCircleDoesntExistShouldReturnNewCircle()
        {
            var studentCircle = await _studentCircleRepository.GetStudentCircleByStudentId(14, CircleTypes.Trusted);
            Assert.NotNull(studentCircle);
            Assert.Equal(1, (int)studentCircle.CircleTypeId);
        }

        [Fact]
        public async void GetStudentCircleByStudentIdScheduledCircleDoesntExistShouldReturnNewCircle()
        {
            var studentCircle = await _studentCircleRepository.GetStudentCircleByStudentId(14, CircleTypes.Scheduled);
            Assert.NotNull(studentCircle);
            Assert.Equal(2, (int)studentCircle.CircleTypeId);
        }
    }
}
