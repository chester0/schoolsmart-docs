﻿using System;
using System.Data.Entity.Core.EntityClient;
using System.IO;
using System.Linq;
using Effort.DataLoaders;
using Repository.Pattern.DataContext;
using Repository.Pattern.Ef6;
using Repository.Pattern.Repositories;
using Repository.Pattern.UnitOfWork;
using StudentPickup.Data;
using StudentPickup.Repository.Repositories;
using Xunit;

namespace StudentPickup.Repository.Tests.IntegrationTests
{
    public class PickupScheduleSessionRepositoryTests
    {
        //Create in memeory test entity connection to make unit testing easier
        private readonly EntityConnection _connection;
        private readonly IDataContextAsync _studentPickupContext;
        private readonly IUnitOfWorkAsync _unitOfWork;
        private readonly IRepositoryAsync<PickupScheduleSession> _pickupScheduleSessionRepository;

        public PickupScheduleSessionRepositoryTests()
        {
            // Loads csv files from TestData folder, csv file per table
            IDataLoader loader = new CsvDataLoader(Path.Combine(AppDomain.CurrentDomain.SetupInformation.ApplicationBase, "TestData"));

            // Setup In Memory Database Context
            _connection = Effort.EntityConnectionFactory.CreateTransient("name=StudentPickupEntities", loader);
            _studentPickupContext = new StudentPickupEntities(_connection);
            _unitOfWork = new UnitOfWork(_studentPickupContext);

            // Setup Repositories
            _pickupScheduleSessionRepository = new Repository<PickupScheduleSession>(_studentPickupContext, _unitOfWork);
        }

        public void Dispose()
        {
            _unitOfWork.Dispose();
            _studentPickupContext.Dispose();
            _connection.Dispose();
        }

        [Fact]
        public async void GetLatestSchedulesByGuardianIdGuardianExistsSingleStudentTodayShouldReturnSessions()
        {
            var pickupScheduleSession = await _pickupScheduleSessionRepository.FindAsync(1);
            pickupScheduleSession.SessionDate = DateTime.Now;
            _pickupScheduleSessionRepository.Update(pickupScheduleSession);
            await _unitOfWork.SaveChangesAsync();

            var schedules = await _pickupScheduleSessionRepository.GetLatestSchedulesByGuardianId(17);

            Assert.NotNull(schedules);
            Assert.Equal(1, schedules.Count);
            
            var firstPickupSchedule = schedules.First().PickupSchedule;
            Assert.Equal(1, firstPickupSchedule.PickupScheduleId);
            Assert.Equal(11, firstPickupSchedule.Student.StudentId);
            Assert.Equal(1, firstPickupSchedule.Student.School.SchoolId);
            Assert.Equal(3, firstPickupSchedule.Student.School.SchoolGeoZones.Count);
            Assert.Equal(12, firstPickupSchedule.Student.School.SchoolGeoZones.Sum(x => x.GeoZonePolygonPoints.Count));
        }

        [Fact]
        public async void GetLatestSchedulesByGuardianIdGuardianExistsTwoStudentsTodayShouldReturnSessions()
        {
            var pickupScheduleSessionOne = await _pickupScheduleSessionRepository.FindAsync(1);
            pickupScheduleSessionOne.SessionDate = DateTime.Now;
            _pickupScheduleSessionRepository.Update(pickupScheduleSessionOne);
            var pickupScheduleSessionTwo = await _pickupScheduleSessionRepository.FindAsync(11);
            pickupScheduleSessionTwo.SessionDate = DateTime.Now;
            _pickupScheduleSessionRepository.Update(pickupScheduleSessionTwo);
            await _unitOfWork.SaveChangesAsync();

            var schedules = await _pickupScheduleSessionRepository.GetLatestSchedulesByGuardianId(17);

            Assert.NotNull(schedules);
            Assert.Equal(2, schedules.Count);

            var firstPickupSchedule = schedules.Single(s => s.PickupScheduleSessionId == 1).PickupSchedule;
            Assert.Equal(1, firstPickupSchedule.PickupScheduleId);
            Assert.Equal(11, firstPickupSchedule.Student.StudentId);
            Assert.Equal(1, firstPickupSchedule.Student.School.SchoolId);
            Assert.Equal(3, firstPickupSchedule.Student.School.SchoolGeoZones.Count);
            Assert.Equal(12, firstPickupSchedule.Student.School.SchoolGeoZones.Sum(x => x.GeoZonePolygonPoints.Count));

            var secondPickupSchedule = schedules.Single(s => s.PickupScheduleSessionId == 11).PickupSchedule;
            Assert.Equal(11, secondPickupSchedule.PickupScheduleId);
            Assert.Equal(12, secondPickupSchedule.Student.StudentId);
            Assert.Equal(1, secondPickupSchedule.Student.School.SchoolId);
            Assert.Equal(3, secondPickupSchedule.Student.School.SchoolGeoZones.Count);
            Assert.Equal(12, secondPickupSchedule.Student.School.SchoolGeoZones.Sum(x => x.GeoZonePolygonPoints.Count));
        }

        [Fact]
        public async void GetLatestSchedulesByGuardianIdGuardianNotExistsShouldReturnEmptyList()
        {
            var schedules = await _pickupScheduleSessionRepository.GetLatestSchedulesByGuardianId(-1);

            Assert.NotNull(schedules);
            Assert.Equal(0, schedules.Count);
        }
    }
}
