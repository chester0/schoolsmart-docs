﻿using System;
using System.Data.Entity.Core.EntityClient;
using System.IO;
using System.Linq;
using Effort.DataLoaders;
using Repository.Pattern.DataContext;
using Repository.Pattern.Ef6;
using Repository.Pattern.Repositories;
using Repository.Pattern.UnitOfWork;
using StudentPickup.Data;
using StudentPickup.Repository.Repositories;
using Xunit;

namespace StudentPickup.Repository.Tests.IntegrationTests
{
    public class GuardianRepositoryTests
    {
        //Create in memeory test entity connection to make unit testing easier
        private readonly EntityConnection _connection;
        private readonly IDataContextAsync _studentPickupContext;
        private readonly IUnitOfWorkAsync _unitOfWork;
        private readonly IRepositoryAsync<Guardian> _guardianRepository;

        public GuardianRepositoryTests()
        {
            // Loads csv files from TestData folder, csv file per table
            IDataLoader loader = new CsvDataLoader(Path.Combine(AppDomain.CurrentDomain.SetupInformation.ApplicationBase, "TestData"));

            // Setup In Memory Database Context
            _connection = Effort.EntityConnectionFactory.CreateTransient("name=StudentPickupEntities", loader);
            _studentPickupContext = new StudentPickupEntities(_connection);
            _unitOfWork = new UnitOfWork(_studentPickupContext);

            // Setup Repositories
            _guardianRepository = new Repository<Guardian>(_studentPickupContext, _unitOfWork);
        }

        public void Dispose()
        {
            _unitOfWork.Dispose();
            _studentPickupContext.Dispose();
            _connection.Dispose();
        }

        [Fact]
        public async void TestUpdateGuardianLocation()
        {
            var testGuardian = new Guardian
            {
                FamilyName = "Smith",
                GivenName = "John",
                EmailAddress = "john.smith@test.com",
                MobileNumber = "123456"
            };

            _guardianRepository.Insert(testGuardian);
            await _unitOfWork.SaveChangesAsync();
            Assert.NotEqual(testGuardian.GuardianId, 0);
            Assert.Equal(testGuardian.LastKnownLatitude, null);
            Assert.Equal(testGuardian.LastKnownLongtitude, null);

            await _guardianRepository.UpdateGuardianLocation(testGuardian.GuardianId, (decimal)1234.56, (decimal)98765.43);
            await _unitOfWork.SaveChangesAsync();

            var updatedGuardian = _guardianRepository
                .Queryable()
                .Single(g => g.GuardianId == testGuardian.GuardianId);

            Assert.Equal(updatedGuardian.LastKnownLatitude, (decimal)1234.56);
            Assert.Equal(updatedGuardian.LastKnownLongtitude, (decimal)98765.43);
        }

        [Fact]
        public async void GetGuardianUpdatedRecentlyByIdGuardianUpdatedNowShouldReturnGuardian()
        {
            var guardian = await _guardianRepository.FindAsync(14);
            guardian.LastKnownEtaSeconds = 1;
            guardian.LastKnownLatitude = (decimal)1.1;
            guardian.LastKnownLongtitude = (decimal) 1.1;
            guardian.LastRecordedLocationTimestamp = DateTime.Now;
            _guardianRepository.Update(guardian);
            await _unitOfWork.SaveChangesAsync();
            var guardianUpdatedToday = await _guardianRepository.GetGuardianUpdatedRecentlyById(14);
            Assert.NotNull(guardianUpdatedToday);
            Assert.Equal(guardianUpdatedToday.GuardianId, guardian.GuardianId);
        }

        [Fact]
        public async void GetGuardianUpdatedRecentlyByIdGuardianUpdatedHourAgoShouldReturnGuardian()
        {
            var guardian = await _guardianRepository.FindAsync(14);
            guardian.LastKnownEtaSeconds = 1;
            guardian.LastKnownLatitude = (decimal)1.1;
            guardian.LastKnownLongtitude = (decimal)1.1;
            guardian.LastRecordedLocationTimestamp = DateTime.Now.AddHours(-1);
            _guardianRepository.Update(guardian);
            await _unitOfWork.SaveChangesAsync();
            var guardianUpdatedToday = await _guardianRepository.GetGuardianUpdatedRecentlyById(14);
            Assert.NotNull(guardianUpdatedToday);
            Assert.Equal(guardianUpdatedToday.GuardianId, guardian.GuardianId);
        }

        [Fact]
        public async void GetGuardianUpdatedRecentlyByIdGuardianUpdatedYesterdayShouldReturnNull()
        {
            var guardian = await _guardianRepository.FindAsync(14);
            guardian.LastKnownEtaSeconds = 1;
            guardian.LastKnownLatitude = (decimal)1.1;
            guardian.LastKnownLongtitude = (decimal)1.1;
            guardian.LastRecordedLocationTimestamp = DateTime.Now.AddDays(-1);
            await _unitOfWork.SaveChangesAsync();
            var guardianUpdatedToday = await _guardianRepository.GetGuardianUpdatedRecentlyById(14);
            Assert.Null(guardianUpdatedToday);
        }

        [Fact]
        public async void GetGuardianUpdatedRecentlyByIdGuardianUpdatedTomorrowShouldReturnNull()
        {
            var guardian = await _guardianRepository.FindAsync(14);
            guardian.LastKnownEtaSeconds = 1;
            guardian.LastKnownLatitude = (decimal)1.1;
            guardian.LastKnownLongtitude = (decimal)1.1;
            guardian.LastRecordedLocationTimestamp = DateTime.Now.AddDays(1);
            await _unitOfWork.SaveChangesAsync();
            var guardianUpdatedToday = await _guardianRepository.GetGuardianUpdatedRecentlyById(14);
            Assert.Null(guardianUpdatedToday);
        }

        [Fact]
        public async void GetGuardianUpdatedRecentlyByIdGuardianUpdatedLastWeekShouldReturnNull()
        {
            var guardian = await _guardianRepository.FindAsync(14);
            guardian.LastKnownEtaSeconds = 1;
            guardian.LastKnownLatitude = (decimal)1.1;
            guardian.LastKnownLongtitude = (decimal)1.1;
            guardian.LastRecordedLocationTimestamp = DateTime.Now.AddDays(-7);
            await _unitOfWork.SaveChangesAsync();
            var guardianUpdatedToday = await _guardianRepository.GetGuardianUpdatedRecentlyById(14);
            Assert.Null(guardianUpdatedToday);
        }

        [Fact]
        public async void GetGuardianUpdatedRecentlyByIdGuardianUpdatedNextWeekShouldReturnNull()
        {
            var guardian = await _guardianRepository.FindAsync(14);
            guardian.LastKnownEtaSeconds = 1;
            guardian.LastKnownLatitude = (decimal)1.1;
            guardian.LastKnownLongtitude = (decimal)1.1;
            guardian.LastRecordedLocationTimestamp = DateTime.Now.AddDays(7);
            await _unitOfWork.SaveChangesAsync();
            var guardianUpdatedToday = await _guardianRepository.GetGuardianUpdatedRecentlyById(14);
            Assert.Null(guardianUpdatedToday);
        }

        [Fact]
        public async void GetGuardianByPhoneNumberGuardainExistsShouldReturnGuardian()
        {
            var guardian = await _guardianRepository.GetGuardianByMobileNumber("0123456");
            Assert.NotNull(guardian);
            Assert.Equal(guardian.GuardianId, 16);
            Assert.Equal(guardian.MobileNumber, "0123456");
        }

        [Fact]
        public async void GetGuardianByPhoneNumberGuardainDoesntExistShouldThrowException()
        {
            await Assert.ThrowsAsync<InvalidOperationException>(async () =>
            {
                await _guardianRepository.GetGuardianByMobileNumber("01234567");
            });
        }

        [Fact]
        public async void GetGuardianByPhoneNumberGuardainEmptyMobileNumberShouldThrowException()
        {
            await Assert.ThrowsAsync<InvalidOperationException>(async () =>
            {
                await _guardianRepository.GetGuardianByMobileNumber("");
            });
        }

        [Fact]
        public async void GetGuardiansInCircleByStudentIdGuardiansInTrustedCircle()
        {
            var guardians = await _guardianRepository.GetGuardiansInCircleByStudentId(13, CircleTypes.Trusted);
            Assert.Equal(2, guardians.Count);
        }

        [Fact]
        public async void GetGuardiansInCircleByStudentIdGuardiansInScheduledCircle()
        {
            var guardians = await _guardianRepository.GetGuardiansInCircleByStudentId(13, CircleTypes.Scheduled);
            Assert.Equal(2, guardians.Count);
        }

        [Fact]
        public async void GetGuardiansInCircleByStudentIdNoGuardiansInTrustedCircle()
        {
            var guardians = await _guardianRepository.GetGuardiansInCircleByStudentId(14, CircleTypes.Trusted);
            Assert.Equal(0, guardians.Count);
        }

        [Fact]
        public async void GetGuardiansInCircleByStudentIdNoGuardiansInScheduledCircle()
        {
            var guardians = await _guardianRepository.GetGuardiansInCircleByStudentId(14, CircleTypes.Scheduled);
            Assert.Equal(0, guardians.Count);
        }

        [Fact]
        public async void GetGuardiansInCircleByPickupScheduleIdGuardiansInTrustedCircle()
        {
            var guardians = await _guardianRepository.GetGuardiansInCircleByPickupScheduleId(13);
            Assert.Equal(2, guardians.Count);
        }

        [Fact]
        public async void GetGuardiansInCircleByPickupScheduleIdGuardiansInScheduledCircle()
        {
            var guardians = await _guardianRepository.GetGuardiansInCircleByPickupScheduleId(13);
            Assert.Equal(2, guardians.Count);
        }

        [Fact]
        public async void GetGuardiansInCircleByPickupScheduleIdNoGuardiansInTrustedCircle()
        {
            var guardians = await _guardianRepository.GetGuardiansInCircleByPickupScheduleId(14);
            Assert.Equal(0, guardians.Count);
        }

        [Fact]
        public async void GetGuardiansInCircleByPickupScheduleIdNoGuardiansInScheduledCircle()
        {
            var guardians = await _guardianRepository.GetGuardiansInCircleByPickupScheduleId(14);
            Assert.Equal(0, guardians.Count);
        }
    }
}
