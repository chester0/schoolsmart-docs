﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Configuration;
using PushSharp;
using PushSharp.Android;
using PushSharp.Core;
using Repository.Pattern.UnitOfWork;
using StudentPickup.Core.PushNotifications;
using StudentPickup.Service;

namespace StudentPickup.GooglePushNotificationProvider
{
    /// <summary>
    /// An adapter for the Google Cloud Messaging Service to send push notifcations to Android devices
    /// </summary>
    [Export(typeof(IPushNotificationAdapter))]
    [ExportMetadata("OperatingSystemName", "Android")]
    [ExportMetadata("OperatingSystemId", 1)]
    public class GooglePushNotificationAdapter : PushNotificationAdapter, IPushNotificationAdapter
    {
        private readonly IGuardianDeviceService _guardianDeviceService;
        private readonly IUnitOfWorkAsync _unitOfWorkAsync;

        [ImportingConstructor]
        public GooglePushNotificationAdapter(IGuardianDeviceService guardianDeviceService, IUnitOfWorkAsync unitOfWorkAsync)
        {
            _guardianDeviceService = guardianDeviceService;
            _unitOfWorkAsync = unitOfWorkAsync;
        }

        /// <summary>
        /// Send a push notification to an Android device registered with the Google Cloud Messaging Service
        /// </summary>
        /// <param name="deviceId">The unique identifier of the device registered with the Google Cloud Messaging Service</param>
        /// <param name="pushNotificationType">The type of the push notification to send</param>
        /// <param name="alert">The alert message in the push notitification</param>
        public bool SendPushNotification(string deviceId, PushNotificationTypes pushNotificationType, string alert)
        {
            return SendPushNotification(deviceId, pushNotificationType, alert, new Dictionary<string, string>());
        }

        /// <summary>
        /// Send a push notification to an Android device registered with the Google Cloud Messaging Service
        /// </summary>
        /// <param name="deviceId">The unique identifier of the device registered with the Google Cloud Messaging Service</param>
        /// <param name="pushNotificationType">The type of the push notification to send</param>
        /// <param name="alert">The alert message in the push notitification</param>
        /// <param name="customData">The custom data to be sent to the device as JSON</param>
        public bool SendPushNotification(string deviceId, PushNotificationTypes pushNotificationType, string alert, Dictionary<string, string> customData)
        {
            try
            {
                //Create our push services broker
                var push = new PushBroker();

                //Wire up the events for all the services that the broker registers
                push.OnChannelException += ChannelException;
                push.OnServiceException += ServiceException;
                push.OnNotificationFailed += NotificationFailed;
                push.OnDeviceSubscriptionExpired += DeviceSubscriptionExpired;
                push.OnDeviceSubscriptionChanged += DeviceSubscriptionChanged;

                push.RegisterGcmService(new GcmPushChannelSettings(ConfigurationManager.AppSettings["GCMApiKey"]));

                var notitication =
                    new GcmNotification()
                        .ForDeviceRegistrationId(deviceId);

                if (customData == null)
                    customData = new Dictionary<string, string>();

                customData.Add("message", alert);
                customData.Add("type", GetPushNotificationTypeString(pushNotificationType));
                notitication.WithData(customData);

                push.QueueNotification(notitication);

                //Stop and wait for the queues to drains
                push.StopAllServices();

                // If all goes well should reach here and return true
                return true;
            }
            catch (Exception ex)
            {
                // Shouldn't fail, that would suggest Googles Push Notification Service is down
                // If it is log error and return false
                LogError(ex);
                return false;
            }
        }

        protected virtual async void DeviceSubscriptionChanged(object sender, string oldSubscriptionId, string newSubscriptionId, INotification notification)
        {
            await _guardianDeviceService.UpdateGuardianDeviceSubscriptionId(oldSubscriptionId, newSubscriptionId);
            await _unitOfWorkAsync.SaveChangesAsync();
        }

        protected virtual async void DeviceSubscriptionExpired(object sender, string expiredSubscriptionId, DateTime expirationDateUtc, INotification notification)
        {
            await _guardianDeviceService.RemoveGuardianDeviceBySubscriptionId(expiredSubscriptionId);
            await _unitOfWorkAsync.SaveChangesAsync();
        }
    }
}
