//
//  AppDelegate.h
//  iOSParentAppPrototype
//
//  Created by Faisal Alshemaimry on 6/07/2015.
//  Copyright (c) 2015 schoolsmart. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIApplication+SimulatorRemoteNotifications.h"
#import "UNIRest.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

